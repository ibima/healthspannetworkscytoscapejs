var cytoscapeInput = {
  "format_version" : "1.0",
  "generated_by" : "cytoscape-3.6.0",
  "target_cytoscapejs_version" : "~2.1",
  "data" : {
    "shared_name" : "Alignment_1",
    "name" : "Alignment_1",
    "SUID" : 3618,
    "__Annotations" : [ "" ],
    "selected" : true
  },
  "elements" : {
    "nodes" : [ {
      "data" : {
        "id" : "3637",
        "B" : 12.53081,
        "logFC" : -0.3532056,
        "isExcludedFromPaths" : false,
        "adj_P_Val" : 1.4E-9,
        "P_Value" : 3.32E-10,
        "shared_name" : "",
        "t" : -9.824309,
        "Network" : "genemania_H",
        "Gene_title" : "maternal embryonic leucine zipper kinase",
        "name" : "",
        "isInPath" : false,
        "SUID" : 3637,
        "Protein_name" : "MELK",
        "ID" : "ILMN_2212909",
        "selected" : false
      },
      "position" : {
        "x" : -79.60576038620098,
        "y" : -43.96165084838867
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "3636",
        "isExcludedFromPaths" : false,
        "shared_name" : "",
        "Network" : "genemania_H",
        "Gene_title" : "",
        "name" : "",
        "isInPath" : false,
        "SUID" : 3636,
        "Protein_name" : "BRSK2",
        "ID" : "",
        "selected" : false
      },
      "position" : {
        "x" : -148.9893341064453,
        "y" : -76.23943919875995
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "3635",
        "B" : -3.34842,
        "logFC" : 0.1784461,
        "isExcludedFromPaths" : false,
        "adj_P_Val" : 0.00356,
        "P_Value" : 0.00223,
        "shared_name" : "",
        "t" : 3.393923,
        "Network" : "genemania_H",
        "Gene_title" : "p21 (RAC1) activated kinase 4",
        "name" : "",
        "isInPath" : false,
        "SUID" : 3635,
        "Protein_name" : "PAK4",
        "ID" : "ILMN_1728887",
        "selected" : false
      },
      "position" : {
        "x" : -216.70979900100602,
        "y" : -45.92530822753906
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "3634",
        "B" : -1.06563,
        "logFC" : -0.2122673,
        "isExcludedFromPaths" : false,
        "adj_P_Val" : 4.04E-4,
        "P_Value" : 2.18E-4,
        "shared_name" : "",
        "t" : -4.296217,
        "Network" : "genemania_H",
        "Gene_title" : "glycogen synthase kinase 3 beta",
        "name" : "",
        "isInPath" : true,
        "SUID" : 3634,
        "Protein_name" : "GSK3B",
        "ID" : "ILMN_1779376",
        "selected" : false
      },
      "position" : {
        "x" : -8.111390879365052,
        "y" : -75.31465148925781
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "3633",
        "B" : 29.36006,
        "logFC" : -1.9857078,
        "isExcludedFromPaths" : false,
        "adj_P_Val" : 4.79E-16,
        "P_Value" : 2.56E-17,
        "shared_name" : "",
        "t" : -20.171534,
        "Network" : "genemania_H",
        "Gene_title" : "cyclin dependent kinase inhibitor 2B",
        "name" : "",
        "isInPath" : true,
        "SUID" : 3633,
        "Protein_name" : "CDKN2B",
        "ID" : "ILMN_2376723",
        "selected" : false
      },
      "position" : {
        "x" : 57.835845947265625,
        "y" : -46.8486082051076
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "3632",
        "B" : -3.020219,
        "logFC" : -0.917852,
        "isExcludedFromPaths" : false,
        "adj_P_Val" : 0.07375,
        "P_Value" : 0.0127,
        "shared_name" : "",
        "t" : -3.4482615,
        "Network" : "genemania_C",
        "Gene_title" : "Maternal embryonic leucine zipper kinase",
        "name" : "",
        "isInPath" : true,
        "SUID" : 3632,
        "Protein_name" : "pig-1",
        "ID" : "189138_s_at",
        "selected" : false
      },
      "position" : {
        "x" : -81.52496337890625,
        "y" : -171.09176044723614
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "3631",
        "B" : -4.905542,
        "logFC" : 0.40481433,
        "isExcludedFromPaths" : false,
        "adj_P_Val" : 0.20148,
        "P_Value" : 0.0787,
        "shared_name" : "",
        "t" : 2.0969583,
        "Network" : "genemania_C",
        "Gene_title" : "Serine/threonine kinase SAD-1",
        "name" : "",
        "isInPath" : true,
        "SUID" : 3631,
        "Protein_name" : "sad-1",
        "ID" : "174639_at",
        "selected" : false
      },
      "position" : {
        "x" : -151.30775451660156,
        "y" : -128.77945537307636
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "3630",
        "B" : -2.061419,
        "logFC" : -0.90565233,
        "isExcludedFromPaths" : false,
        "adj_P_Val" : 0.05159,
        "P_Value" : 0.00514,
        "shared_name" : "",
        "t" : -4.1967849,
        "Network" : "genemania_C",
        "Gene_title" : "Serine/threonine-protein kinase pak-2",
        "name" : "",
        "isInPath" : true,
        "SUID" : 3630,
        "Protein_name" : "pak-2",
        "ID" : "193942_s_at",
        "selected" : false
      },
      "position" : {
        "x" : -219.28760700485333,
        "y" : -171.3357696533203
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "3629",
        "B" : -5.665752,
        "logFC" : -0.20418433,
        "isExcludedFromPaths" : false,
        "adj_P_Val" : 0.33324,
        "P_Value" : 0.174,
        "shared_name" : "",
        "t" : -1.5335681,
        "Network" : "genemania_C",
        "Gene_title" : "Glycogen synthase kinase-3",
        "name" : "",
        "isInPath" : false,
        "SUID" : 3629,
        "Protein_name" : "gsk-3",
        "ID" : "192402_at",
        "selected" : false
      },
      "position" : {
        "x" : -9.285476872710143,
        "y" : -125.59605407714844
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "3628",
        "B" : -5.674925,
        "logFC" : -0.13571967,
        "isExcludedFromPaths" : false,
        "adj_P_Val" : 0.33555,
        "P_Value" : 0.176,
        "shared_name" : "",
        "t" : -1.5263512,
        "Network" : "genemania_C",
        "Gene_title" : "hypothetical protein",
        "name" : "",
        "isInPath" : false,
        "SUID" : 3628,
        "Protein_name" : "C25G6.3",
        "ID" : "188702_at",
        "selected" : false
      },
      "position" : {
        "x" : 54.57393818161115,
        "y" : -170.27880859375
      },
      "selected" : false
    } ],
    "edges" : [ {
      "data" : {
        "id" : "3654",
        "source" : "3636",
        "target" : "3637",
        "shared_name" : "",
        "shared_interaction" : "intra",
        "name" : "",
        "interaction" : "intra",
        "isInPath" : false,
        "SUID" : 3654,
        "Weight" : 0.003443691,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "3653",
        "source" : "3635",
        "target" : "3636",
        "shared_name" : "",
        "shared_interaction" : "intra",
        "name" : "",
        "interaction" : "intra",
        "isInPath" : false,
        "SUID" : 3653,
        "Weight" : 0.002957258,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "3652",
        "source" : "3634",
        "target" : "3637",
        "shared_name" : "",
        "shared_interaction" : "intra",
        "name" : "",
        "interaction" : "intra",
        "isInPath" : false,
        "SUID" : 3652,
        "Weight" : 0.003443691,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "3651",
        "source" : "3634",
        "target" : "3636",
        "shared_name" : "",
        "shared_interaction" : "intra",
        "name" : "",
        "interaction" : "intra",
        "isInPath" : false,
        "SUID" : 3651,
        "Weight" : 0.002796792,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "3650",
        "source" : "3634",
        "target" : "3635",
        "shared_name" : "",
        "shared_interaction" : "intra",
        "name" : "",
        "interaction" : "intra",
        "isInPath" : false,
        "SUID" : 3650,
        "Weight" : 0.002957258,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "3649",
        "source" : "3633",
        "target" : "3634",
        "shared_name" : "",
        "shared_interaction" : "intra",
        "name" : "",
        "interaction" : "intra",
        "isInPath" : false,
        "SUID" : 3649,
        "Weight" : 0.015837821,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "3648",
        "source" : "3632",
        "target" : "3637",
        "shared_name" : "",
        "shared_interaction" : "inter",
        "name" : "",
        "interaction" : "inter",
        "isInPath" : false,
        "SUID" : 3648,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "3647",
        "source" : "3631",
        "target" : "3636",
        "shared_name" : "",
        "shared_interaction" : "inter",
        "name" : "",
        "interaction" : "inter",
        "isInPath" : false,
        "SUID" : 3647,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "3646",
        "source" : "3631",
        "target" : "3632",
        "shared_name" : "",
        "shared_interaction" : "intra",
        "name" : "",
        "interaction" : "intra",
        "isInPath" : false,
        "SUID" : 3646,
        "Weight" : 0.004684968,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "3645",
        "source" : "3630",
        "target" : "3635",
        "shared_name" : "",
        "shared_interaction" : "inter",
        "name" : "",
        "interaction" : "inter",
        "isInPath" : false,
        "SUID" : 3645,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "3644",
        "source" : "3630",
        "target" : "3631",
        "shared_name" : "",
        "shared_interaction" : "intra",
        "name" : "",
        "interaction" : "intra",
        "isInPath" : false,
        "SUID" : 3644,
        "Weight" : 0.004828354,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "3643",
        "source" : "3629",
        "target" : "3634",
        "shared_name" : "",
        "shared_interaction" : "inter",
        "name" : "",
        "interaction" : "inter",
        "isInPath" : false,
        "SUID" : 3643,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "3642",
        "source" : "3629",
        "target" : "3632",
        "shared_name" : "",
        "shared_interaction" : "intra",
        "name" : "",
        "interaction" : "intra",
        "isInPath" : false,
        "SUID" : 3642,
        "Weight" : 0.004754209,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "3641",
        "source" : "3629",
        "target" : "3631",
        "shared_name" : "",
        "shared_interaction" : "intra",
        "name" : "",
        "interaction" : "intra",
        "isInPath" : false,
        "SUID" : 3641,
        "Weight" : 0.004642626,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "3640",
        "source" : "3629",
        "target" : "3630",
        "shared_name" : "",
        "shared_interaction" : "intra",
        "name" : "",
        "interaction" : "intra",
        "isInPath" : false,
        "SUID" : 3640,
        "Weight" : 0.004899714,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "3639",
        "source" : "3628",
        "target" : "3633",
        "shared_name" : "",
        "shared_interaction" : "inter",
        "name" : "",
        "interaction" : "inter",
        "isInPath" : false,
        "SUID" : 3639,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "3638",
        "source" : "3628",
        "target" : "3629",
        "shared_name" : "",
        "shared_interaction" : "intra",
        "name" : "",
        "interaction" : "intra",
        "isInPath" : false,
        "SUID" : 3638,
        "Weight" : 0.086104962,
        "selected" : false
      },
      "selected" : false
    } ]
  }
}
var cyHumanCElegans = writeHTMLenvironment("cyHumanCElegans",cytoscapeInput,8,1,"Protein_name","","");

