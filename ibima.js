

function existsInArray(array,ele) {
   for(var i=0,l=array.length; i<l; i++) {
      if (array[i]==ele) return(i);
   }
   return(-1);
}

function writeGeneNames(graph,containerName,attributeName) {
  document.write("<p><b>Genes referenced in network:</b> ");
  //document.write("<div id=\""+attributeName+"_genes\">");
  var genes=new Array();

  //document.write("A");
  graph.nodes().forEach(function(ele) {
	//document.write(ele.data("gene_name"));
	//document.write(ele.data("Protein_name"));
	genes.push(ele.data(attributeName));
  });
  genes.sort();
  //document.write("B");
  var genesL=genes.length;
  for(var geneno=0; geneno<genesL; geneno++) {
     var gene = genes[geneno];
     var divid = attributeName+"_gene_"+gene+">";
    //document.write("<button id=\""+divid+"\" onclick=\"document.getElementById('p_"
    //               +containerName+"').innerHTML='<b>Genes selected:</b> "+gene+"';\">"+gene+"</button>\n");
    document.write("<button id=\""+divid+"\" onclick=\"selectGenesByName("
	                +containerName+","
	                +"'"+containerName+"'"+","
	                +"'"+attributeName+"',"
	                +"['"+gene+"']);\">"+gene+"</button>\n");
  }
  document.write("</p>\n");
}


function findNodeIdByGeneName(graph,attribute,name) {
   var r = -1;
   graph.nodes().forEach(function(ele) {
      if(ele.data(attribute)==name) {
	r=ele.id();
	//break;
      }
   });
   return(r);
}

function selectGenesByName(graph,containerName,attribute,genes) {
  graph.$(':selected').style({
	'border-color': 'white',
	'border-style': 'solid',
	'border-width': 1,
	'shape': 'ellipse'
      });

  graph.$(':selected').unselect();
  var p = document.getElementById("p_"+containerName); // text field for gene list
  var mem = document.getElementById("mem_"+containerName); // <a>... hyperlink to MEM
  var gprofiler = document.getElementById("gprofiler_"+containerName); // <a>... hyperlink to MEM
  mem.href="https://biit.cs.ut.ee/mem/index.cgi?";
  gprofiler.href="https://biit.cs.ut.ee/gprofiler/index.cgi?organism=hsapiens";
  if (containerName.startsWith("cyC")) {
    mem.href+="dc=A-AFFY-60&";
  }
  else { 
    // human
    mem.href+="dc=A-AFFY-33&";
  }
  if (genes.length>1) {
    mem.href += "mgrep=";
    mem.href += genes.join("+");
  }
  mem.href += "&query="+genes[0];
  gprofiler.href += "&query="+genes.join("+");
  gprofiler.href += "&significant=1&ordered_query=0";

  mem.innerHTML = "Presentation of genes in expression data in 'Multi Experiment Matrix'";
  gprofiler.innerHTML = "'g:profiler'";

  var v = "<b>Genes selected:</b> ";
  for(var g=0,l=genes.length;g<l;g++) {
    if (0<g) v+=", ";
    v += genes[g];
    var gid= findNodeIdByGeneName(graph,attribute,genes[g]);
    //v += " ("+gid+")";   // which is the node ID in graph
    graph.getElementById(gid).select();
  }
  p.innerHTML=v;
  //document.write("<button id=\""+divid+"\" onclick=\"document.getElementById('p_"
  //               +containerName+"')

  // style of selected genes in graph
  graph.$(':selected').style({
	'border-color': 'yellow',
	'border-style': 'solid',
	'border-width': 6,
	'shape': 'diamond'
  });

}

function writeGroups(graph,containerName,attributeName,whatIsIt,groupAttributeName,minNumber) {
  var groups = new Array();
  var group2genes= new Array();
  graph.nodes().forEach(function(ele) {
    //document.write(ele.data("gene_name"));
    var annotations = ele.data(groupAttributeName);
    if (typeof annotations == "undefined") {
      return;
    }

    if (!Array.isArray(annotations)) {
      if (typeof annotations == "number") {
        annotations = [ ""+annotations ];
      } else if (typeof annotations == "string") {
        annotations = [ annotations ];
      }
    }

    if (Array.isArray(annotations)) {
      annotations.sort();
      var annotation_pref="";
      for(var i=0,l=annotations.length; i<l; i++) {
        var annotation = annotations[i];
        if (annotation == annotation_pref) continue;
	if (""!=annotation) {
          var annotIdx = existsInArray(groups,annotation);
          //document.write("annotation:"+annotation+"annotIdx:"+annotIdx+"<br>");
	  var gene = ele.data(attributeName);
	  //document.write("  <small>"+annotation +" <- " + gene +"</small>");
          if (annotIdx < 0) {
            groups.push(annotation);
            var a = new Array();
	    a.push(gene);
            group2genes.push(a);
	    //document.write("  <small>"+annotation +" <- " + ele.data(attributeName)+"</small>");
          } else {
            group2genes[annotIdx].push(gene);
          }
	}
        annotation_pref=annotation;
      }
    } else {
      console.log("E: annotations is of type ");
      console.log(typeof annotations);
    }
  });
  //document.write("groups:"+groups+"<br>");
  //document.write("<p>group2genes: "); document.write(group2genes); document.write("</p>\n");

  var groupLength = [];
  for(var i=0,l=group2genes.length; i<l; i++) {
    var g2gI = group2genes[i];
    groupLength.push(g2gI.length);
  }
  //document.write(groupLength+"<br>");
  
  var groupsSorted = groups;
  function compareGroups(a,b) {
    //document.write("a="+a+", b="+b+"<br>");
    var aIdx=existsInArray(groups,a);
    var bIdx=existsInArray(groups,b);
    var aIdxL = groupLength[aIdx];
    var bIdxL = groupLength[bIdx];
    var diff=aIdxL-bIdxL;
    // document.write("aIdx:"+aIdx+" bIdx:"+bIdx+" diff:"+diff+"<br>");
    return(diff);
  }

  //groupsSorted.sort(compareGroups);
  groupsSorted.sort();

  //document.write("<p><b>groupsSorted:</b> "+groupsSorted+"<br></p>");
  //document.write("<p><b>groups:</b> "+groups+"<br></p>");

 var l=groupsSorted.length;
 if (l>0) {
  document.write("<p><b>"+whatIsIt+" with >= "+minNumber+" genes:</b> ");
  for(var s=0,c=0; s<l; s++) {
    var groupName=groupsSorted[s];
    var i=existsInArray(groups,groupName);
    if (-1 == i) {
      document.write("\n<br><b>Internal error - no knowing the gene group " + groupName + "'<br>\n");
      return(-1);
    }
    //document.write(groupName+": "+i+"<br>");
    var g2gI = group2genes[i];
    var g2L = groupLength[i];

    if (g2gI.length>=minNumber) {
      g2gIname = groups[i];
      var divid="g_"+groupName.replace(/[^a-zA-Z0-9]/g,'_');
      document.write("<button id=\""+divid+"\" ");
      document.write(   "onclick=\"selectGenesByName("    +containerName
	                                           +","+"'"+containerName+"'"
	                                           +","+"'"+attributeName+"',"
	                                           +"['");
      for(var g=0; g<g2L;g++) {
        if (g>0) document.write("', '");
	document.write(g2gI[g]);
      }
      document.write("']);\">");
      document.write(groupName+" ("+g2gI.length //+" vs. "+g2L
      +")");
      document.write("</button>\n");
    }
  }
  document.write("</p>\n");
 }
 return(0);
}

function writeHTMLenvironment(containerName,cytoscapeInput,minGenesPerGroup,minZoom,attributeName,attributeColour,attributeSize) {

  if (typeof(attributeName) === "undefined") attributeName="gene_name";
  if (typeof(attributeColour) === "undefined") attributeColour="";
  if (typeof(attributeSize) === "undefined") attributeSize="";

  let hasAdjPVal=false;
  let hasPValue=false;
  let hasLogFC=false;
  let hasLogScore=false;
  let hasScore=false;
  let hasEffectSize=false;
//  document.write("<p>Number of nodes: " + l+"<br>\n");
  let l=cytoscapeInput.elements.nodes.length;
//  if (""==attributeSize || ""==attributeColour) {
    if (l>0) {
	let n = cytoscapeInput.elements.nodes[0].data;
	if (typeof(n) === "undefined") {
           document.write("<p>Error: Could not access data for nodes[0] of '"+containerName+"'</p>\n");
	} else {
//           document.write("<p>Can access data for nodes[0] of '"+containerName+"'.</p>\n");
	}
        hasAdjPVal = ! (typeof(n.adj_P_Val) === "undefined");
        hasPValue = ! (typeof(n.P_Value) === "undefined");
        hasLogFC = ! (typeof(n.logFC) === "undefined");
        hasLogScore = ! (typeof(n.log_score) === "undefined");
        hasScore = ! (typeof(n.score) === "undefined");
        hasEffectSize = ! (typeof(n.effect_size) === "undefined");
     } else {
        document.write("Error: No node attributes for '"+containerName+"'.\n");
     }
 // }

/*
   document.write("hasAdjPVal: " + hasAdjPVal + "<br />");
   document.write("hasPValue: " + hasPValue + "<br />");
   document.write("hasLogFC: " + hasLogFC + "<br />");
   document.write("hasLogScore: " + hasLogScore +"<br />")
*/

  document.write("<p>");
  if ("" == attributeSize) {
     attributeSize="score";
     if (hasScore) {
        document.write("Size adjustment by score node attribute.\n"); // default
     } else if (hasAdjPVal) {
        document.write("Size adjustment by 'adj_P_Val' node attribute.\n");
        for(let i=0;i<l;i++) {
           cytoscapeInput.elements.nodes[i].data.score = -Math.log(cytoscapeInput.elements.nodes[i].data.adj_P_Val)/Math.LN2;
        }
     } else if (hasPValue) {
        document.write("Size adjustment by P_Value node attribute since no adj_P_Val specified.\n");
        for(let i=0;i<l;i++) {
          //document.write("ID: "+cytoscapeInput.elements.nodes[i].data.id);
          cytoscapeInput.elements.nodes[i].data.score = -Math.log(cytoscapeInput.elements.nodes[i].data.P_Value)/Math.LN2;
        }
     } else {
        document.write("No node size adjustments since "+containerName
		      +" does neither feature the score nor the adj_P_Val nor the P_Value attribute.\n");
     }
  } else {
     document.write("Size adjustment by "+attributeSize+" node attribute.");
  }

  document.write(" ");

  if ("" == attributeColour) {
     attributeColour="logFC";
     if (hasLogFC) {
         document.write("Colouring by logFC attribute.\n");
     } else if (hasEffectSize) {
        document.write("Colouring by the effect size node attribute since no LogFC specified.\n");
        for(let i=0;i<l;i++) {
          cytoscapeInput.elements.nodes[i].data.logFC = cytoscapeInput.elements.nodes[i].data.effect_size;
        }
//     } else {
 //       document.write("No colouring since "+containerName+" does neither feature the logFC nor one of the known equivalent node attributes.\n");
     }

     for(let i=0;i<l;i++) {
        /* Adding logFC properties for those object that miss them */
        if (typeof(cytoscapeInput.elements.nodes[i].data.logFC)==="undefined") {
           console.log("I "+containerName+": Adding default logFC val to 0 for gene "+cytoscapeInput.elements.nodes[i].data[attributeName])
           cytoscapeInput.elements.nodes[i].data.logFC=0;
           cytoscapeInput.elements.nodes[i].data.missingColour=1;
        } else {
           cytoscapeInput.elements.nodes[i].data.missingColour=0;
	}
     }
  } else {
     document.write("Colouring by "+attributeColour+" node attribute.");
     for(let i=0;i<l;i++) {
        /* Adding logFC properties for those object that miss them */
        if (typeof(cytoscapeInput.elements.nodes[i].data[attributeColour])==="undefined") {
           console.log("I "+containerName+": Adding default "+attributeColour+" val to 0 for gene "+cytoscapeInput.elements.nodes[i].data[attributeName])
           cytoscapeInput.elements.nodes[i].data[attributeColour]=0;
           cytoscapeInput.elements.nodes[i].data.missingColour=1;
        } else {
           cytoscapeInput.elements.nodes[i].data.missingColour=0;
        }
        cytoscapeInput.elements.nodes[i].data["logFC"]=cytoscapeInput.elements.nodes[i].data[attributeColour];
     }
  }
  document.write("\nMissing data are shown with gray background.\n");
  document.write("</p>\n");

  var minSize=20,maxSize=70;
  if (cytoscapeInput.elements.nodes.length<15) {
    minSize=10;
    maxSize=28;
  }

  if (""==attributeSize || ""==attributeColour || ""==attributeSize) {
    document.write("<p>Error: Unset attribute names: attributeSize="+attributeSize+" attributeColour="+attributeColour+" attributeName="+attributeName+".</p>")
    console.loog("<p>Error: Unset attribute names="+attributeSize+" attributeColour="+attributeColour+" attributeName="+attributeName+".</p>")
  }

  cytoscapeInput.layout = { name: 'preset' };
  cytoscapeInput.style = [ // the stylesheet for the graph
    {
      selector: 'node',
      style: {
        'background-color': '#666',
        'label': 'data('+attributeName+')',
//        'font-size': 'small',
	//'background-color':  'mapData('+attributeColour+', -2, 2, blue, red)',
	'background-color':  function(ele) {
	    if (ele.data("missingColour")==1) return("rgb(125,125,125)");
            let red=0,green=0,blue=0;
            let l=ele.data("logFC");
	    if (ele.data("logFC")<0) {
               if (l < -2) l = -2;
               blue=255;
               red=green=Math.round(255*(2+l)/2);
            } else {
               if (l > 2) l = 2;
               red=255;
               green=blue=Math.round(255*(2-l)/2);
	    }
	    let s="rgb("+red+","+green+","+blue+")";
            console.log(s);
	    return(s);
	},
       // 'width': 'mapData(B,0,40,15,45)',
       // 'height': 'mapData(B,0,40,15,45)'
       //'width': 'mapData(tabs,0,10,'+minSize+','+maxSize+')',
       //'height': 'mapData(tabs,0,10,'+minSize+','+maxSize+')'
       'width': 'mapData('+attributeSize+',0,1,'+minSize+','+maxSize+')',
       'height': 'mapData('+attributeSize+',0,1,'+minSize+','+maxSize+')'
//       'border-style': function(ele) {if(ele.data("missingColour")==1){return("dotted");} return("solid");},
//       'border-width': function(ele) {return(3*ele.data("missingColour"));},
//       'style': function(ele) {if(ele.data("missingColour")==1){return("star");} return("ellipse");},
//       'background-opacity': 'data("missingColour")',
//       'border-color': "green"
      }
    },

    {
      selector: 'edge',
      style: {
        'width': 3,
        'line-color': function(ele) {//document.write(ele.data("shared_interaction"));
                                      if(ele.data("shared_interaction")=="inter") {return("green");} return("#ccc");
                                    },
        'line-style': function(ele) {if(ele.data("shared_interaction")=="inter") {return("dashed");} return("solid");}, // dotted as alternative
        'target-arrow-color': '#ccc',
        'target-arrow-shape': 'triangle'
      }
    }
  ];

  document.write("<div id='"+containerName+"'>");
  document.write("</div>");

  cytoscapeInput.container=document.getElementById(containerName); // container to render in
  var cy = cytoscape(cytoscapeInput);
  cy.minZoom(minZoom);

  document.write("<button type=\"button\" onclick=\"");
  document.write(containerName);
  document.write(".fit()\">fit graph</button>\n");

/*
  document.write("&nbsp;");

  document.write("<button type=\"button\" onclick=\""+"let img"+containerName+"=");
  document.write(containerName+".jpeg(full=true); ");
  document.write("let r=document.getElementById('result');");
  document.write("r.src=img"+containerName);
  document.write("\">Export Graph as JPEG</button>\n");
*/

  document.write("&nbsp;");

  document.write("<button type=\"button\" onclick=\""+"let img"+containerName+"=");
  document.write(containerName+".jpeg(full=false); ");
  document.write("let r=document.getElementById('result');");
  document.write("r.src=img"+containerName);
  document.write("\">Export View as JPEG</button>\n");

  document.write("&nbsp;");

  document.write("<small><a href=\"#result\">Exported image.</a></small>\n");

  document.write("&nbsp;");
  document.write("<p id=\""+"p_"+containerName+"\"><b>Genes selected:</b> none</p>");
  document.write("&nbsp;");
  document.write("<a id=\"mem_"+containerName+"\" href=\""+"\"><a>");
  document.write(" &nbsp; ");
  document.write("<a id=\"gprofiler_"+containerName+"\" href=\""+"\"><a>\n");
  document.write("<p>Buttons below define gene selection.</p>\n");

  writeGroups(cy,containerName,attributeName,"Clusters","__mclCluster",2);
  writeGroups(cy,containerName,attributeName,"GO terms","annotation_name",minGenesPerGroup);
  writeGeneNames(cy,containerName,attributeName);
  return(cy);
}

