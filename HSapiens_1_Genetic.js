var cytoscapeInput = {
  "format_version" : "1.0",
  "generated_by" : "cytoscape-3.6.0",
  "target_cytoscapejs_version" : "~2.1",
  "data" : {
    "source_networks" : "[{\"group\":\"Physical Interactions\",\"name\":\"IREF-HPRD\",\"weight\":2.8012254608893443E-4},{\"group\":\"Predicted\",\"name\":\"Wu-Stein-2010\",\"weight\":0.005068757929764536},{\"group\":\"Co-expression\",\"name\":\"Noble-Diehl-2008\",\"weight\":0.01518574412912439},{\"group\":\"Co-localization\",\"name\":\"Johnson-Shoemaker-2003\",\"weight\":0.021705021026302937},{\"group\":\"Co-expression\",\"name\":\"Perou-Botstein-2000\",\"weight\":0.029897515156160133},{\"group\":\"Co-expression\",\"name\":\"Bahr-Bowler-2013\",\"weight\":0.06676996884398675},{\"group\":\"Co-expression\",\"name\":\"Mallon-McKay-2013\",\"weight\":0.021432727501802373},{\"group\":\"Co-expression\",\"name\":\"Perou-Botstein-1999\",\"weight\":0.044702090969093805},{\"group\":\"Physical Interactions\",\"name\":\"Coyaud-Raught-2015\",\"weight\":0.06847845436560353},{\"group\":\"Co-expression\",\"name\":\"Bild-Nevins-2006 B\",\"weight\":0.010378404010554428},{\"group\":\"Co-expression\",\"name\":\"Boldrick-Relman-2002\",\"weight\":0.04128379037777172},{\"group\":\"Co-expression\",\"name\":\"Ramaswamy-Golub-2001\",\"weight\":0.026457598762982246},{\"group\":\"Physical Interactions\",\"name\":\"IREF-INTACT\",\"weight\":0.0033551593143451106},{\"group\":\"Shared protein domains\",\"name\":\"PFAM\",\"weight\":0.0017733601242763178},{\"group\":\"Co-expression\",\"name\":\"Dobbin-Giordano-2005\",\"weight\":0.02660954835092872},{\"group\":\"Shared protein domains\",\"name\":\"INTERPRO\",\"weight\":0.036969329819831365},{\"group\":\"Pathway\",\"name\":\"REACTOME\",\"weight\":0.004133919576777557},{\"group\":\"Predicted\",\"name\":\"I2D-INNATEDB-Mouse2Human\",\"weight\":0.006347543433221126},{\"group\":\"Co-expression\",\"name\":\"Alizadeh-Staudt-2000\",\"weight\":0.03408206277458427},{\"group\":\"Pathway\",\"name\":\"Wu-Stein-2010\",\"weight\":0.0914087134351174},{\"group\":\"Physical Interactions\",\"name\":\"IREF-SMALL-SCALE-STUDIES\",\"weight\":0.006129261169667846},{\"group\":\"Pathway\",\"name\":\"IMID\",\"weight\":0.15829471114557145},{\"group\":\"Co-localization\",\"name\":\"Schadt-Shoemaker-2004\",\"weight\":0.00773189465286489},{\"group\":\"Co-expression\",\"name\":\"Wang-Maris-2006\",\"weight\":0.06421499898051258},{\"group\":\"Physical Interactions\",\"name\":\"BIOGRID-SMALL-SCALE-STUDIES\",\"weight\":0.01141009993517426},{\"group\":\"Genetic Interactions\",\"name\":\"Lin-Smith-2010\",\"weight\":0.01040544925726619},{\"group\":\"Pathway\",\"name\":\"CELL_MAP\",\"weight\":0.09366311264596208},{\"group\":\"Co-expression\",\"name\":\"Roth-Zlotnik-2006\",\"weight\":0.0186987340161946},{\"group\":\"Co-expression\",\"name\":\"Burington-Shaughnessy-2008\",\"weight\":0.07313190574846838}]",
    "organism" : "H. sapiens",
    "__clusterAttribute" : "__mclCluster",
    "__Annotations" : [ "edgeThickness=3.0|canvas=background|fillOpacity=20.0|zoom=0.6581952065888115|type=org.cytoscape.view.presentation.annotations.ShapeAnnotation|uuid=83f4850a-2edd-4c86-b83c-b48fe4a8e55a|fillColor=-60|shapeType=ELLIPSE|edgeColor=-12566464|edgeOpacity=100.0|name=ShapeAnnotation_2|x=2014.0|width=274.02744666899287|y=-401.0|z=-1|height=90.45964063482182", "canvas=foreground|color=-16777216|zoom=0.6581952065888115|type=org.cytoscape.view.presentation.annotations.TextAnnotation|fontStyle=0|uuid=add0aa99-a01e-49cc-89f4-f25d6de059a8|fontFamily=Arial|name=TextAnnotation_0|x=1147.0|y=316.0|z=-1|fontSize=22|text=negative regulation cell mediated muscle", "edgeThickness=3.0|canvas=background|fillOpacity=20.0|zoom=0.6581952065888115|type=org.cytoscape.view.presentation.annotations.ShapeAnnotation|uuid=2ad24b5b-9e74-482c-8df1-3313d3fd756e|fillColor=-60|shapeType=ELLIPSE|edgeColor=-12566464|edgeOpacity=100.0|name=ShapeAnnotation_11|x=-1890.0|width=282.855276562807|y=381.0|z=-1|height=90.45969051875376", "canvas=foreground|color=-16777216|zoom=0.6581952065888115|type=org.cytoscape.view.presentation.annotations.TextAnnotation|fontStyle=0|uuid=27ddb42c-314e-49a1-8d12-0f422421800a|fontFamily=Arial|name=TextAnnotation_1|x=-654.0|y=-527.0|z=-1|fontSize=28|text=lipid transport esterification process cholesterol", "canvas=foreground|color=-16777216|zoom=0.6581952065888115|type=org.cytoscape.view.presentation.annotations.TextAnnotation|fontStyle=0|uuid=124da95f-0102-4ac8-b8fe-8885c4d39fd2|fontFamily=Arial|name=TextAnnotation_7|x=707.0|y=-507.0|z=-1|fontSize=28|text=smoothened signaling canonical wnt pathway", "canvas=foreground|color=-16777216|zoom=0.6581952065888115|type=org.cytoscape.view.presentation.annotations.TextAnnotation|fontStyle=0|uuid=87855b58-fcef-4b21-96bb-215f9e40edab|fontFamily=Arial|name=TextAnnotation_5|x=1801.0|y=-442.0|z=-1|fontSize=23|text=cyclin-dependent kinase regulator activity inhibitor", "canvas=foreground|color=-16777216|zoom=0.6581952065888115|type=org.cytoscape.view.presentation.annotations.TextAnnotation|fontStyle=0|uuid=e7a7ab11-8677-41a6-b6b7-27141547baee|fontFamily=Arial|name=TextAnnotation_12|x=1593.0|y=322.0|z=-1|fontSize=22|text=apoptotic absence ligand endopeptidase regulation", "canvas=foreground|color=-16777216|zoom=0.6581952065888115|type=org.cytoscape.view.presentation.annotations.TextAnnotation|fontStyle=0|uuid=0d18d9d9-b347-4e90-9793-d6c425123f98|fontFamily=Arial|name=TextAnnotation_9|x=-358.0|y=326.0|z=-1|fontSize=18|text=regulation cysteine-type endopeptidase activation positive", "canvas=foreground|color=-16777216|zoom=0.6581952065888115|type=org.cytoscape.view.presentation.annotations.TextAnnotation|fontStyle=0|uuid=9ed9884c-6110-407e-becb-576485b46d6f|fontFamily=Arial|name=TextAnnotation_2|x=-2362.0|y=-530.0|z=-1|fontSize=50|text=initiation rna notch processing transcription", "canvas=foreground|color=-16777216|zoom=0.6581952065888115|type=org.cytoscape.view.presentation.annotations.TextAnnotation|fontStyle=0|uuid=8bf39cca-3f4f-4b50-8550-fbe2759311f3|fontFamily=Arial|name=TextAnnotation_3|x=-879.0|y=326.0|z=-1|fontSize=18|text=negative regulation receptor-mediated endocytosis", "canvas=foreground|color=-256|zoom=0.6581952065888115|type=org.cytoscape.view.presentation.annotations.TextAnnotation|fontStyle=0|uuid=7a4805c1-1757-433c-a1b2-e348c664fe45|fontFamily=Arial|name=TextAnnotation_11|x=-1545.0|y=-452.0|z=-1|fontSize=46|text=cell regulation muscle positive proliferation", "edgeThickness=9.0|canvas=background|fillOpacity=20.0|zoom=0.6581952065888115|type=org.cytoscape.view.presentation.annotations.ShapeAnnotation|uuid=0be551eb-dd5d-4ced-854b-33372975d2bb|fillColor=-60|shapeType=ELLIPSE|edgeColor=-256|edgeOpacity=100.0|name=ShapeAnnotation_1|x=-1010.0|width=247.79571179997546|y=-376.0|z=-1|height=225.24770966970917", "canvas=foreground|color=-16777216|zoom=0.6581952065888115|type=org.cytoscape.view.presentation.annotations.TextAnnotation|fontStyle=0|uuid=19c4c0ec-23dd-44da-a617-b289fbfe7f27|fontFamily=Arial|name=TextAnnotation_4|x=39.0|y=-460.0|z=-1|fontSize=28|text=regulation cell differentiation thymus activation", "edgeThickness=3.0|canvas=background|fillOpacity=20.0|zoom=0.6581952065888115|type=org.cytoscape.view.presentation.annotations.ShapeAnnotation|uuid=044990de-c4b3-4fe5-af23-851460334089|fillColor=-60|shapeType=ELLIPSE|edgeColor=-12566464|edgeOpacity=100.0|name=ShapeAnnotation_9|x=-1744.0|width=202.67583426905404|y=-455.0|z=-1|height=275.5783568411245", "canvas=foreground|color=-16777216|zoom=0.6581952065888115|type=org.cytoscape.view.presentation.annotations.TextAnnotation|fontStyle=0|uuid=c13630eb-5361-401a-9047-63966116c381|fontFamily=Arial|name=TextAnnotation_10|x=1277.0|y=-473.0|z=-1|fontSize=28|text=regulatory nucleic binding myotube striated", "edgeThickness=3.0|canvas=background|fillOpacity=20.0|zoom=0.6581952065888115|type=org.cytoscape.view.presentation.annotations.ShapeAnnotation|uuid=235b8d47-952e-46a2-9c8b-27b92226cea4|fillColor=-60|shapeType=ELLIPSE|edgeColor=-12566464|edgeOpacity=100.0|name=ShapeAnnotation_5|x=-695.0|width=282.855276562807|y=381.0|z=-1|height=90.45969051875376", "edgeThickness=3.0|canvas=background|fillOpacity=20.0|zoom=0.6581952065888115|type=org.cytoscape.view.presentation.annotations.ShapeAnnotation|uuid=fa65e9de-5408-4d32-ad95-8820483862d7|fillColor=-60|shapeType=ELLIPSE|edgeColor=-12566464|edgeOpacity=100.0|name=ShapeAnnotation_8|x=1769.0|width=288.9464947517644|y=369.0|z=-1|height=90.45963691456762", "edgeThickness=3.0|canvas=background|fillOpacity=20.0|zoom=0.6581952065888115|type=org.cytoscape.view.presentation.annotations.ShapeAnnotation|uuid=4c9f7543-1882-4afd-9881-ae69797901e9|fillColor=-60|shapeType=ELLIPSE|edgeColor=-12566464|edgeOpacity=100.0|name=ShapeAnnotation_3|x=-103.0|width=282.855276562807|y=381.0|z=-1|height=90.45969051875376", "edgeThickness=3.0|canvas=background|fillOpacity=20.0|zoom=0.6581952065888115|type=org.cytoscape.view.presentation.annotations.ShapeAnnotation|uuid=da68177f-61c3-4aa4-820b-23584d3a2502|fillColor=-60|shapeType=ELLIPSE|edgeColor=-12566464|edgeOpacity=100.0|name=ShapeAnnotation_10|x=-1299.0|width=282.855276562807|y=381.0|z=-1|height=90.45969051875376", "edgeThickness=3.0|canvas=background|fillOpacity=20.0|zoom=0.6581952065888115|type=org.cytoscape.view.presentation.annotations.ShapeAnnotation|uuid=33d5aad4-120e-4bd1-99d9-20f0c16133fe|fillColor=-60|shapeType=ELLIPSE|edgeColor=-12566464|edgeOpacity=100.0|name=ShapeAnnotation_12|x=338.0|width=201.2327957561926|y=-411.0|z=-1|height=148.03770643632174", "edgeThickness=3.0|canvas=background|fillOpacity=20.0|zoom=0.6581952065888115|type=org.cytoscape.view.presentation.annotations.ShapeAnnotation|uuid=e76f15be-1b1e-4ab6-a038-e0bb672a661f|fillColor=-60|shapeType=ELLIPSE|edgeColor=-12566464|edgeOpacity=100.0|name=ShapeAnnotation_4|x=1222.0|width=325.4923806285493|y=364.0|z=-1|height=90.45963691456762", "canvas=foreground|color=-16777216|zoom=0.6581952065888115|type=org.cytoscape.view.presentation.annotations.TextAnnotation|fontStyle=0|uuid=75d6122b-947f-453b-911c-c7f31a8386f6|fontFamily=Arial|name=TextAnnotation_8|x=-1454.0|y=326.0|z=-1|fontSize=18|text=extrinsic apoptotic signaling pathway absence", "edgeThickness=3.0|canvas=background|fillOpacity=20.0|zoom=0.6581952065888115|type=org.cytoscape.view.presentation.annotations.ShapeAnnotation|uuid=a634768d-0591-40df-a1c4-e27e6dfe00db|fillColor=-60|shapeType=ELLIPSE|edgeColor=-12566464|edgeOpacity=100.0|name=ShapeAnnotation_7|x=944.0|width=188.9802110589688|y=-460.0|z=-1|height=184.39538512035963", "canvas=foreground|color=-16777216|zoom=0.6581952065888115|type=org.cytoscape.view.presentation.annotations.TextAnnotation|fontStyle=0|uuid=dec707b5-ad98-4b70-8bf9-aa72599b7472|fontFamily=Arial|name=TextAnnotation_6|x=-1903.0|y=326.0|z=-1|fontSize=18|text=alcohol biosynthetic process", "edgeThickness=3.0|canvas=background|fillOpacity=20.0|zoom=0.6581952065888115|type=org.cytoscape.view.presentation.annotations.ShapeAnnotation|uuid=462fa4c6-074d-4441-a446-04bd541d9982|fillColor=-60|shapeType=ELLIPSE|edgeColor=-12566464|edgeOpacity=100.0|name=ShapeAnnotation_0|x=-272.0|width=185.6111909536894|y=-473.0|z=-1|height=185.6111909536894", "edgeThickness=3.0|canvas=background|fillOpacity=20.0|zoom=0.6581952065888115|type=org.cytoscape.view.presentation.annotations.ShapeAnnotation|uuid=1be1554f-cee3-412d-9ad3-31a8835065cb|fillColor=-60|shapeType=ELLIPSE|edgeColor=-12566464|edgeOpacity=100.0|name=ShapeAnnotation_6|x=1532.0|width=197.22015265646064|y=-422.0|z=-1|height=156.06297983804666" ],
    "annotations" : "[{\"name\":\"GO:0007219\",\"description\":\"Notch signaling pathway\",\"qValue\":1.0746735859140581E-7,\"sample\":10,\"total\":112},{\"name\":\"GO:0050865\",\"description\":\"regulation of cell activation\",\"qValue\":1.1028821332409221E-5,\"sample\":11,\"total\":254},{\"name\":\"GO:0042110\",\"description\":\"T cell activation\",\"qValue\":6.998703718471637E-5,\"sample\":10,\"total\":242},{\"name\":\"GO:0006367\",\"description\":\"transcription initiation from RNA polymerase II promoter\",\"qValue\":9.298207343607845E-5,\"sample\":9,\"total\":190},{\"name\":\"GO:0097191\",\"description\":\"extrinsic apoptotic signaling pathway\",\"qValue\":9.511089578684363E-5,\"sample\":8,\"total\":137},{\"name\":\"GO:2001233\",\"description\":\"regulation of apoptotic signaling pathway\",\"qValue\":1.0090343031307081E-4,\"sample\":9,\"total\":201},{\"name\":\"GO:0051249\",\"description\":\"regulation of lymphocyte activation\",\"qValue\":1.0166631655284353E-4,\"sample\":9,\"total\":208},{\"name\":\"GO:0001819\",\"description\":\"positive regulation of cytokine production\",\"qValue\":1.0166631655284353E-4,\"sample\":9,\"total\":207},{\"name\":\"GO:0006352\",\"description\":\"DNA-templated transcription, initiation\",\"qValue\":1.1085715004015123E-4,\"sample\":9,\"total\":213},{\"name\":\"GO:1902105\",\"description\":\"regulation of leukocyte differentiation\",\"qValue\":1.8847562550986928E-4,\"sample\":7,\"total\":109},{\"name\":\"GO:0002694\",\"description\":\"regulation of leukocyte activation\",\"qValue\":1.8847562550986928E-4,\"sample\":9,\"total\":232},{\"name\":\"GO:0050863\",\"description\":\"regulation of T cell activation\",\"qValue\":2.026520074639214E-4,\"sample\":8,\"total\":169},{\"name\":\"GO:0046651\",\"description\":\"lymphocyte proliferation\",\"qValue\":3.455942718983049E-4,\"sample\":7,\"total\":123},{\"name\":\"GO:0032943\",\"description\":\"mononuclear cell proliferation\",\"qValue\":3.583221024002424E-4,\"sample\":7,\"total\":125},{\"name\":\"GO:0070661\",\"description\":\"leukocyte proliferation\",\"qValue\":5.104527010993702E-4,\"sample\":7,\"total\":133},{\"name\":\"GO:0038034\",\"description\":\"signal transduction in absence of ligand\",\"qValue\":6.15178852524055E-4,\"sample\":5,\"total\":46},{\"name\":\"GO:0097192\",\"description\":\"extrinsic apoptotic signaling pathway in absence of ligand\",\"qValue\":6.15178852524055E-4,\"sample\":5,\"total\":46},{\"name\":\"GO:0050798\",\"description\":\"activated T cell proliferation\",\"qValue\":6.72410217534103E-4,\"sample\":4,\"total\":20},{\"name\":\"GO:0050670\",\"description\":\"regulation of lymphocyte proliferation\",\"qValue\":7.36825829802907E-4,\"sample\":6,\"total\":92},{\"name\":\"GO:2001236\",\"description\":\"regulation of extrinsic apoptotic signaling pathway\",\"qValue\":7.36825829802907E-4,\"sample\":6,\"total\":92},{\"name\":\"GO:0007220\",\"description\":\"Notch receptor processing\",\"qValue\":7.36825829802907E-4,\"sample\":4,\"total\":21},{\"name\":\"GO:0032944\",\"description\":\"regulation of mononuclear cell proliferation\",\"qValue\":7.498005973879607E-4,\"sample\":6,\"total\":93},{\"name\":\"GO:0032609\",\"description\":\"interferon-gamma production\",\"qValue\":7.688359357434719E-4,\"sample\":5,\"total\":51},{\"name\":\"GO:0032729\",\"description\":\"positive regulation of interferon-gamma production\",\"qValue\":8.988929502341957E-4,\"sample\":4,\"total\":23},{\"name\":\"GO:0070663\",\"description\":\"regulation of leukocyte proliferation\",\"qValue\":8.988929502341957E-4,\"sample\":6,\"total\":98},{\"name\":\"GO:0046635\",\"description\":\"positive regulation of alpha-beta T cell activation\",\"qValue\":0.001453306374419707,\"sample\":4,\"total\":27},{\"name\":\"GO:0050867\",\"description\":\"positive regulation of cell activation\",\"qValue\":0.001453306374419707,\"sample\":7,\"total\":172},{\"name\":\"GO:0032374\",\"description\":\"regulation of cholesterol transport\",\"qValue\":0.001453306374419707,\"sample\":4,\"total\":27},{\"name\":\"GO:0032371\",\"description\":\"regulation of sterol transport\",\"qValue\":0.001453306374419707,\"sample\":4,\"total\":27},{\"name\":\"GO:0045619\",\"description\":\"regulation of lymphocyte differentiation\",\"qValue\":0.001453306374419707,\"sample\":5,\"total\":61},{\"name\":\"GO:0033077\",\"description\":\"T cell differentiation in thymus\",\"qValue\":0.0016158009932904992,\"sample\":4,\"total\":28},{\"name\":\"GO:0051055\",\"description\":\"negative regulation of lipid biosynthetic process\",\"qValue\":0.0020841761337146423,\"sample\":4,\"total\":30},{\"name\":\"GO:0016538\",\"description\":\"cyclin-dependent protein serine/threonine kinase regulator activity\",\"qValue\":0.002732205241488788,\"sample\":3,\"total\":10},{\"name\":\"GO:0042098\",\"description\":\"T cell proliferation\",\"qValue\":0.002732205241488788,\"sample\":5,\"total\":71},{\"name\":\"GO:0000987\",\"description\":\"core promoter proximal region sequence-specific DNA binding\",\"qValue\":0.0028230343523024935,\"sample\":4,\"total\":33},{\"name\":\"GO:0001159\",\"description\":\"core promoter proximal region DNA binding\",\"qValue\":0.0030185958219330865,\"sample\":4,\"total\":34},{\"name\":\"GO:2001239\",\"description\":\"regulation of extrinsic apoptotic signaling pathway in absence of ligand\",\"qValue\":0.0030185958219330865,\"sample\":4,\"total\":34},{\"name\":\"GO:0046634\",\"description\":\"regulation of alpha-beta T cell activation\",\"qValue\":0.003148196477916282,\"sample\":4,\"total\":35},{\"name\":\"GO:0050870\",\"description\":\"positive regulation of T cell activation\",\"qValue\":0.003148196477916282,\"sample\":6,\"total\":131},{\"name\":\"GO:0016055\",\"description\":\"Wnt signaling pathway\",\"qValue\":0.003148196477916282,\"sample\":7,\"total\":203},{\"name\":\"GO:0006474\",\"description\":\"N-terminal protein amino acid acetylation\",\"qValue\":0.003148196477916282,\"sample\":3,\"total\":11},{\"name\":\"GO:0030098\",\"description\":\"lymphocyte differentiation\",\"qValue\":0.004973979456054929,\"sample\":6,\"total\":144},{\"name\":\"GO:0030101\",\"description\":\"natural killer cell activation\",\"qValue\":0.004973979456054929,\"sample\":4,\"total\":40},{\"name\":\"GO:0042104\",\"description\":\"positive regulation of activated T cell proliferation\",\"qValue\":0.004973979456054929,\"sample\":3,\"total\":13},{\"name\":\"GO:0002521\",\"description\":\"leukocyte differentiation\",\"qValue\":0.00577184317113666,\"sample\":7,\"total\":226},{\"name\":\"GO:0001916\",\"description\":\"positive regulation of T cell mediated cytotoxicity\",\"qValue\":0.006040218933440558,\"sample\":3,\"total\":14},{\"name\":\"GO:0030217\",\"description\":\"T cell differentiation\",\"qValue\":0.006340667897519708,\"sample\":5,\"total\":90},{\"name\":\"GO:0000976\",\"description\":\"transcription regulatory region sequence-specific DNA binding\",\"qValue\":0.006387682905255724,\"sample\":5,\"total\":91},{\"name\":\"GO:0051251\",\"description\":\"positive regulation of lymphocyte activation\",\"qValue\":0.006387682905255724,\"sample\":6,\"total\":155},{\"name\":\"GO:0030111\",\"description\":\"regulation of Wnt signaling pathway\",\"qValue\":0.006387682905255724,\"sample\":6,\"total\":154},{\"name\":\"GO:0010894\",\"description\":\"negative regulation of steroid biosynthetic process\",\"qValue\":0.006793115273733884,\"sample\":3,\"total\":15},{\"name\":\"GO:0030178\",\"description\":\"negative regulation of Wnt signaling pathway\",\"qValue\":0.007081543048900473,\"sample\":5,\"total\":94},{\"name\":\"GO:0019218\",\"description\":\"regulation of steroid metabolic process\",\"qValue\":0.007186241179825673,\"sample\":4,\"total\":46},{\"name\":\"GO:0032649\",\"description\":\"regulation of interferon-gamma production\",\"qValue\":0.007689164504283003,\"sample\":4,\"total\":47},{\"name\":\"GO:0046825\",\"description\":\"regulation of protein export from nucleus\",\"qValue\":0.007733424931921481,\"sample\":3,\"total\":16},{\"name\":\"GO:0002237\",\"description\":\"response to molecule of bacterial origin\",\"qValue\":0.008148639865763974,\"sample\":5,\"total\":101},{\"name\":\"GO:0045939\",\"description\":\"negative regulation of steroid metabolic process\",\"qValue\":0.008148639865763974,\"sample\":3,\"total\":17},{\"name\":\"GO:0046631\",\"description\":\"alpha-beta T cell activation\",\"qValue\":0.008148639865763974,\"sample\":4,\"total\":49},{\"name\":\"GO:0048534\",\"description\":\"hematopoietic or lymphoid organ development\",\"qValue\":0.008148639865763974,\"sample\":7,\"total\":250},{\"name\":\"GO:0032660\",\"description\":\"regulation of interleukin-17 production\",\"qValue\":0.008148639865763974,\"sample\":3,\"total\":17},{\"name\":\"GO:0002696\",\"description\":\"positive regulation of leukocyte activation\",\"qValue\":0.008148639865763974,\"sample\":6,\"total\":166},{\"name\":\"GO:0001914\",\"description\":\"regulation of T cell mediated cytotoxicity\",\"qValue\":0.008148639865763974,\"sample\":3,\"total\":17},{\"name\":\"GO:0015850\",\"description\":\"organic hydroxy compound transport\",\"qValue\":0.008148639865763974,\"sample\":5,\"total\":101},{\"name\":\"GO:0032620\",\"description\":\"interleukin-17 production\",\"qValue\":0.008148639865763974,\"sample\":3,\"total\":17},{\"name\":\"GO:0045833\",\"description\":\"negative regulation of lipid metabolic process\",\"qValue\":0.008592488004631263,\"sample\":4,\"total\":51},{\"name\":\"GO:0043565\",\"description\":\"sequence-specific DNA binding\",\"qValue\":0.008592488004631263,\"sample\":7,\"total\":255},{\"name\":\"GO:0008013\",\"description\":\"beta-catenin binding\",\"qValue\":0.008592488004631263,\"sample\":4,\"total\":51},{\"name\":\"GO:0045580\",\"description\":\"regulation of T cell differentiation\",\"qValue\":0.009868685420740002,\"sample\":4,\"total\":53},{\"name\":\"GO:0000975\",\"description\":\"regulatory region DNA binding\",\"qValue\":0.00996833882698678,\"sample\":7,\"total\":268},{\"name\":\"GO:0002520\",\"description\":\"immune system development\",\"qValue\":0.00996833882698678,\"sample\":7,\"total\":267},{\"name\":\"GO:0030301\",\"description\":\"cholesterol transport\",\"qValue\":0.00996833882698678,\"sample\":4,\"total\":55},{\"name\":\"GO:0044212\",\"description\":\"transcription regulatory region DNA binding\",\"qValue\":0.00996833882698678,\"sample\":7,\"total\":267},{\"name\":\"GO:0060828\",\"description\":\"regulation of canonical Wnt signaling pathway\",\"qValue\":0.00996833882698678,\"sample\":5,\"total\":108},{\"name\":\"GO:0046006\",\"description\":\"regulation of activated T cell proliferation\",\"qValue\":0.00996833882698678,\"sample\":3,\"total\":19},{\"name\":\"GO:0032368\",\"description\":\"regulation of lipid transport\",\"qValue\":0.00996833882698678,\"sample\":4,\"total\":55},{\"name\":\"GO:0015918\",\"description\":\"sterol transport\",\"qValue\":0.00996833882698678,\"sample\":4,\"total\":55},{\"name\":\"GO:0001067\",\"description\":\"regulatory region nucleic acid binding\",\"qValue\":0.00996833882698678,\"sample\":7,\"total\":268},{\"name\":\"GO:0031365\",\"description\":\"N-terminal protein amino acid modification\",\"qValue\":0.00996833882698678,\"sample\":3,\"total\":19},{\"name\":\"GO:0019901\",\"description\":\"protein kinase binding\",\"qValue\":0.011821355325024898,\"sample\":7,\"total\":276},{\"name\":\"GO:0001912\",\"description\":\"positive regulation of leukocyte mediated cytotoxicity\",\"qValue\":0.012166908991552506,\"sample\":3,\"total\":21},{\"name\":\"GO:0001913\",\"description\":\"T cell mediated cytotoxicity\",\"qValue\":0.012166908991552506,\"sample\":3,\"total\":21},{\"name\":\"GO:0030291\",\"description\":\"protein serine/threonine kinase inhibitor activity\",\"qValue\":0.012166908991552506,\"sample\":3,\"total\":21},{\"name\":\"GO:0007050\",\"description\":\"cell cycle arrest\",\"qValue\":0.012449216911507082,\"sample\":6,\"total\":191},{\"name\":\"GO:0097285\",\"description\":\"cell-type specific apoptotic process\",\"qValue\":0.013413953509683945,\"sample\":6,\"total\":194},{\"name\":\"GO:0034358\",\"description\":\"plasma lipoprotein particle\",\"qValue\":0.013557008481551055,\"sample\":3,\"total\":22},{\"name\":\"GO:0050671\",\"description\":\"positive regulation of lymphocyte proliferation\",\"qValue\":0.01361899411182916,\"sample\":4,\"total\":61},{\"name\":\"GO:0032946\",\"description\":\"positive regulation of mononuclear cell proliferation\",\"qValue\":0.014190299702132796,\"sample\":4,\"total\":62},{\"name\":\"GO:1902107\",\"description\":\"positive regulation of leukocyte differentiation\",\"qValue\":0.014190299702132796,\"sample\":4,\"total\":62},{\"name\":\"GO:0031343\",\"description\":\"positive regulation of cell killing\",\"qValue\":0.014614653656475348,\"sample\":3,\"total\":23},{\"name\":\"GO:0008201\",\"description\":\"heparin binding\",\"qValue\":0.014614653656475348,\"sample\":4,\"total\":63},{\"name\":\"GO:0032994\",\"description\":\"protein-lipid complex\",\"qValue\":0.014614653656475348,\"sample\":3,\"total\":23},{\"name\":\"GO:0070665\",\"description\":\"positive regulation of leukocyte proliferation\",\"qValue\":0.015213982723525123,\"sample\":4,\"total\":64},{\"name\":\"GO:0042129\",\"description\":\"regulation of T cell proliferation\",\"qValue\":0.015213982723525123,\"sample\":4,\"total\":64},{\"name\":\"GO:0000978\",\"description\":\"RNA polymerase II core promoter proximal region sequence-specific DNA binding\",\"qValue\":0.01586311945732622,\"sample\":3,\"total\":24},{\"name\":\"GO:0045862\",\"description\":\"positive regulation of proteolysis\",\"qValue\":0.01586311945732622,\"sample\":4,\"total\":65},{\"name\":\"GO:0006790\",\"description\":\"sulfur compound metabolic process\",\"qValue\":0.017256682750427094,\"sample\":6,\"total\":208},{\"name\":\"GO:0090090\",\"description\":\"negative regulation of canonical Wnt signaling pathway\",\"qValue\":0.018502175830603283,\"sample\":4,\"total\":68},{\"name\":\"GO:0002711\",\"description\":\"positive regulation of T cell mediated immunity\",\"qValue\":0.019655728227315,\"sample\":3,\"total\":26},{\"name\":\"GO:0008589\",\"description\":\"regulation of smoothened signaling pathway\",\"qValue\":0.02183498199396964,\"sample\":3,\"total\":27},{\"name\":\"GO:0010954\",\"description\":\"positive regulation of protein processing\",\"qValue\":0.02367577801683746,\"sample\":4,\"total\":73},{\"name\":\"GO:0002763\",\"description\":\"positive regulation of myeloid leukocyte differentiation\",\"qValue\":0.02367698062489359,\"sample\":3,\"total\":28},{\"name\":\"GO:0001910\",\"description\":\"regulation of leukocyte mediated cytotoxicity\",\"qValue\":0.02367698062489359,\"sample\":3,\"total\":28},{\"name\":\"GO:0032481\",\"description\":\"positive regulation of type I interferon production\",\"qValue\":0.024003393425911977,\"sample\":4,\"total\":74},{\"name\":\"GO:0001085\",\"description\":\"RNA polymerase II transcription factor binding\",\"qValue\":0.024003393425911977,\"sample\":4,\"total\":74},{\"name\":\"GO:0051241\",\"description\":\"negative regulation of multicellular organismal process\",\"qValue\":0.027364436985931044,\"sample\":6,\"total\":230},{\"name\":\"GO:0060070\",\"description\":\"canonical Wnt signaling pathway\",\"qValue\":0.027509147464338503,\"sample\":5,\"total\":145},{\"name\":\"GO:0030097\",\"description\":\"hemopoiesis\",\"qValue\":0.02814804913888909,\"sample\":6,\"total\":232},{\"name\":\"GO:0031341\",\"description\":\"regulation of cell killing\",\"qValue\":0.029631351274316927,\"sample\":3,\"total\":31},{\"name\":\"GO:0046890\",\"description\":\"regulation of lipid biosynthetic process\",\"qValue\":0.029631351274316927,\"sample\":4,\"total\":79},{\"name\":\"GO:0050810\",\"description\":\"regulation of steroid biosynthetic process\",\"qValue\":0.029631351274316927,\"sample\":3,\"total\":31},{\"name\":\"GO:0002824\",\"description\":\"positive regulation of adaptive immune response based on somatic recombination of immune receptors built from immunoglobulin superfamily domains\",\"qValue\":0.029631351274316927,\"sample\":3,\"total\":31},{\"name\":\"GO:0033344\",\"description\":\"cholesterol efflux\",\"qValue\":0.034846276386832936,\"sample\":3,\"total\":33},{\"name\":\"GO:0002708\",\"description\":\"positive regulation of lymphocyte mediated immunity\",\"qValue\":0.034846276386832936,\"sample\":3,\"total\":33},{\"name\":\"GO:0002709\",\"description\":\"regulation of T cell mediated immunity\",\"qValue\":0.034846276386832936,\"sample\":3,\"total\":33},{\"name\":\"GO:0006611\",\"description\":\"protein export from nucleus\",\"qValue\":0.03746638017938125,\"sample\":3,\"total\":34},{\"name\":\"GO:0002821\",\"description\":\"positive regulation of adaptive immune response\",\"qValue\":0.03746638017938125,\"sample\":3,\"total\":34},{\"name\":\"GO:0003713\",\"description\":\"transcription coactivator activity\",\"qValue\":0.03774565896565082,\"sample\":6,\"total\":249},{\"name\":\"GO:0048660\",\"description\":\"regulation of smooth muscle cell proliferation\",\"qValue\":0.03984668927918707,\"sample\":3,\"total\":35},{\"name\":\"GO:0002705\",\"description\":\"positive regulation of leukocyte mediated immunity\",\"qValue\":0.03984668927918707,\"sample\":3,\"total\":35},{\"name\":\"GO:1901681\",\"description\":\"sulfur compound binding\",\"qValue\":0.040313992418270585,\"sample\":4,\"total\":88},{\"name\":\"GO:0005539\",\"description\":\"glycosaminoglycan binding\",\"qValue\":0.040313992418270585,\"sample\":4,\"total\":88},{\"name\":\"GO:0048659\",\"description\":\"smooth muscle cell proliferation\",\"qValue\":0.041951182597891955,\"sample\":3,\"total\":36},{\"name\":\"GO:0010830\",\"description\":\"regulation of myotube differentiation\",\"qValue\":0.041951182597891955,\"sample\":3,\"total\":36},{\"name\":\"GO:0032496\",\"description\":\"response to lipopolysaccharide\",\"qValue\":0.042881829093324046,\"sample\":4,\"total\":90},{\"name\":\"GO:0007259\",\"description\":\"JAK-STAT cascade\",\"qValue\":0.044378904080841296,\"sample\":4,\"total\":91},{\"name\":\"GO:0009617\",\"description\":\"response to bacterium\",\"qValue\":0.04463361295644057,\"sample\":5,\"total\":167},{\"name\":\"GO:0043280\",\"description\":\"positive regulation of cysteine-type endopeptidase activity involved in apoptotic process\",\"qValue\":0.04747009870650572,\"sample\":4,\"total\":93},{\"name\":\"GO:0045596\",\"description\":\"negative regulation of cell differentiation\",\"qValue\":0.05015815362549845,\"sample\":6,\"total\":267},{\"name\":\"GO:0010942\",\"description\":\"positive regulation of cell death\",\"qValue\":0.05077148729737507,\"sample\":6,\"total\":268},{\"name\":\"GO:0002460\",\"description\":\"adaptive immune response based on somatic recombination of immune receptors built from immunoglobulin superfamily domains\",\"qValue\":0.05663278029260644,\"sample\":4,\"total\":98},{\"name\":\"GO:0097194\",\"description\":\"execution phase of apoptosis\",\"qValue\":0.05797209958599959,\"sample\":4,\"total\":99},{\"name\":\"GO:0004860\",\"description\":\"protein kinase inhibitor activity\",\"qValue\":0.05797209958599959,\"sample\":3,\"total\":41},{\"name\":\"GO:2001056\",\"description\":\"positive regulation of cysteine-type endopeptidase activity\",\"qValue\":0.061617928785447765,\"sample\":4,\"total\":101},{\"name\":\"GO:0046165\",\"description\":\"alcohol biosynthetic process\",\"qValue\":0.061617928785447765,\"sample\":4,\"total\":101},{\"name\":\"GO:0051091\",\"description\":\"positive regulation of sequence-specific DNA binding transcription factor activity\",\"qValue\":0.061840151831458204,\"sample\":5,\"total\":182},{\"name\":\"GO:0002456\",\"description\":\"T cell mediated immunity\",\"qValue\":0.06351513007087922,\"sample\":3,\"total\":43},{\"name\":\"GO:0071260\",\"description\":\"cellular response to mechanical stimulus\",\"qValue\":0.06351513007087922,\"sample\":3,\"total\":43},{\"name\":\"GO:0042102\",\"description\":\"positive regulation of T cell proliferation\",\"qValue\":0.06351513007087922,\"sample\":3,\"total\":43},{\"name\":\"GO:0043122\",\"description\":\"regulation of I-kappaB kinase/NF-kappaB signaling\",\"qValue\":0.06471563464484859,\"sample\":5,\"total\":185},{\"name\":\"GO:0032606\",\"description\":\"type I interferon production\",\"qValue\":0.06543553786867076,\"sample\":4,\"total\":104},{\"name\":\"GO:0032479\",\"description\":\"regulation of type I interferon production\",\"qValue\":0.06543553786867076,\"sample\":4,\"total\":104},{\"name\":\"GO:0019210\",\"description\":\"kinase inhibitor activity\",\"qValue\":0.06607838728482696,\"sample\":3,\"total\":44},{\"name\":\"GO:0010950\",\"description\":\"positive regulation of endopeptidase activity\",\"qValue\":0.06690141822388344,\"sample\":4,\"total\":105},{\"name\":\"GO:0034341\",\"description\":\"response to interferon-gamma\",\"qValue\":0.06861787410879093,\"sample\":4,\"total\":106},{\"name\":\"GO:0045786\",\"description\":\"negative regulation of cell cycle\",\"qValue\":0.06861787410879093,\"sample\":6,\"total\":290},{\"name\":\"GO:0090399\",\"description\":\"replicative senescence\",\"qValue\":0.06902887329394419,\"sample\":2,\"total\":10},{\"name\":\"GO:0048261\",\"description\":\"negative regulation of receptor-mediated endocytosis\",\"qValue\":0.06902887329394419,\"sample\":2,\"total\":10},{\"name\":\"GO:0032375\",\"description\":\"negative regulation of cholesterol transport\",\"qValue\":0.06902887329394419,\"sample\":2,\"total\":10},{\"name\":\"GO:0032372\",\"description\":\"negative regulation of sterol transport\",\"qValue\":0.06902887329394419,\"sample\":2,\"total\":10},{\"name\":\"GO:0001909\",\"description\":\"leukocyte mediated cytotoxicity\",\"qValue\":0.07087110742984239,\"sample\":3,\"total\":46},{\"name\":\"GO:0007224\",\"description\":\"smoothened signaling pathway\",\"qValue\":0.07087110742984239,\"sample\":3,\"total\":46},{\"name\":\"GO:0001568\",\"description\":\"blood vessel development\",\"qValue\":0.07175160720382517,\"sample\":5,\"total\":193},{\"name\":\"GO:0010952\",\"description\":\"positive regulation of peptidase activity\",\"qValue\":0.07201836178224848,\"sample\":4,\"total\":109},{\"name\":\"GO:0045639\",\"description\":\"positive regulation of myeloid cell differentiation\",\"qValue\":0.07404465832691144,\"sample\":3,\"total\":47},{\"name\":\"GO:0045165\",\"description\":\"cell fate commitment\",\"qValue\":0.07613269962601123,\"sample\":4,\"total\":111},{\"name\":\"GO:0008202\",\"description\":\"steroid metabolic process\",\"qValue\":0.07672338292910619,\"sample\":5,\"total\":197},{\"name\":\"GO:0014902\",\"description\":\"myotube differentiation\",\"qValue\":0.07679082603091136,\"sample\":3,\"total\":48},{\"name\":\"GO:0002039\",\"description\":\"p53 binding\",\"qValue\":0.07679082603091136,\"sample\":3,\"total\":48},{\"name\":\"GO:0007249\",\"description\":\"I-kappaB kinase/NF-kappaB signaling\",\"qValue\":0.07702032536772613,\"sample\":5,\"total\":198},{\"name\":\"GO:0032376\",\"description\":\"positive regulation of cholesterol transport\",\"qValue\":0.07742423671893217,\"sample\":2,\"total\":11},{\"name\":\"GO:0032373\",\"description\":\"positive regulation of sterol transport\",\"qValue\":0.07742423671893217,\"sample\":2,\"total\":11},{\"name\":\"GO:0032740\",\"description\":\"positive regulation of interleukin-17 production\",\"qValue\":0.07742423671893217,\"sample\":2,\"total\":11},{\"name\":\"GO:0002020\",\"description\":\"protease binding\",\"qValue\":0.0790934392585835,\"sample\":3,\"total\":49},{\"name\":\"GO:0030155\",\"description\":\"regulation of cell adhesion\",\"qValue\":0.07992311516424622,\"sample\":5,\"total\":201},{\"name\":\"GO:0051153\",\"description\":\"regulation of striated muscle cell differentiation\",\"qValue\":0.08735107814589176,\"sample\":3,\"total\":51},{\"name\":\"GO:0030856\",\"description\":\"regulation of epithelial cell differentiation\",\"qValue\":0.08735107814589176,\"sample\":3,\"total\":51},{\"name\":\"GO:0090181\",\"description\":\"regulation of cholesterol metabolic process\",\"qValue\":0.08833833906855541,\"sample\":2,\"total\":12},{\"name\":\"GO:0034435\",\"description\":\"cholesterol esterification\",\"qValue\":0.08833833906855541,\"sample\":2,\"total\":12},{\"name\":\"GO:0034434\",\"description\":\"sterol esterification\",\"qValue\":0.08833833906855541,\"sample\":2,\"total\":12},{\"name\":\"GO:0034433\",\"description\":\"steroid esterification\",\"qValue\":0.08833833906855541,\"sample\":2,\"total\":12},{\"name\":\"GO:2001237\",\"description\":\"negative regulation of extrinsic apoptotic signaling pathway\",\"qValue\":0.09327988798680763,\"sample\":3,\"total\":53},{\"name\":\"GO:0001906\",\"description\":\"cell killing\",\"qValue\":0.09327988798680763,\"sample\":3,\"total\":53},{\"name\":\"GO:0008080\",\"description\":\"N-acetyltransferase activity\",\"qValue\":0.09327988798680763,\"sample\":3,\"total\":53},{\"name\":\"GO:0002706\",\"description\":\"regulation of lymphocyte mediated immunity\",\"qValue\":0.09327988798680763,\"sample\":3,\"total\":53},{\"name\":\"GO:0043235\",\"description\":\"receptor complex\",\"qValue\":0.09685268932352863,\"sample\":5,\"total\":213},{\"name\":\"GO:0071887\",\"description\":\"leukocyte apoptotic process\",\"qValue\":0.09685268932352863,\"sample\":3,\"total\":54},{\"name\":\"GO:0033002\",\"description\":\"muscle cell proliferation\",\"qValue\":0.09685268932352863,\"sample\":3,\"total\":54},{\"name\":\"GO:0004857\",\"description\":\"enzyme inhibitor activity\",\"qValue\":0.09760514834762332,\"sample\":5,\"total\":214},{\"name\":\"GO:0045649\",\"description\":\"regulation of macrophage differentiation\",\"qValue\":0.09783526827980349,\"sample\":2,\"total\":13},{\"name\":\"GO:0034185\",\"description\":\"apolipoprotein binding\",\"qValue\":0.09783526827980349,\"sample\":2,\"total\":13},{\"name\":\"GO:0045879\",\"description\":\"negative regulation of smoothened signaling pathway\",\"qValue\":0.09783526827980349,\"sample\":2,\"total\":13}]",
    "__clusterParams" : [ "adjustLoops", "edgeCutOff=0.0", "undirectedEdges", "converter=None", "dataAttribute=--None--" ],
    "type" : "genemania",
    "search_limit" : 20,
    "shared_name" : "H. sapiens (1)",
    "attribute_search_limit" : 20,
    "__clusterType" : "mcl",
    "Cloud_Counter" : 1,
    "Use_Stemming" : false,
    "combining_method" : "automatic",
    "name" : "H. sapiens genetic data",
    "SUID" : 618,
    "Cloud_Network_UID" : 2,
    "selected" : true,
    "data_version" : "2017-03-17"
  },
  "elements" : {
    "nodes" : [ {
      "data" : {
        "id" : "699",
        "annotations" : [ "" ],
        "annotation_name" : [ "" ],
        "shared_name" : "H__sapiens__1_-727887",
        "score" : 0.9999999999999872,
        "RefSeq_mRNA_ID" : "NM_001287053",
        "node_type" : "query",
        "Entrez_Gene_ID" : "101928448",
        "Ensembl_Gene_ID" : "ENSG00000225940",
        "Gene_title" : "",
        "SUID" : 699,
        "ID" : "",
        "RefSeq_Protein_ID" : "NP_001273982",
        "Uniprot_ID" : "F2Z3F1",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000480926",
        "isExcludedFromPaths" : false,
        "gene_name" : "C5orf67",
        "log_score" : -1.2767564783189382E-14,
        "name" : "H__sapiens__1_-727887",
        "isInPath" : false,
        "Synonym" : ""
      },
      "position" : {
        "x" : 705.5706384234104,
        "y" : 396.5014279168893
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "698",
        "annotations" : [ "" ],
        "annotation_name" : [ "" ],
        "shared_name" : "H__sapiens__1_-717167",
        "score" : 0.9634891195111489,
        "RefSeq_mRNA_ID" : "NM_001143988",
        "node_type" : "query",
        "Entrez_Gene_ID" : "653149",
        "Ensembl_Gene_ID" : "ENSG00000186086",
        "Gene_title" : "",
        "SUID" : 698,
        "ID" : "",
        "RefSeq_Protein_ID" : "NP_001137460",
        "Uniprot_ID" : "Q5VWK0",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000434735",
        "isExcludedFromPaths" : false,
        "gene_name" : "NBPF6",
        "log_score" : -0.03719408386283424,
        "name" : "H__sapiens__1_-717167",
        "isInPath" : false,
        "Synonym" : ""
      },
      "position" : {
        "x" : 982.1575425944848,
        "y" : 699.9028087874988
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "697",
        "annotations" : [ "" ],
        "annotation_name" : [ "" ],
        "shared_name" : "H__sapiens__1_-711718",
        "score" : 0.9499235591766351,
        "RefSeq_mRNA_ID" : "NM_057176",
        "node_type" : "query",
        "Entrez_Gene_ID" : "7809",
        "Ensembl_Gene_ID" : "ENSG00000162399",
        "Gene_title" : "",
        "SUID" : 697,
        "ID" : "",
        "RefSeq_Protein_ID" : "NP_476517",
        "Uniprot_ID" : "Q8WZ55",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000360312",
        "isExcludedFromPaths" : false,
        "gene_name" : "BSND",
        "log_score" : -0.05137376164954849,
        "name" : "H__sapiens__1_-711718",
        "isInPath" : false,
        "Synonym" : "DFNB73"
      },
      "position" : {
        "x" : 1035.962232676662,
        "y" : 561.0168938738373
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "696",
        "annotations" : [ "" ],
        "annotation_name" : [ "" ],
        "shared_name" : "H__sapiens__1_-746012",
        "score" : 0.9999999999999872,
        "RefSeq_mRNA_ID" : "",
        "node_type" : "query",
        "Entrez_Gene_ID" : "",
        "Ensembl_Gene_ID" : "ENSG00000255524",
        "Gene_title" : "",
        "SUID" : 696,
        "ID" : "",
        "RefSeq_Protein_ID" : "NP_001297065",
        "Uniprot_ID" : "E9PQR5",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000434399",
        "isExcludedFromPaths" : false,
        "gene_name" : "NPIPB8",
        "log_score" : -1.2767564783189382E-14,
        "name" : "H__sapiens__1_-746012",
        "isInPath" : false,
        "Synonym" : ""
      },
      "position" : {
        "x" : 705.5706384234109,
        "y" : 725.5323598307855
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "695",
        "B" : 28.68829,
        "annotations" : [ "" ],
        "annotation_name" : [ "" ],
        "shared_name" : "H__sapiens__1_-712410",
        "score" : 0.942663940864362,
        "RefSeq_mRNA_ID" : "NM_001033723",
        "node_type" : "query",
        "Entrez_Gene_ID" : "619279",
        "Ensembl_Gene_ID" : "ENSG00000164684",
        "Gene_title" : "zinc finger protein 704",
        "SUID" : 695,
        "ID" : "ILMN_1849186",
        "__mclCluster" : 4,
        "RefSeq_Protein_ID" : "NP_001028895",
        "Uniprot_ID" : "Q6ZNC4",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000427715",
        "logFC" : -0.9658598,
        "isExcludedFromPaths" : false,
        "P_Value" : 4.93E-17,
        "adj_P_Val" : 8.47E-16,
        "gene_name" : "ZNF704",
        "t" : -19.634754,
        "log_score" : -0.05904543222456726,
        "name" : "H__sapiens__1_-712410",
        "isInPath" : false,
        "Synonym" : "FLJ16218"
      },
      "position" : {
        "x" : 466.2417907714844,
        "y" : -350.21362060410894
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "694",
        "annotations" : [ "" ],
        "annotation_name" : [ "" ],
        "shared_name" : "H__sapiens__1_-713636",
        "score" : 0.01769722421174963,
        "RefSeq_mRNA_ID" : "",
        "node_type" : "result",
        "Entrez_Gene_ID" : "",
        "Ensembl_Gene_ID" : "ENSG00000169618",
        "Gene_title" : "",
        "SUID" : 694,
        "ID" : "",
        "RefSeq_Protein_ID" : "",
        "Uniprot_ID" : "Q8TCW9",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000303775",
        "isExcludedFromPaths" : false,
        "gene_name" : "PROKR1",
        "log_score" : -4.03434747589549,
        "name" : "H__sapiens__1_-713636",
        "isInPath" : false,
        "Synonym" : "ZAQ"
      },
      "position" : {
        "x" : 773.3898807013011,
        "y" : 362.7314578623787
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "693",
        "B" : 29.36006,
        "annotations" : [ "GO:0019901", "GO:0006367", "GO:0019210", "GO:0002520", "GO:0004857", "GO:0048534", "GO:0004860", "GO:0007050", "GO:0030291", "GO:0030097", "GO:0016538", "GO:0045786", "GO:0006352" ],
        "annotation_name" : [ "protein kinase binding", "transcription initiation from RNA polymerase II promoter", "kinase inhibitor activity", "immune system development", "enzyme inhibitor activity", "hematopoietic or lymphoid organ development", "protein kinase inhibitor activity", "cell cycle arrest", "protein serine/threonine kinase inhibitor activity", "hemopoiesis", "cyclin-dependent protein serine/threonine kinase regulator activity", "negative regulation of cell cycle", "DNA-templated transcription, initiation" ],
        "shared_name" : "H__sapiens__1_-710026",
        "score" : 0.7874021496605643,
        "RefSeq_mRNA_ID" : "NM_078487",
        "node_type" : "query",
        "Entrez_Gene_ID" : "1030",
        "Ensembl_Gene_ID" : "ENSG00000147883",
        "Gene_title" : "cyclin dependent kinase inhibitor 2B",
        "SUID" : 693,
        "ID" : "ILMN_2376723",
        "__mclCluster" : 7,
        "RefSeq_Protein_ID" : "NP_511042",
        "Uniprot_ID" : "P42772",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000369487",
        "logFC" : -1.9857078,
        "isExcludedFromPaths" : false,
        "P_Value" : 2.56E-17,
        "adj_P_Val" : 4.79E-16,
        "gene_name" : "CDKN2B",
        "t" : -20.171534,
        "log_score" : -0.23901617040184972,
        "name" : "H__sapiens__1_-710026",
        "isInPath" : false,
        "Synonym" : "TP15"
      },
      "position" : {
        "x" : 2130.209711911343,
        "y" : -328.9954504380933
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "692",
        "B" : 6.51456,
        "annotations" : [ "GO:0050670", "GO:1902105", "GO:0032944", "GO:0030098", "GO:0050865", "GO:0045619", "GO:0046651", "GO:0070661", "GO:0043565", "GO:0002694", "GO:0051249", "GO:0032943", "GO:0002521", "GO:0070663" ],
        "annotation_name" : [ "regulation of lymphocyte proliferation", "regulation of leukocyte differentiation", "regulation of mononuclear cell proliferation", "lymphocyte differentiation", "regulation of cell activation", "regulation of lymphocyte differentiation", "lymphocyte proliferation", "leukocyte proliferation", "sequence-specific DNA binding", "regulation of leukocyte activation", "regulation of lymphocyte activation", "mononuclear cell proliferation", "leukocyte differentiation", "regulation of leukocyte proliferation" ],
        "shared_name" : "H__sapiens__1_-711599",
        "score" : 0.7950702528765123,
        "RefSeq_mRNA_ID" : "NM_183232",
        "node_type" : "query",
        "Entrez_Gene_ID" : "22806",
        "Ensembl_Gene_ID" : "ENSG00000161405",
        "Gene_title" : "IKAROS family zinc finger 3",
        "SUID" : 692,
        "ID" : "ILMN_1669692",
        "__mclCluster" : 4,
        "RefSeq_Protein_ID" : "NP_899055",
        "Uniprot_ID" : "Q9UKT9",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000485515",
        "logFC" : 0.2233332,
        "isExcludedFromPaths" : false,
        "P_Value" : 1.19E-7,
        "adj_P_Val" : 3.43E-7,
        "gene_name" : "IKZF3",
        "t" : 7.224094,
        "log_score" : -0.2293247998339333,
        "name" : "H__sapiens__1_-711599",
        "isInPath" : false,
        "Synonym" : "ZNFN1A3"
      },
      "position" : {
        "x" : 421.50513228203636,
        "y" : -245.83418164934355
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "691",
        "annotations" : [ "GO:0050870", "GO:0032944", "GO:0042129", "GO:0046634", "GO:0051251", "GO:0032609", "GO:0046651", "GO:0001906", "GO:0042102", "GO:0050671", "GO:0001910", "GO:0002696", "GO:0002521", "GO:0032660", "GO:0032649", "GO:0002705", "GO:0046631", "GO:0032496", "GO:1902105", "GO:1902107", "GO:0050863", "GO:0042104", "GO:0032620", "GO:0002456", "GO:0001912", "GO:0045165", "GO:0070665", "GO:0043235", "GO:0002237", "GO:0002821", "GO:0050670", "GO:0001913", "GO:0001914", "GO:0034341", "GO:0001819", "GO:0045619", "GO:0002709", "GO:0002711", "GO:0032740", "GO:0070661", "GO:0050798", "GO:0001909", "GO:0002460", "GO:0002824", "GO:0050867", "GO:0002706", "GO:0032946", "GO:0051241", "GO:0046006", "GO:0002763", "GO:0045639", "GO:0009617", "GO:0046635", "GO:0030217", "GO:0030098", "GO:0050865", "GO:0007259", "GO:0002708", "GO:0042110", "GO:0032729", "GO:0031343", "GO:0001916", "GO:0030101", "GO:0045580", "GO:0002694", "GO:0051249", "GO:0032943", "GO:0042098", "GO:0070663", "GO:0031341" ],
        "annotation_name" : [ "positive regulation of T cell activation", "regulation of mononuclear cell proliferation", "regulation of T cell proliferation", "regulation of alpha-beta T cell activation", "positive regulation of lymphocyte activation", "interferon-gamma production", "lymphocyte proliferation", "cell killing", "positive regulation of T cell proliferation", "positive regulation of lymphocyte proliferation", "regulation of leukocyte mediated cytotoxicity", "positive regulation of leukocyte activation", "leukocyte differentiation", "regulation of interleukin-17 production", "regulation of interferon-gamma production", "positive regulation of leukocyte mediated immunity", "alpha-beta T cell activation", "response to lipopolysaccharide", "regulation of leukocyte differentiation", "positive regulation of leukocyte differentiation", "regulation of T cell activation", "positive regulation of activated T cell proliferation", "interleukin-17 production", "T cell mediated immunity", "positive regulation of leukocyte mediated cytotoxicity", "cell fate commitment", "positive regulation of leukocyte proliferation", "receptor complex", "response to molecule of bacterial origin", "positive regulation of adaptive immune response", "regulation of lymphocyte proliferation", "T cell mediated cytotoxicity", "regulation of T cell mediated cytotoxicity", "response to interferon-gamma", "positive regulation of cytokine production", "regulation of lymphocyte differentiation", "regulation of T cell mediated immunity", "positive regulation of T cell mediated immunity", "positive regulation of interleukin-17 production", "leukocyte proliferation", "activated T cell proliferation", "leukocyte mediated cytotoxicity", "adaptive immune response based on somatic recombination of immune receptors built from immunoglobulin superfamily domains", "positive regulation of adaptive immune response based on somatic recombination of immune receptors built from immunoglobulin superfamily domains", "positive regulation of cell activation", "regulation of lymphocyte mediated immunity", "positive regulation of mononuclear cell proliferation", "negative regulation of multicellular organismal process", "regulation of activated T cell proliferation", "positive regulation of myeloid leukocyte differentiation", "positive regulation of myeloid cell differentiation", "response to bacterium", "positive regulation of alpha-beta T cell activation", "T cell differentiation", "lymphocyte differentiation", "regulation of cell activation", "JAK-STAT cascade", "positive regulation of lymphocyte mediated immunity", "T cell activation", "positive regulation of interferon-gamma production", "positive regulation of cell killing", "positive regulation of T cell mediated cytotoxicity", "natural killer cell activation", "regulation of T cell differentiation", "regulation of leukocyte activation", "regulation of lymphocyte activation", "mononuclear cell proliferation", "T cell proliferation", "regulation of leukocyte proliferation", "regulation of cell killing" ],
        "shared_name" : "H__sapiens__1_-711762",
        "score" : 0.8659136771583043,
        "RefSeq_mRNA_ID" : "NM_144701",
        "node_type" : "query",
        "Entrez_Gene_ID" : "149233",
        "Ensembl_Gene_ID" : "ENSG00000162594",
        "Gene_title" : "",
        "SUID" : 691,
        "ID" : "",
        "__mclCluster" : 2,
        "RefSeq_Protein_ID" : "NP_653302",
        "Uniprot_ID" : "Q5VWK5",
        "selected" : true,
        "Ensembl_Protein_ID" : "ENSP00000490340",
        "isExcludedFromPaths" : false,
        "gene_name" : "IL23R",
        "log_score" : -0.14397005534384763,
        "name" : "H__sapiens__1_-711762",
        "isInPath" : false,
        "Synonym" : "IL-23R"
      },
      "position" : {
        "x" : -777.861230087472,
        "y" : -91.70354489821779
      },
      "selected" : true
    }, {
      "data" : {
        "id" : "690",
        "B" : -8.20001,
        "annotations" : [ "" ],
        "annotation_name" : [ "" ],
        "shared_name" : "H__sapiens__1_-713226",
        "score" : 0.9343315835022943,
        "RefSeq_mRNA_ID" : "NM_178171",
        "node_type" : "query",
        "Entrez_Gene_ID" : "284110",
        "Ensembl_Gene_ID" : "ENSG00000167914",
        "Gene_title" : "gasdermin A",
        "SUID" : 690,
        "ID" : "ILMN_2115974",
        "RefSeq_Protein_ID" : "NP_835465",
        "Uniprot_ID" : "Q96QA5",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000490739",
        "logFC" : 0.0144286,
        "isExcludedFromPaths" : false,
        "P_Value" : 0.679,
        "adj_P_Val" : 0.71,
        "gene_name" : "GSDMA",
        "t" : 0.419124,
        "log_score" : -0.06792388929959789,
        "name" : "H__sapiens__1_-713226",
        "isInPath" : false,
        "Synonym" : "GSDM1"
      },
      "position" : {
        "x" : 848.8285609942614,
        "y" : 355.7410346787461
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "689",
        "annotations" : [ "GO:0005539", "GO:0008201", "GO:1901681" ],
        "annotation_name" : [ "glycosaminoglycan binding", "heparin binding", "sulfur compound binding" ],
        "shared_name" : "H__sapiens__1_-717747",
        "score" : 0.9157364274590671,
        "RefSeq_mRNA_ID" : "NM_198721",
        "node_type" : "query",
        "Entrez_Gene_ID" : "84570",
        "Ensembl_Gene_ID" : "ENSG00000188517",
        "Gene_title" : "",
        "SUID" : 689,
        "ID" : "",
        "__mclCluster" : 1,
        "RefSeq_Protein_ID" : "NP_942014",
        "Uniprot_ID" : "Q9BXS0",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000484110",
        "isExcludedFromPaths" : false,
        "gene_name" : "COL25A1",
        "log_score" : -0.08802669866256872,
        "name" : "H__sapiens__1_-717747",
        "isInPath" : false,
        "Synonym" : ""
      },
      "position" : {
        "x" : -1583.2320849380149,
        "y" : -335.8705729567558
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "688",
        "B" : 12.52175,
        "annotations" : [ "" ],
        "annotation_name" : [ "" ],
        "shared_name" : "H__sapiens__1_-707211",
        "score" : 0.694377362244087,
        "RefSeq_mRNA_ID" : "NM_006114",
        "node_type" : "query",
        "Entrez_Gene_ID" : "10452",
        "Ensembl_Gene_ID" : "ENSG00000130204",
        "Gene_title" : "translocase of outer mitochondrial membrane 40",
        "SUID" : 688,
        "ID" : "ILMN_1683475",
        "__mclCluster" : 1,
        "RefSeq_Protein_ID" : "NP_006105",
        "Uniprot_ID" : "O96008",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000466084",
        "logFC" : 0.5345991,
        "isExcludedFromPaths" : false,
        "P_Value" : 3.35E-10,
        "adj_P_Val" : 1.41E-9,
        "gene_name" : "TOMM40",
        "t" : 9.820087,
        "log_score" : -0.3647397166223469,
        "name" : "H__sapiens__1_-707211",
        "isInPath" : false,
        "Synonym" : "TOM40"
      },
      "position" : {
        "x" : -1668.2070605239524,
        "y" : -319.24897185201945
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "687",
        "B" : -7.92865,
        "annotations" : [ "GO:0090090", "GO:0051091", "GO:0060828", "GO:0030178", "GO:0060070", "GO:0030111", "GO:0016055" ],
        "annotation_name" : [ "negative regulation of canonical Wnt signaling pathway", "positive regulation of sequence-specific DNA binding transcription factor activity", "regulation of canonical Wnt signaling pathway", "negative regulation of Wnt signaling pathway", "canonical Wnt signaling pathway", "regulation of Wnt signaling pathway", "Wnt signaling pathway" ],
        "shared_name" : "H__sapiens__1_-711077",
        "score" : 0.026603628837674476,
        "RefSeq_mRNA_ID" : "NM_003505",
        "node_type" : "result",
        "Entrez_Gene_ID" : "8321",
        "Ensembl_Gene_ID" : "ENSG00000157240",
        "Gene_title" : "frizzled class receptor 1",
        "SUID" : 687,
        "ID" : "ILMN_1696546",
        "__mclCluster" : 5,
        "RefSeq_Protein_ID" : "NP_003496",
        "Uniprot_ID" : "Q9UP38",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000287934",
        "logFC" : -0.0301925,
        "isExcludedFromPaths" : false,
        "P_Value" : 0.408,
        "adj_P_Val" : 0.45,
        "gene_name" : "FZD1",
        "t" : -0.841062,
        "log_score" : -3.6267076500302173,
        "name" : "H__sapiens__1_-711077",
        "isInPath" : false,
        "Synonym" : "DKFZp564G072"
      },
      "position" : {
        "x" : 1018.0638452162035,
        "y" : -394.95028442246837
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "686",
        "B" : -4.39258,
        "annotations" : [ "GO:0008589", "GO:0045879", "GO:0045862", "GO:0010954", "GO:0007224" ],
        "annotation_name" : [ "regulation of smoothened signaling pathway", "negative regulation of smoothened signaling pathway", "positive regulation of proteolysis", "positive regulation of protein processing", "smoothened signaling pathway" ],
        "shared_name" : "H__sapiens__1_-712776",
        "score" : 0.5398155339062415,
        "RefSeq_mRNA_ID" : "NM_033637",
        "node_type" : "query",
        "Entrez_Gene_ID" : "8945",
        "Ensembl_Gene_ID" : "ENSG00000166167",
        "Gene_title" : "beta-transducin repeat containing E3 ubiquitin protein ligase",
        "SUID" : 686,
        "ID" : "ILMN_1815718",
        "__mclCluster" : 5,
        "RefSeq_Protein_ID" : "NP_378663",
        "Uniprot_ID" : "Q9Y297",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000385339",
        "logFC" : 0.1937327,
        "isExcludedFromPaths" : false,
        "P_Value" : 0.00663,
        "adj_P_Val" : 0.00976,
        "gene_name" : "BTRC",
        "t" : 2.95276,
        "log_score" : -0.616527801661043,
        "name" : "H__sapiens__1_-712776",
        "isInPath" : false,
        "Synonym" : "Fwd1"
      },
      "position" : {
        "x" : 1021.5051322820364,
        "y" : -245.83418164934355
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "685",
        "B" : -1.25049,
        "annotations" : [ "GO:0010942" ],
        "annotation_name" : [ "positive regulation of cell death" ],
        "shared_name" : "H__sapiens__1_-746649",
        "score" : 0.7631950962817367,
        "RefSeq_mRNA_ID" : "NM_005143",
        "node_type" : "query",
        "Entrez_Gene_ID" : "3240",
        "Ensembl_Gene_ID" : "ENSG00000257017",
        "Gene_title" : "haptoglobin",
        "SUID" : 685,
        "ID" : "ILMN_1812433",
        "__mclCluster" : 3,
        "RefSeq_Protein_ID" : "NP_005134",
        "Uniprot_ID" : "P00738",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000478279",
        "logFC" : -0.6133144,
        "isExcludedFromPaths" : false,
        "P_Value" : 2.63E-4,
        "adj_P_Val" : 4.81E-4,
        "gene_name" : "HP",
        "t" : -4.224982,
        "log_score" : -0.27024158407607013,
        "name" : "H__sapiens__1_-746649",
        "isInPath" : false,
        "Synonym" : ""
      },
      "position" : {
        "x" : -128.49486771796364,
        "y" : -432.4367220277874
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "684",
        "annotations" : [ "GO:0050870", "GO:0097191", "GO:0032944", "GO:0046634", "GO:0051251", "GO:0032609", "GO:0046651", "GO:0001906", "GO:0007050", "GO:0050671", "GO:0001910", "GO:0048659", "GO:0002696", "GO:0033002", "GO:0010942", "GO:0045786", "GO:0032660", "GO:0032649", "GO:0002705", "GO:0046631", "GO:0032496", "GO:0050863", "GO:0030155", "GO:0032620", "GO:0002456", "GO:0001912", "GO:0070665", "GO:0002237", "GO:0002821", "GO:0050670", "GO:0001913", "GO:0001914", "GO:0001819", "GO:0002709", "GO:0002711", "GO:0070661", "GO:0048660", "GO:0001909", "GO:0002460", "GO:0002824", "GO:0050867", "GO:0002706", "GO:0032946", "GO:0051241", "GO:0097285", "GO:0009617", "GO:0046635", "GO:0050865", "GO:0007259", "GO:0002708", "GO:0042110", "GO:0032729", "GO:0031343", "GO:0001916", "GO:0030101", "GO:0002694", "GO:0051249", "GO:0032943", "GO:0070663", "GO:0031341" ],
        "annotation_name" : [ "positive regulation of T cell activation", "extrinsic apoptotic signaling pathway", "regulation of mononuclear cell proliferation", "regulation of alpha-beta T cell activation", "positive regulation of lymphocyte activation", "interferon-gamma production", "lymphocyte proliferation", "cell killing", "cell cycle arrest", "positive regulation of lymphocyte proliferation", "regulation of leukocyte mediated cytotoxicity", "smooth muscle cell proliferation", "positive regulation of leukocyte activation", "muscle cell proliferation", "positive regulation of cell death", "negative regulation of cell cycle", "regulation of interleukin-17 production", "regulation of interferon-gamma production", "positive regulation of leukocyte mediated immunity", "alpha-beta T cell activation", "response to lipopolysaccharide", "regulation of T cell activation", "regulation of cell adhesion", "interleukin-17 production", "T cell mediated immunity", "positive regulation of leukocyte mediated cytotoxicity", "positive regulation of leukocyte proliferation", "response to molecule of bacterial origin", "positive regulation of adaptive immune response", "regulation of lymphocyte proliferation", "T cell mediated cytotoxicity", "regulation of T cell mediated cytotoxicity", "positive regulation of cytokine production", "regulation of T cell mediated immunity", "positive regulation of T cell mediated immunity", "leukocyte proliferation", "regulation of smooth muscle cell proliferation", "leukocyte mediated cytotoxicity", "adaptive immune response based on somatic recombination of immune receptors built from immunoglobulin superfamily domains", "positive regulation of adaptive immune response based on somatic recombination of immune receptors built from immunoglobulin superfamily domains", "positive regulation of cell activation", "regulation of lymphocyte mediated immunity", "positive regulation of mononuclear cell proliferation", "negative regulation of multicellular organismal process", "cell-type specific apoptotic process", "response to bacterium", "positive regulation of alpha-beta T cell activation", "regulation of cell activation", "JAK-STAT cascade", "positive regulation of lymphocyte mediated immunity", "T cell activation", "positive regulation of interferon-gamma production", "positive regulation of cell killing", "positive regulation of T cell mediated cytotoxicity", "natural killer cell activation", "regulation of leukocyte activation", "regulation of lymphocyte activation", "mononuclear cell proliferation", "regulation of leukocyte proliferation", "regulation of cell killing" ],
        "shared_name" : "H__sapiens__1_-713444",
        "score" : 0.7383838151674472,
        "RefSeq_mRNA_ID" : "NM_000882",
        "node_type" : "query",
        "Entrez_Gene_ID" : "3592",
        "Ensembl_Gene_ID" : "ENSG00000168811",
        "Gene_title" : "",
        "SUID" : 684,
        "ID" : "",
        "__mclCluster" : 8,
        "RefSeq_Protein_ID" : "NP_000873",
        "Uniprot_ID" : "P29459",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000420184",
        "isExcludedFromPaths" : false,
        "gene_name" : "IL12A",
        "log_score" : -0.30329151482102895,
        "name" : "H__sapiens__1_-713444",
        "isInPath" : false,
        "Synonym" : "NKSF1"
      },
      "position" : {
        "x" : 1361.1041708433695,
        "y" : 436.8726806640625
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "683",
        "B" : -8.26884,
        "annotations" : [ "" ],
        "annotation_name" : [ "" ],
        "shared_name" : "H__sapiens__1_-701859",
        "score" : 0.7516699709451887,
        "RefSeq_mRNA_ID" : "NM_012470",
        "node_type" : "query",
        "Entrez_Gene_ID" : "23534",
        "Ensembl_Gene_ID" : "ENSG00000064419",
        "Gene_title" : "transportin 3",
        "SUID" : 683,
        "ID" : "ILMN_1683811",
        "RefSeq_Protein_ID" : "NP_036602",
        "Uniprot_ID" : "Q9Y5L0",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000487231",
        "logFC" : 0.0128372,
        "isExcludedFromPaths" : false,
        "P_Value" : 0.838,
        "adj_P_Val" : 0.857,
        "gene_name" : "TNPO3",
        "t" : 0.206124,
        "log_score" : -0.2854579197871806,
        "name" : "H__sapiens__1_-701859",
        "isInPath" : false,
        "Synonym" : "TRN-SR2"
      },
      "position" : {
        "x" : 654.5301986296136,
        "y" : 669.5436644871222
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "682",
        "B" : -1.06563,
        "annotations" : [ "GO:0097191", "GO:0019901", "GO:0006611", "GO:0045596", "GO:0030178", "GO:0002039", "GO:0097192", "GO:0038034", "GO:2001233", "GO:0030155", "GO:0090090", "GO:0001085", "GO:0060828", "GO:0030856", "GO:0060070", "GO:0030111", "GO:0016055", "GO:0008013", "GO:0046825" ],
        "annotation_name" : [ "extrinsic apoptotic signaling pathway", "protein kinase binding", "protein export from nucleus", "negative regulation of cell differentiation", "negative regulation of Wnt signaling pathway", "p53 binding", "extrinsic apoptotic signaling pathway in absence of ligand", "signal transduction in absence of ligand", "regulation of apoptotic signaling pathway", "regulation of cell adhesion", "negative regulation of canonical Wnt signaling pathway", "RNA polymerase II transcription factor binding", "regulation of canonical Wnt signaling pathway", "regulation of epithelial cell differentiation", "canonical Wnt signaling pathway", "regulation of Wnt signaling pathway", "Wnt signaling pathway", "beta-catenin binding", "regulation of protein export from nucleus" ],
        "shared_name" : "H__sapiens__1_-702553",
        "score" : 0.6130086762129904,
        "RefSeq_mRNA_ID" : "NM_002093",
        "node_type" : "query",
        "Entrez_Gene_ID" : "2932",
        "Ensembl_Gene_ID" : "ENSG00000082701",
        "Gene_title" : "glycogen synthase kinase 3 beta",
        "SUID" : 682,
        "ID" : "ILMN_1779376",
        "__mclCluster" : 2,
        "RefSeq_Protein_ID" : "NP_002084",
        "Uniprot_ID" : "P49841",
        "selected" : true,
        "Ensembl_Protein_ID" : "ENSP00000324806",
        "logFC" : -0.2122673,
        "isExcludedFromPaths" : false,
        "P_Value" : 2.18E-4,
        "adj_P_Val" : 4.04E-4,
        "gene_name" : "GSK3B",
        "t" : -4.296217,
        "log_score" : -0.4893761894544236,
        "name" : "H__sapiens__1_-702553",
        "isInPath" : false,
        "Synonym" : ""
      },
      "position" : {
        "x" : -695.3985595703125,
        "y" : -252.95488662311737
      },
      "selected" : true
    }, {
      "data" : {
        "id" : "681",
        "annotations" : [ "" ],
        "annotation_name" : [ "" ],
        "shared_name" : "H__sapiens__1_-710479",
        "score" : 0.8815295845555062,
        "RefSeq_mRNA_ID" : "NM_080671",
        "node_type" : "query",
        "Entrez_Gene_ID" : "23704",
        "Ensembl_Gene_ID" : "ENSG00000152049",
        "Gene_title" : "",
        "SUID" : 681,
        "ID" : "",
        "__mclCluster" : 6,
        "RefSeq_Protein_ID" : "NP_542402",
        "Uniprot_ID" : "Q8WWG9",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000281830",
        "isExcludedFromPaths" : false,
        "gene_name" : "KCNE4",
        "log_score" : -0.12609671610159756,
        "name" : "H__sapiens__1_-710479",
        "isInPath" : false,
        "Synonym" : "MiRP3"
      },
      "position" : {
        "x" : 1614.62255859375,
        "y" : -357.0961633314379
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "680",
        "annotations" : [ "GO:0097194", "GO:0050670", "GO:0006611", "GO:0071887", "GO:0032944", "GO:0010952", "GO:0042129", "GO:0019210", "GO:0043280", "GO:0002039", "GO:0045619", "GO:0046651", "GO:0070661", "GO:0004857", "GO:0007050", "GO:2001056", "GO:0002521", "GO:0010942", "GO:0033077", "GO:0016538", "GO:0045786", "GO:0097285", "GO:0019901", "GO:1902105", "GO:0030217", "GO:0030098", "GO:0050865", "GO:0050863", "GO:0010950", "GO:0042110", "GO:0030155", "GO:0004860", "GO:0045580", "GO:0090399", "GO:0002694", "GO:0051249", "GO:0032943", "GO:0042098", "GO:0030291", "GO:0070663", "GO:0046825" ],
        "annotation_name" : [ "execution phase of apoptosis", "regulation of lymphocyte proliferation", "protein export from nucleus", "leukocyte apoptotic process", "regulation of mononuclear cell proliferation", "positive regulation of peptidase activity", "regulation of T cell proliferation", "kinase inhibitor activity", "positive regulation of cysteine-type endopeptidase activity involved in apoptotic process", "p53 binding", "regulation of lymphocyte differentiation", "lymphocyte proliferation", "leukocyte proliferation", "enzyme inhibitor activity", "cell cycle arrest", "positive regulation of cysteine-type endopeptidase activity", "leukocyte differentiation", "positive regulation of cell death", "T cell differentiation in thymus", "cyclin-dependent protein serine/threonine kinase regulator activity", "negative regulation of cell cycle", "cell-type specific apoptotic process", "protein kinase binding", "regulation of leukocyte differentiation", "T cell differentiation", "lymphocyte differentiation", "regulation of cell activation", "regulation of T cell activation", "positive regulation of endopeptidase activity", "T cell activation", "regulation of cell adhesion", "protein kinase inhibitor activity", "regulation of T cell differentiation", "replicative senescence", "regulation of leukocyte activation", "regulation of lymphocyte activation", "mononuclear cell proliferation", "T cell proliferation", "protein serine/threonine kinase inhibitor activity", "regulation of leukocyte proliferation", "regulation of protein export from nucleus" ],
        "shared_name" : "H__sapiens__1_-710028",
        "score" : 0.7111267643847011,
        "RefSeq_mRNA_ID" : "NM_058195",
        "node_type" : "query",
        "Entrez_Gene_ID" : "1029",
        "Ensembl_Gene_ID" : "ENSG00000147889",
        "Gene_title" : "",
        "SUID" : 680,
        "ID" : "",
        "RefSeq_Protein_ID" : "NP_478104",
        "Uniprot_ID" : "Q8N726",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000468510",
        "isExcludedFromPaths" : false,
        "gene_name" : "CDKN2A",
        "log_score" : -0.3409045747968769,
        "name" : "H__sapiens__1_-710028",
        "isInPath" : false,
        "Synonym" : "p19Arf"
      },
      "position" : {
        "x" : 921.6982675085696,
        "y" : 745.5595339954612
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "679",
        "B" : 6.45733,
        "annotations" : [ "" ],
        "annotation_name" : [ "" ],
        "shared_name" : "H__sapiens__1_-701028",
        "score" : 0.7155277337361641,
        "RefSeq_mRNA_ID" : "NM_005777",
        "node_type" : "query",
        "Entrez_Gene_ID" : "10180",
        "Ensembl_Gene_ID" : "ENSG00000004534",
        "Gene_title" : "RNA binding motif protein 6",
        "SUID" : 679,
        "ID" : "ILMN_1738239",
        "__mclCluster" : 9,
        "RefSeq_Protein_ID" : "NP_005768",
        "Uniprot_ID" : "P78332",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000408665",
        "logFC" : -0.6008362,
        "isExcludedFromPaths" : false,
        "P_Value" : 1.26E-7,
        "adj_P_Val" : 3.61E-7,
        "gene_name" : "RBM6",
        "t" : -7.201008,
        "log_score" : -0.33473491940047473,
        "name" : "H__sapiens__1_-701028",
        "isInPath" : false,
        "Synonym" : "NY-LU-12"
      },
      "position" : {
        "x" : 1894.621004016386,
        "y" : 441.62158203125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "678",
        "annotations" : [ "GO:0019901", "GO:0006367", "GO:0008080", "GO:0031365", "GO:0019210", "GO:0006474", "GO:0007219", "GO:0004857", "GO:0004860", "GO:0007050", "GO:0003713", "GO:0030291", "GO:0016538", "GO:0045786", "GO:0006352" ],
        "annotation_name" : [ "protein kinase binding", "transcription initiation from RNA polymerase II promoter", "N-acetyltransferase activity", "N-terminal protein amino acid modification", "kinase inhibitor activity", "N-terminal protein amino acid acetylation", "Notch signaling pathway", "enzyme inhibitor activity", "protein kinase inhibitor activity", "cell cycle arrest", "transcription coactivator activity", "protein serine/threonine kinase inhibitor activity", "cyclin-dependent protein serine/threonine kinase regulator activity", "negative regulation of cell cycle", "DNA-templated transcription, initiation" ],
        "shared_name" : "H__sapiens__1_-705245",
        "score" : 0.5874906468166492,
        "RefSeq_mRNA_ID" : "NM_003884",
        "node_type" : "query",
        "Entrez_Gene_ID" : "8850",
        "Ensembl_Gene_ID" : "ENSG00000114166",
        "Gene_title" : "",
        "SUID" : 678,
        "ID" : "",
        "__mclCluster" : 1,
        "RefSeq_Protein_ID" : "NP_003875",
        "Uniprot_ID" : "Q92831",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000263754",
        "isExcludedFromPaths" : false,
        "gene_name" : "KAT2B",
        "log_score" : -0.531894953402639,
        "name" : "H__sapiens__1_-705245",
        "isInPath" : false,
        "Synonym" : "PCAF"
      },
      "position" : {
        "x" : -1506.7984619140625,
        "y" : -166.78614157505365
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "677",
        "B" : 0.77768,
        "annotations" : [ "GO:0007219", "GO:0007220", "GO:0001568" ],
        "annotation_name" : [ "Notch signaling pathway", "Notch receptor processing", "blood vessel development" ],
        "shared_name" : "H__sapiens__1_-707068",
        "score" : 0.026934126777366396,
        "RefSeq_mRNA_ID" : "NM_019074",
        "node_type" : "result",
        "Entrez_Gene_ID" : "54567",
        "Ensembl_Gene_ID" : "ENSG00000128917",
        "Gene_title" : "delta like canonical Notch ligand 4",
        "SUID" : 677,
        "ID" : "ILMN_1690174",
        "__mclCluster" : 1,
        "RefSeq_Protein_ID" : "NP_061947",
        "Uniprot_ID" : "Q9NR61",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000249749",
        "logFC" : -0.1536865,
        "isExcludedFromPaths" : false,
        "P_Value" : 3.44E-5,
        "adj_P_Val" : 7.12E-5,
        "gene_name" : "DLL4",
        "t" : -4.999246,
        "log_score" : -3.6143611430014384,
        "name" : "H__sapiens__1_-707068",
        "isInPath" : false,
        "Synonym" : ""
      },
      "position" : {
        "x" : -1649.1531786021217,
        "y" : -114.23553466796875
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "676",
        "B" : 18.02324,
        "annotations" : [ "GO:0008202", "GO:0006790" ],
        "annotation_name" : [ "steroid metabolic process", "sulfur compound metabolic process" ],
        "shared_name" : "H__sapiens__1_-718153",
        "score" : 0.7348091675824389,
        "RefSeq_mRNA_ID" : "NM_177536",
        "node_type" : "query",
        "Entrez_Gene_ID" : "6817",
        "Ensembl_Gene_ID" : "ENSG00000196502",
        "Gene_title" : "sulfotransferase family 1A member 1",
        "SUID" : 676,
        "ID" : "ILMN_1656900",
        "__mclCluster" : 3,
        "RefSeq_Protein_ID" : "NP_803880",
        "Uniprot_ID" : "P50225",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000457912",
        "logFC" : -1.1168868,
        "isExcludedFromPaths" : false,
        "P_Value" : 1.58E-12,
        "adj_P_Val" : 1.0E-11,
        "gene_name" : "SULT1A1",
        "t" : -12.600325,
        "log_score" : -0.3081444494228069,
        "name" : "H__sapiens__1_-718153",
        "isInPath" : false,
        "Synonym" : "STP1"
      },
      "position" : {
        "x" : -228.49486771796364,
        "y" : -332.4367220277874
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "675",
        "B" : -6.07378,
        "annotations" : [ "" ],
        "annotation_name" : [ "" ],
        "shared_name" : "H__sapiens__1_-702552",
        "score" : 0.9095711739093582,
        "RefSeq_mRNA_ID" : "NM_001256348",
        "node_type" : "query",
        "Entrez_Gene_ID" : "54437",
        "Ensembl_Gene_ID" : "ENSG00000082684",
        "Gene_title" : "semaphorin 5B",
        "SUID" : 675,
        "ID" : "ILMN_1794370",
        "__mclCluster" : 7,
        "RefSeq_Protein_ID" : "NP_001243277",
        "Uniprot_ID" : "Q9P283",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000479602",
        "logFC" : -0.0912047,
        "isExcludedFromPaths" : false,
        "P_Value" : 0.0406,
        "adj_P_Val" : 0.053,
        "gene_name" : "SEMA5B",
        "t" : -2.155366,
        "log_score" : -0.0947820280007521,
        "name" : "H__sapiens__1_-702552",
        "isInPath" : false,
        "Synonym" : "SEMAG"
      },
      "position" : {
        "x" : 2323.3272680650466,
        "y" : -332.4367370605469
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "674",
        "B" : -2.80934,
        "annotations" : [ "GO:0002237", "GO:0009617", "GO:0032496", "GO:0005539", "GO:0050865", "GO:0050867", "GO:0008201", "GO:1901681" ],
        "annotation_name" : [ "response to molecule of bacterial origin", "response to bacterium", "response to lipopolysaccharide", "glycosaminoglycan binding", "regulation of cell activation", "positive regulation of cell activation", "heparin binding", "sulfur compound binding" ],
        "shared_name" : "H__sapiens__1_-714657",
        "score" : 0.6866414521901735,
        "RefSeq_mRNA_ID" : "NM_003005",
        "node_type" : "query",
        "Entrez_Gene_ID" : "6403",
        "Ensembl_Gene_ID" : "ENSG00000174175",
        "Gene_title" : "selectin P",
        "SUID" : 674,
        "ID" : "ILMN_1715417",
        "__mclCluster" : 2,
        "RefSeq_Protein_ID" : "NP_002996",
        "Uniprot_ID" : "P16109",
        "selected" : true,
        "Ensembl_Protein_ID" : "ENSP00000399368",
        "logFC" : -0.1346174,
        "isExcludedFromPaths" : false,
        "P_Value" : 0.00128,
        "adj_P_Val" : 0.00212,
        "gene_name" : "SELP",
        "t" : -3.612775,
        "log_score" : -0.3759430266522176,
        "name" : "H__sapiens__1_-714657",
        "isInPath" : false,
        "Synonym" : "PSEL"
      },
      "position" : {
        "x" : -882.7911376953125,
        "y" : -318.7659996073514
      },
      "selected" : true
    }, {
      "data" : {
        "id" : "673",
        "B" : -8.17219,
        "annotations" : [ "GO:0046165" ],
        "annotation_name" : [ "alcohol biosynthetic process" ],
        "shared_name" : "H__sapiens__1_-720891",
        "score" : 0.8701917490300572,
        "RefSeq_mRNA_ID" : "NM_025257",
        "node_type" : "query",
        "Entrez_Gene_ID" : "80736",
        "Ensembl_Gene_ID" : "ENSG00000235336",
        "Gene_title" : "solute carrier family 44 member 4",
        "SUID" : 673,
        "ID" : "ILMN_2274775",
        "__mclCluster" : 10,
        "RefSeq_Protein_ID" : "NP_079533",
        "Uniprot_ID" : "Q53GD3",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000450005",
        "logFC" : -0.0362224,
        "isExcludedFromPaths" : false,
        "P_Value" : 0.636,
        "adj_P_Val" : 0.67,
        "gene_name" : "SLC44A4",
        "t" : -0.479249,
        "log_score" : -0.13904169043430656,
        "name" : "H__sapiens__1_-720891",
        "isInPath" : false,
        "Synonym" : "TPPT"
      },
      "position" : {
        "x" : -1776.3483298851042,
        "y" : 454.86161259295426
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "672",
        "B" : 26.65008,
        "annotations" : [ "" ],
        "annotation_name" : [ "" ],
        "shared_name" : "H__sapiens__1_-702208",
        "score" : 0.886655708298359,
        "RefSeq_mRNA_ID" : "NM_018530",
        "node_type" : "query",
        "Entrez_Gene_ID" : "55876",
        "Ensembl_Gene_ID" : "ENSG00000073605",
        "Gene_title" : "gasdermin B",
        "SUID" : 672,
        "ID" : "ILMN_1666206",
        "RefSeq_Protein_ID" : "NP_061000",
        "Uniprot_ID" : "Q8TAX9",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000463519",
        "logFC" : -1.7696133,
        "isExcludedFromPaths" : false,
        "P_Value" : 3.59E-16,
        "adj_P_Val" : 4.98E-15,
        "gene_name" : "GSDMB",
        "t" : -18.082866,
        "log_score" : -0.1202985250128769,
        "name" : "H__sapiens__1_-702208",
        "isInPath" : false,
        "Synonym" : "PRO2521"
      },
      "position" : {
        "x" : 1022.0410261352458,
        "y" : 486.5450165706496
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "671",
        "annotations" : [ "" ],
        "annotation_name" : [ "" ],
        "shared_name" : "H__sapiens__1_-716648",
        "score" : 0.7562642219115159,
        "RefSeq_mRNA_ID" : "NM_005656",
        "node_type" : "query",
        "Entrez_Gene_ID" : "7113",
        "Ensembl_Gene_ID" : "ENSG00000184012",
        "Gene_title" : "",
        "SUID" : 671,
        "ID" : "",
        "__mclCluster" : 10,
        "RefSeq_Protein_ID" : "NP_005647",
        "Uniprot_ID" : "O15393",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000397846",
        "isExcludedFromPaths" : false,
        "gene_name" : "TMPRSS2",
        "log_score" : -0.27936446398050385,
        "name" : "H__sapiens__1_-716648",
        "isInPath" : false,
        "Synonym" : "PRSS10"
      },
      "position" : {
        "x" : -1576.3483298851042,
        "y" : 454.86161259295426
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "670",
        "B" : -7.17463,
        "annotations" : [ "GO:0097194", "GO:0045833", "GO:0001819", "GO:0002520", "GO:0045619", "GO:2001233", "GO:0048534", "GO:0002521", "GO:0010942", "GO:0033077", "GO:0051241", "GO:0045939", "GO:0097285", "GO:1902105", "GO:0030217", "GO:0008202", "GO:0030098", "GO:0006790", "GO:0050865", "GO:0046165", "GO:0051055", "GO:0050863", "GO:0010894", "GO:0042110", "GO:0046890", "GO:0090181", "GO:0045580", "GO:0002694", "GO:0051249", "GO:0050810", "GO:0019218" ],
        "annotation_name" : [ "execution phase of apoptosis", "negative regulation of lipid metabolic process", "positive regulation of cytokine production", "immune system development", "regulation of lymphocyte differentiation", "regulation of apoptotic signaling pathway", "hematopoietic or lymphoid organ development", "leukocyte differentiation", "positive regulation of cell death", "T cell differentiation in thymus", "negative regulation of multicellular organismal process", "negative regulation of steroid metabolic process", "cell-type specific apoptotic process", "regulation of leukocyte differentiation", "T cell differentiation", "steroid metabolic process", "lymphocyte differentiation", "sulfur compound metabolic process", "regulation of cell activation", "alcohol biosynthetic process", "negative regulation of lipid biosynthetic process", "regulation of T cell activation", "negative regulation of steroid biosynthetic process", "T cell activation", "regulation of lipid biosynthetic process", "regulation of cholesterol metabolic process", "regulation of T cell differentiation", "regulation of leukocyte activation", "regulation of lymphocyte activation", "regulation of steroid biosynthetic process", "regulation of steroid metabolic process" ],
        "shared_name" : "H__sapiens__1_-709197",
        "score" : 0.017116357838462715,
        "RefSeq_mRNA_ID" : "NM_000454",
        "node_type" : "result",
        "Entrez_Gene_ID" : "6647",
        "Ensembl_Gene_ID" : "ENSG00000142168",
        "Gene_title" : "superoxide dismutase 1, soluble",
        "SUID" : 670,
        "ID" : "ILMN_1662438",
        "RefSeq_Protein_ID" : "NP_000445",
        "Uniprot_ID" : "P00441",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000374645",
        "logFC" : 0.1064063,
        "isExcludedFromPaths" : false,
        "P_Value" : 0.146,
        "adj_P_Val" : 0.175,
        "gene_name" : "SOD1",
        "t" : 1.497584,
        "log_score" : -4.067720673982009,
        "name" : "H__sapiens__1_-709197",
        "isInPath" : false,
        "Synonym" : "IPOA"
      },
      "position" : {
        "x" : 921.6982675085692,
        "y" : 376.4742537522132
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "669",
        "B" : 11.13547,
        "annotations" : [ "GO:0006367", "GO:0007219", "GO:0007220", "GO:0043235", "GO:0006352" ],
        "annotation_name" : [ "transcription initiation from RNA polymerase II promoter", "Notch signaling pathway", "Notch receptor processing", "receptor complex", "DNA-templated transcription, initiation" ],
        "shared_name" : "H__sapiens__1_-702228",
        "score" : 0.020668010423485372,
        "RefSeq_mRNA_ID" : "NM_000435",
        "node_type" : "result",
        "Entrez_Gene_ID" : "4854",
        "Ensembl_Gene_ID" : "ENSG00000074181",
        "Gene_title" : "notch 3",
        "SUID" : 669,
        "ID" : "ILMN_1658926",
        "__mclCluster" : 1,
        "RefSeq_Protein_ID" : "NP_000426",
        "Uniprot_ID" : "Q9UM47",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000473138",
        "logFC" : -0.7097128,
        "isExcludedFromPaths" : false,
        "P_Value" : 1.29E-9,
        "adj_P_Val" : 4.98E-9,
        "gene_name" : "NOTCH3",
        "t" : -9.186295,
        "log_score" : -3.879168164227668,
        "name" : "H__sapiens__1_-702228",
        "isInPath" : false,
        "Synonym" : "CASIL"
      },
      "position" : {
        "x" : -1648.2506396255149,
        "y" : -173.79707136984172
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "668",
        "annotations" : [ "" ],
        "annotation_name" : [ "" ],
        "shared_name" : "H__sapiens__1_-714838",
        "score" : 0.7916078394167869,
        "RefSeq_mRNA_ID" : "NM_020469",
        "node_type" : "query",
        "Entrez_Gene_ID" : "28",
        "Ensembl_Gene_ID" : "ENSG00000281879",
        "Gene_title" : "",
        "SUID" : 668,
        "ID" : "",
        "RefSeq_Protein_ID" : "NP_065202",
        "Uniprot_ID" : "",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000487108",
        "isExcludedFromPaths" : false,
        "gene_name" : "ABO",
        "log_score" : -0.23368916204760043,
        "name" : "H__sapiens__1_-714838",
        "isInPath" : false,
        "Synonym" : "A3GALT1"
      },
      "position" : {
        "x" : 848.8285609942614,
        "y" : 766.2927530689285
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "667",
        "B" : 31.36431,
        "annotations" : [ "GO:0048534", "GO:0006367", "GO:0045596", "GO:0030856", "GO:0045165", "GO:0002520", "GO:0007219", "GO:0007220", "GO:0001568", "GO:0030097", "GO:0006352" ],
        "annotation_name" : [ "hematopoietic or lymphoid organ development", "transcription initiation from RNA polymerase II promoter", "negative regulation of cell differentiation", "regulation of epithelial cell differentiation", "cell fate commitment", "immune system development", "Notch signaling pathway", "Notch receptor processing", "blood vessel development", "hemopoiesis", "DNA-templated transcription, initiation" ],
        "shared_name" : "H__sapiens__1_-720857",
        "score" : 0.6436226470804682,
        "RefSeq_mRNA_ID" : "NM_004557",
        "node_type" : "query",
        "Entrez_Gene_ID" : "4855",
        "Ensembl_Gene_ID" : "ENSG00000238196",
        "Gene_title" : "notch 4",
        "SUID" : 667,
        "ID" : "ILMN_1711157",
        "__mclCluster" : 1,
        "RefSeq_Protein_ID" : "NP_004548",
        "Uniprot_ID" : "Q99466",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000410674",
        "logFC" : -1.8710201,
        "isExcludedFromPaths" : false,
        "P_Value" : 3.63E-18,
        "adj_P_Val" : 8.65E-17,
        "gene_name" : "NOTCH4",
        "t" : -21.852857,
        "log_score" : -0.44064267635299625,
        "name" : "H__sapiens__1_-720857",
        "isInPath" : false,
        "Synonym" : ""
      },
      "position" : {
        "x" : -1603.2469469985617,
        "y" : -103.76508894796672
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "666",
        "B" : 33.73602,
        "annotations" : [ "GO:0032374", "GO:0030178", "GO:0032368", "GO:0032373", "GO:0001568", "GO:0033344", "GO:0032371", "GO:0034185", "GO:0015850", "GO:0032376", "GO:0030111", "GO:0030301", "GO:0016055", "GO:0043235", "GO:0015918" ],
        "annotation_name" : [ "regulation of cholesterol transport", "negative regulation of Wnt signaling pathway", "regulation of lipid transport", "positive regulation of sterol transport", "blood vessel development", "cholesterol efflux", "regulation of sterol transport", "apolipoprotein binding", "organic hydroxy compound transport", "positive regulation of cholesterol transport", "regulation of Wnt signaling pathway", "cholesterol transport", "Wnt signaling pathway", "receptor complex", "sterol transport" ],
        "shared_name" : "H__sapiens__1_-706362",
        "score" : 0.6734353102710342,
        "RefSeq_mRNA_ID" : "NM_002332",
        "node_type" : "query",
        "Entrez_Gene_ID" : "4035",
        "Ensembl_Gene_ID" : "ENSG00000123384",
        "Gene_title" : "LDL receptor related protein 1",
        "SUID" : 666,
        "ID" : "ILMN_1669772",
        "__mclCluster" : 1,
        "RefSeq_Protein_ID" : "NP_002323",
        "Uniprot_ID" : "Q07954",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000451737",
        "logFC" : -1.6429853,
        "isExcludedFromPaths" : false,
        "P_Value" : 3.57E-19,
        "adj_P_Val" : 1.13E-17,
        "gene_name" : "LRP1",
        "t" : -24.008783,
        "log_score" : -0.3953633378249501,
        "name" : "H__sapiens__1_-706362",
        "isInPath" : false,
        "Synonym" : "LRP1A"
      },
      "position" : {
        "x" : -1691.6844469985617,
        "y" : -152.53761092062297
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "665",
        "B" : -8.17484,
        "annotations" : [ "GO:0050870", "GO:0002237", "GO:0050670", "GO:0032944", "GO:0042129", "GO:0046634", "GO:0051251", "GO:0032609", "GO:0001819", "GO:0001568", "GO:0046651", "GO:0032740", "GO:0070661", "GO:0050798", "GO:0042102", "GO:0050671", "GO:0002460", "GO:0002696", "GO:0050867", "GO:0032946", "GO:0046006", "GO:0032660", "GO:0032649", "GO:0046631", "GO:0009617", "GO:0032496", "GO:0046635", "GO:0050865", "GO:0050863", "GO:0042104", "GO:0042110", "GO:0032729", "GO:0030155", "GO:0032620", "GO:0030101", "GO:0002694", "GO:0051249", "GO:0032943", "GO:0042098", "GO:0070663", "GO:0070665" ],
        "annotation_name" : [ "positive regulation of T cell activation", "response to molecule of bacterial origin", "regulation of lymphocyte proliferation", "regulation of mononuclear cell proliferation", "regulation of T cell proliferation", "regulation of alpha-beta T cell activation", "positive regulation of lymphocyte activation", "interferon-gamma production", "positive regulation of cytokine production", "blood vessel development", "lymphocyte proliferation", "positive regulation of interleukin-17 production", "leukocyte proliferation", "activated T cell proliferation", "positive regulation of T cell proliferation", "positive regulation of lymphocyte proliferation", "adaptive immune response based on somatic recombination of immune receptors built from immunoglobulin superfamily domains", "positive regulation of leukocyte activation", "positive regulation of cell activation", "positive regulation of mononuclear cell proliferation", "regulation of activated T cell proliferation", "regulation of interleukin-17 production", "regulation of interferon-gamma production", "alpha-beta T cell activation", "response to bacterium", "response to lipopolysaccharide", "positive regulation of alpha-beta T cell activation", "regulation of cell activation", "regulation of T cell activation", "positive regulation of activated T cell proliferation", "T cell activation", "positive regulation of interferon-gamma production", "regulation of cell adhesion", "interleukin-17 production", "natural killer cell activation", "regulation of leukocyte activation", "regulation of lymphocyte activation", "mononuclear cell proliferation", "T cell proliferation", "regulation of leukocyte proliferation", "positive regulation of leukocyte proliferation" ],
        "shared_name" : "H__sapiens__1_-710332",
        "score" : 0.6923583383817126,
        "RefSeq_mRNA_ID" : "NM_001562",
        "node_type" : "query",
        "Entrez_Gene_ID" : "3606",
        "Ensembl_Gene_ID" : "ENSG00000150782",
        "Gene_title" : "interleukin 18",
        "SUID" : 665,
        "ID" : "ILMN_1778457",
        "__mclCluster" : 2,
        "RefSeq_Protein_ID" : "NP_001553",
        "Uniprot_ID" : "Q14116",
        "selected" : true,
        "Ensembl_Protein_ID" : "ENSP00000434561",
        "logFC" : 0.0342181,
        "isExcludedFromPaths" : false,
        "P_Value" : 0.64,
        "adj_P_Val" : 0.674,
        "gene_name" : "IL18",
        "t" : 0.473833,
        "log_score" : -0.3676516273607946,
        "name" : "H__sapiens__1_-710332",
        "isInPath" : false,
        "Synonym" : "IL1F4"
      },
      "position" : {
        "x" : -713.9943248750697,
        "y" : -162.39948300856935
      },
      "selected" : true
    }, {
      "data" : {
        "id" : "664",
        "B" : -8.26999,
        "annotations" : [ "" ],
        "annotation_name" : [ "" ],
        "shared_name" : "H__sapiens__1_-716588",
        "score" : 0.7371705169015564,
        "RefSeq_mRNA_ID" : "NM_005879",
        "node_type" : "query",
        "Entrez_Gene_ID" : "10293",
        "Ensembl_Gene_ID" : "ENSG00000183763",
        "Gene_title" : "TRAF interacting protein",
        "SUID" : 664,
        "ID" : "ILMN_1715540",
        "__mclCluster" : 11,
        "RefSeq_Protein_ID" : "NP_005870",
        "Uniprot_ID" : "Q9BWF2",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000420085",
        "logFC" : -0.0118206,
        "isExcludedFromPaths" : false,
        "P_Value" : 0.843,
        "adj_P_Val" : 0.86,
        "gene_name" : "TRAIP",
        "t" : -0.200683,
        "log_score" : -0.30493604733734136,
        "name" : "H__sapiens__1_-716588",
        "isInPath" : false,
        "Synonym" : "RNF206"
      },
      "position" : {
        "x" : -1176.3483298851042,
        "y" : 454.86161259295426
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "663",
        "annotations" : [ "GO:2001237", "GO:0097191", "GO:2001236", "GO:0043565", "GO:0090399", "GO:0097192", "GO:2001239", "GO:0038034", "GO:2001233" ],
        "annotation_name" : [ "negative regulation of extrinsic apoptotic signaling pathway", "extrinsic apoptotic signaling pathway", "regulation of extrinsic apoptotic signaling pathway", "sequence-specific DNA binding", "replicative senescence", "extrinsic apoptotic signaling pathway in absence of ligand", "regulation of extrinsic apoptotic signaling pathway in absence of ligand", "signal transduction in absence of ligand", "regulation of apoptotic signaling pathway" ],
        "shared_name" : "H__sapiens__1_-712333",
        "score" : 0.6952907436220319,
        "RefSeq_mRNA_ID" : "NM_198253",
        "node_type" : "query",
        "Entrez_Gene_ID" : "7015",
        "Ensembl_Gene_ID" : "ENSG00000164362",
        "Gene_title" : "",
        "SUID" : 663,
        "ID" : "",
        "__mclCluster" : 11,
        "RefSeq_Protein_ID" : "NP_937983",
        "Uniprot_ID" : "O14746",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000426042",
        "isExcludedFromPaths" : false,
        "gene_name" : "TERT",
        "log_score" : -0.3634251847487063,
        "name" : "H__sapiens__1_-712333",
        "isInPath" : false,
        "Synonym" : "TRT"
      },
      "position" : {
        "x" : -976.3483298851042,
        "y" : 454.86161259295426
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "662",
        "B" : -6.58943,
        "annotations" : [ "GO:0006790" ],
        "annotation_name" : [ "sulfur compound metabolic process" ],
        "shared_name" : "H__sapiens__1_-709795",
        "score" : 0.015035221726236947,
        "RefSeq_mRNA_ID" : "NM_000255",
        "node_type" : "result",
        "Entrez_Gene_ID" : "4594",
        "Ensembl_Gene_ID" : "ENSG00000146085",
        "Gene_title" : "methylmalonyl-CoA mutase",
        "SUID" : 662,
        "ID" : "ILMN_1785113",
        "RefSeq_Protein_ID" : "NP_000246",
        "Uniprot_ID" : "P22033",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000274813",
        "logFC" : 0.0846532,
        "isExcludedFromPaths" : false,
        "P_Value" : 0.0729,
        "adj_P_Val" : 0.0916,
        "gene_name" : "MUT",
        "t" : 1.869608,
        "log_score" : -4.197359715311713,
        "name" : "H__sapiens__1_-709795",
        "isInPath" : false,
        "Synonym" : ""
      },
      "position" : {
        "x" : 627.1618555389027,
        "y" : 523.1359603431358
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "661",
        "B" : 24.6684,
        "annotations" : [ "GO:0001159", "GO:0000976", "GO:0045596", "GO:0034341", "GO:0007259", "GO:0000975", "GO:0000987", "GO:0000978", "GO:0001067", "GO:0048660", "GO:0043565", "GO:0030856", "GO:0043122", "GO:0007249", "GO:0048659", "GO:0033002", "GO:0044212", "GO:0051241" ],
        "annotation_name" : [ "core promoter proximal region DNA binding", "transcription regulatory region sequence-specific DNA binding", "negative regulation of cell differentiation", "response to interferon-gamma", "JAK-STAT cascade", "regulatory region DNA binding", "core promoter proximal region sequence-specific DNA binding", "RNA polymerase II core promoter proximal region sequence-specific DNA binding", "regulatory region nucleic acid binding", "regulation of smooth muscle cell proliferation", "sequence-specific DNA binding", "regulation of epithelial cell differentiation", "regulation of I-kappaB kinase/NF-kappaB signaling", "I-kappaB kinase/NF-kappaB signaling", "smooth muscle cell proliferation", "muscle cell proliferation", "transcription regulatory region DNA binding", "negative regulation of multicellular organismal process" ],
        "shared_name" : "H__sapiens__1_-705407",
        "score" : 0.017076675008233122,
        "RefSeq_mRNA_ID" : "NM_139266",
        "node_type" : "result",
        "Entrez_Gene_ID" : "6772",
        "Ensembl_Gene_ID" : "ENSG00000115415",
        "Gene_title" : "signal transducer and activator of transcription 1",
        "SUID" : 661,
        "ID" : "ILMN_1691364",
        "__mclCluster" : 2,
        "RefSeq_Protein_ID" : "NP_644671",
        "Uniprot_ID" : "P42224",
        "selected" : true,
        "Ensembl_Protein_ID" : "ENSP00000438703",
        "logFC" : -1.3988901,
        "isExcludedFromPaths" : false,
        "P_Value" : 2.47E-15,
        "adj_P_Val" : 2.79E-14,
        "gene_name" : "STAT1",
        "t" : -16.677368,
        "log_score" : -4.070041781221813,
        "name" : "H__sapiens__1_-705407",
        "isInPath" : false,
        "Synonym" : "STAT91"
      },
      "position" : {
        "x" : -827.7394382478724,
        "y" : -172.24881772536622
      },
      "selected" : true
    }, {
      "data" : {
        "id" : "660",
        "B" : 2.32986,
        "annotations" : [ "GO:0016055", "GO:0030155" ],
        "annotation_name" : [ "Wnt signaling pathway", "regulation of cell adhesion" ],
        "shared_name" : "H__sapiens__1_-709312",
        "score" : 0.6826541110979831,
        "RefSeq_mRNA_ID" : "NM_001408",
        "node_type" : "query",
        "Entrez_Gene_ID" : "1952",
        "Ensembl_Gene_ID" : "ENSG00000143126",
        "Gene_title" : "cadherin EGF LAG seven-pass G-type receptor 2",
        "SUID" : 660,
        "ID" : "ILMN_1711208",
        "__mclCluster" : 5,
        "RefSeq_Protein_ID" : "NP_001399",
        "Uniprot_ID" : "Q9HCU4",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000271332",
        "logFC" : 0.5799918,
        "isExcludedFromPaths" : false,
        "P_Value" : 7.34E-6,
        "adj_P_Val" : 1.67E-5,
        "gene_name" : "CELSR2",
        "t" : 5.589198,
        "log_score" : -0.381766973606296,
        "name" : "H__sapiens__1_-709312",
        "isInPath" : false,
        "Synonym" : "MEGF3"
      },
      "position" : {
        "x" : 1171.5051322820364,
        "y" : -332.4367220277874
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "659",
        "B" : -7.68465,
        "annotations" : [ "GO:0019901" ],
        "annotation_name" : [ "protein kinase binding" ],
        "shared_name" : "H__sapiens__1_-713803",
        "score" : 0.01791414142881581,
        "RefSeq_mRNA_ID" : "NM_005900",
        "node_type" : "result",
        "Entrez_Gene_ID" : "4086",
        "Ensembl_Gene_ID" : "ENSG00000170365",
        "Gene_title" : "SMAD family member 1",
        "SUID" : 659,
        "ID" : "ILMN_1670540",
        "RefSeq_Protein_ID" : "NP_005891",
        "Uniprot_ID" : "Q15797",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000427002",
        "logFC" : 0.031484,
        "isExcludedFromPaths" : false,
        "P_Value" : 0.284,
        "adj_P_Val" : 0.324,
        "gene_name" : "SMAD1",
        "t" : 1.093063,
        "log_score" : -4.022164854083899,
        "name" : "H__sapiens__1_-713803",
        "isInPath" : false,
        "Synonym" : "MADR1"
      },
      "position" : {
        "x" : 773.3898807013011,
        "y" : 759.3023298852961
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "658",
        "B" : 25.49447,
        "annotations" : [ "GO:0006790", "GO:0007259" ],
        "annotation_name" : [ "sulfur compound metabolic process", "JAK-STAT cascade" ],
        "shared_name" : "H__sapiens__1_-706817",
        "score" : 0.01791554144236146,
        "RefSeq_mRNA_ID" : "NM_003152",
        "node_type" : "result",
        "Entrez_Gene_ID" : "6776",
        "Ensembl_Gene_ID" : "ENSG00000126561",
        "Gene_title" : "signal transducer and activator of transcription 5A",
        "SUID" : 658,
        "ID" : "ILMN_1753547",
        "__mclCluster" : 2,
        "RefSeq_Protein_ID" : "NP_003143",
        "Uniprot_ID" : "P42229",
        "selected" : true,
        "Ensembl_Protein_ID" : "ENSP00000468749",
        "logFC" : -1.7703795,
        "isExcludedFromPaths" : false,
        "P_Value" : 1.11E-15,
        "adj_P_Val" : 1.36E-14,
        "gene_name" : "STAT5A",
        "t" : -17.251439,
        "log_score" : -4.022086705831714,
        "name" : "H__sapiens__1_-706817",
        "isInPath" : false,
        "Synonym" : "STAT5"
      },
      "position" : {
        "x" : -866.0873802225857,
        "y" : -214.63287353515625
      },
      "selected" : true
    }, {
      "data" : {
        "id" : "657",
        "B" : 5.56786,
        "annotations" : [ "GO:0000976", "GO:0045833", "GO:0000975", "GO:0001819", "GO:0032371", "GO:0001067", "GO:0071260", "GO:0060828", "GO:0060070", "GO:0030111", "GO:0016055", "GO:0045939", "GO:0015918", "GO:0032374", "GO:0008202", "GO:0046165", "GO:0051055", "GO:0032368", "GO:0010894", "GO:0032479", "GO:0046890", "GO:0032606", "GO:0015850", "GO:0032481", "GO:0051091", "GO:0043565", "GO:0032375", "GO:0050810", "GO:0030301", "GO:0032372", "GO:0044212", "GO:0019218" ],
        "annotation_name" : [ "transcription regulatory region sequence-specific DNA binding", "negative regulation of lipid metabolic process", "regulatory region DNA binding", "positive regulation of cytokine production", "regulation of sterol transport", "regulatory region nucleic acid binding", "cellular response to mechanical stimulus", "regulation of canonical Wnt signaling pathway", "canonical Wnt signaling pathway", "regulation of Wnt signaling pathway", "Wnt signaling pathway", "negative regulation of steroid metabolic process", "sterol transport", "regulation of cholesterol transport", "steroid metabolic process", "alcohol biosynthetic process", "negative regulation of lipid biosynthetic process", "regulation of lipid transport", "negative regulation of steroid biosynthetic process", "regulation of type I interferon production", "regulation of lipid biosynthetic process", "type I interferon production", "organic hydroxy compound transport", "positive regulation of type I interferon production", "positive regulation of sequence-specific DNA binding transcription factor activity", "sequence-specific DNA binding", "negative regulation of cholesterol transport", "regulation of steroid biosynthetic process", "cholesterol transport", "negative regulation of sterol transport", "transcription regulatory region DNA binding", "regulation of steroid metabolic process" ],
        "shared_name" : "H__sapiens__1_-704636",
        "score" : 0.020023670732817855,
        "RefSeq_mRNA_ID" : "NM_003998",
        "node_type" : "result",
        "Entrez_Gene_ID" : "4790",
        "Ensembl_Gene_ID" : "ENSG00000109320",
        "Gene_title" : "nuclear factor kappa B subunit 1",
        "SUID" : 657,
        "ID" : "ILMN_1714965",
        "__mclCluster" : 2,
        "RefSeq_Protein_ID" : "NP_003989",
        "Uniprot_ID" : "P19838",
        "selected" : true,
        "Ensembl_Protein_ID" : "ENSP00000469340",
        "logFC" : -0.3546899,
        "isExcludedFromPaths" : false,
        "P_Value" : 3.01E-7,
        "adj_P_Val" : 8.22E-7,
        "gene_name" : "NFKB1",
        "t" : -6.845311,
        "log_score" : -3.910840168614617,
        "name" : "H__sapiens__1_-704636",
        "isInPath" : false,
        "Synonym" : "NFKB-p50"
      },
      "position" : {
        "x" : -796.6556396484375,
        "y" : -218.9667121846408
      },
      "selected" : true
    }, {
      "data" : {
        "id" : "656",
        "B" : 17.39594,
        "annotations" : [ "GO:0050870", "GO:0008589", "GO:0000976", "GO:0071887", "GO:0045596", "GO:0046634", "GO:0030178", "GO:0051251", "GO:0000975", "GO:0002520", "GO:0045619", "GO:0007224", "GO:0000978", "GO:0090090", "GO:0048534", "GO:0001067", "GO:0060828", "GO:0060070", "GO:0030111", "GO:0002696", "GO:0016055", "GO:0002521", "GO:0050867", "GO:0030097", "GO:0033077", "GO:0097285", "GO:0001159", "GO:0046631", "GO:1902105", "GO:0046635", "GO:0030217", "GO:0030098", "GO:0050865", "GO:1902107", "GO:0050863", "GO:0000987", "GO:0042110", "GO:0043565", "GO:0045879", "GO:0045580", "GO:0002694", "GO:0051249", "GO:0044212", "GO:0008013" ],
        "annotation_name" : [ "positive regulation of T cell activation", "regulation of smoothened signaling pathway", "transcription regulatory region sequence-specific DNA binding", "leukocyte apoptotic process", "negative regulation of cell differentiation", "regulation of alpha-beta T cell activation", "negative regulation of Wnt signaling pathway", "positive regulation of lymphocyte activation", "regulatory region DNA binding", "immune system development", "regulation of lymphocyte differentiation", "smoothened signaling pathway", "RNA polymerase II core promoter proximal region sequence-specific DNA binding", "negative regulation of canonical Wnt signaling pathway", "hematopoietic or lymphoid organ development", "regulatory region nucleic acid binding", "regulation of canonical Wnt signaling pathway", "canonical Wnt signaling pathway", "regulation of Wnt signaling pathway", "positive regulation of leukocyte activation", "Wnt signaling pathway", "leukocyte differentiation", "positive regulation of cell activation", "hemopoiesis", "T cell differentiation in thymus", "cell-type specific apoptotic process", "core promoter proximal region DNA binding", "alpha-beta T cell activation", "regulation of leukocyte differentiation", "positive regulation of alpha-beta T cell activation", "T cell differentiation", "lymphocyte differentiation", "regulation of cell activation", "positive regulation of leukocyte differentiation", "regulation of T cell activation", "core promoter proximal region sequence-specific DNA binding", "T cell activation", "sequence-specific DNA binding", "negative regulation of smoothened signaling pathway", "regulation of T cell differentiation", "regulation of leukocyte activation", "regulation of lymphocyte activation", "transcription regulatory region DNA binding", "beta-catenin binding" ],
        "shared_name" : "H__sapiens__1_-704308",
        "score" : 0.02675142058638602,
        "RefSeq_mRNA_ID" : "NM_000168",
        "node_type" : "result",
        "Entrez_Gene_ID" : "2737",
        "Ensembl_Gene_ID" : "ENSG00000106571",
        "Gene_title" : "GLI family zinc finger 3",
        "SUID" : 656,
        "ID" : "ILMN_1771962",
        "__mclCluster" : 4,
        "RefSeq_Protein_ID" : "NP_000159",
        "Uniprot_ID" : "P10071",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000407963",
        "logFC" : -0.5728495,
        "isExcludedFromPaths" : false,
        "P_Value" : 2.91E-12,
        "adj_P_Val" : 1.74E-11,
        "gene_name" : "GLI3",
        "t" : -12.25931,
        "log_score" : -3.621167701017075,
        "name" : "H__sapiens__1_-704308",
        "isInPath" : false,
        "Synonym" : "PPDIV"
      },
      "position" : {
        "x" : 571.5051322820364,
        "y" : -332.4367220277874
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "655",
        "B" : 6.00433,
        "annotations" : [ "GO:0032374", "GO:0034358", "GO:0008202", "GO:0051055", "GO:0032368", "GO:0045833", "GO:0034433", "GO:0033344", "GO:0046890", "GO:0032371", "GO:0032994", "GO:0004857", "GO:0015850", "GO:0034434", "GO:0032375", "GO:0048261", "GO:0030301", "GO:0032372", "GO:0034435", "GO:0051241", "GO:0019218", "GO:0015918" ],
        "annotation_name" : [ "regulation of cholesterol transport", "plasma lipoprotein particle", "steroid metabolic process", "negative regulation of lipid biosynthetic process", "regulation of lipid transport", "negative regulation of lipid metabolic process", "steroid esterification", "cholesterol efflux", "regulation of lipid biosynthetic process", "regulation of sterol transport", "protein-lipid complex", "enzyme inhibitor activity", "organic hydroxy compound transport", "sterol esterification", "negative regulation of cholesterol transport", "negative regulation of receptor-mediated endocytosis", "cholesterol transport", "negative regulation of sterol transport", "cholesterol esterification", "negative regulation of multicellular organismal process", "regulation of steroid metabolic process", "sterol transport" ],
        "shared_name" : "H__sapiens__1_-707212",
        "score" : 0.7755880523325785,
        "RefSeq_mRNA_ID" : "NM_001645",
        "node_type" : "query",
        "Entrez_Gene_ID" : "341",
        "Ensembl_Gene_ID" : "ENSG00000130208",
        "Gene_title" : "apolipoprotein C1",
        "SUID" : 655,
        "ID" : "ILMN_1789007",
        "__mclCluster" : 3,
        "RefSeq_Protein_ID" : "NP_001636",
        "Uniprot_ID" : "P02654",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000468276",
        "logFC" : -1.1789926,
        "isExcludedFromPaths" : false,
        "P_Value" : 1.96E-7,
        "adj_P_Val" : 5.49E-7,
        "gene_name" : "APOC1",
        "t" : -7.019144,
        "log_score" : -0.2541337601513477,
        "name" : "H__sapiens__1_-707212",
        "isInPath" : false,
        "Synonym" : ""
      },
      "position" : {
        "x" : -128.49486771796364,
        "y" : -232.43672202778743
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "654",
        "B" : 28.80182,
        "annotations" : [ "GO:0034358", "GO:0045833", "GO:0032373", "GO:0033344", "GO:0032371", "GO:0032376", "GO:0005539", "GO:0034434", "GO:1901681", "GO:0051241", "GO:0045939", "GO:0015918", "GO:0032374", "GO:0008202", "GO:0050865", "GO:0046165", "GO:0051055", "GO:0032368", "GO:0010894", "GO:0034433", "GO:0008201", "GO:0046890", "GO:0032994", "GO:0015850", "GO:0090181", "GO:0045862", "GO:0050810", "GO:0030301", "GO:0010954", "GO:0034435", "GO:0019218" ],
        "annotation_name" : [ "plasma lipoprotein particle", "negative regulation of lipid metabolic process", "positive regulation of sterol transport", "cholesterol efflux", "regulation of sterol transport", "positive regulation of cholesterol transport", "glycosaminoglycan binding", "sterol esterification", "sulfur compound binding", "negative regulation of multicellular organismal process", "negative regulation of steroid metabolic process", "sterol transport", "regulation of cholesterol transport", "steroid metabolic process", "regulation of cell activation", "alcohol biosynthetic process", "negative regulation of lipid biosynthetic process", "regulation of lipid transport", "negative regulation of steroid biosynthetic process", "steroid esterification", "heparin binding", "regulation of lipid biosynthetic process", "protein-lipid complex", "organic hydroxy compound transport", "regulation of cholesterol metabolic process", "positive regulation of proteolysis", "regulation of steroid biosynthetic process", "cholesterol transport", "positive regulation of protein processing", "cholesterol esterification", "regulation of steroid metabolic process" ],
        "shared_name" : "H__sapiens__1_-707210",
        "score" : 0.6623566693009905,
        "RefSeq_mRNA_ID" : "NM_001302691",
        "node_type" : "query",
        "Entrez_Gene_ID" : "348",
        "Ensembl_Gene_ID" : "ENSG00000130203",
        "Gene_title" : "apolipoprotein E",
        "SUID" : 654,
        "ID" : "ILMN_1740938",
        "__mclCluster" : 3,
        "RefSeq_Protein_ID" : "NP_001289620",
        "Uniprot_ID" : "P02649",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000413653",
        "logFC" : -2.7770425,
        "isExcludedFromPaths" : false,
        "P_Value" : 4.42E-17,
        "adj_P_Val" : 7.67E-16,
        "gene_name" : "APOE",
        "t" : -19.724566,
        "log_score" : -0.4119510927533331,
        "name" : "H__sapiens__1_-707210",
        "isInPath" : false,
        "Synonym" : "AD2"
      },
      "position" : {
        "x" : -28.494867717963643,
        "y" : -332.4367220277874
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "653",
        "B" : 2.42558,
        "annotations" : [ "" ],
        "annotation_name" : [ "" ],
        "shared_name" : "H__sapiens__1_-709006",
        "score" : 0.763438507531446,
        "RefSeq_mRNA_ID" : "NM_001080432",
        "node_type" : "query",
        "Entrez_Gene_ID" : "79068",
        "Ensembl_Gene_ID" : "ENSG00000140718",
        "Gene_title" : "fat mass and obesity associated",
        "SUID" : 653,
        "ID" : "ILMN_2288070",
        "__mclCluster" : 12,
        "RefSeq_Protein_ID" : "NP_001073901",
        "Uniprot_ID" : "Q9C0B1",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000490516",
        "logFC" : -0.3650273,
        "isExcludedFromPaths" : false,
        "P_Value" : 6.68E-6,
        "adj_P_Val" : 1.53E-5,
        "gene_name" : "FTO",
        "t" : -5.62571,
        "log_score" : -0.26992269780085115,
        "name" : "H__sapiens__1_-709006",
        "isInPath" : false,
        "Synonym" : "MGC5149"
      },
      "position" : {
        "x" : -576.3483298851042,
        "y" : 454.86161259295426
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "652",
        "B" : 10.5236,
        "annotations" : [ "GO:0048261" ],
        "annotation_name" : [ "negative regulation of receptor-mediated endocytosis" ],
        "shared_name" : "H__sapiens__1_-721077",
        "score" : 0.692584045442746,
        "RefSeq_mRNA_ID" : "NM_002973",
        "node_type" : "query",
        "Entrez_Gene_ID" : "6311",
        "Ensembl_Gene_ID" : "ENSG00000204842",
        "Gene_title" : "ataxin 2",
        "SUID" : 652,
        "ID" : "ILMN_1743829",
        "__mclCluster" : 12,
        "RefSeq_Protein_ID" : "NP_002964",
        "Uniprot_ID" : "Q99700",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000481448",
        "logFC" : -0.4406417,
        "isExcludedFromPaths" : false,
        "P_Value" : 2.35E-9,
        "adj_P_Val" : 8.68E-9,
        "gene_name" : "ATXN2",
        "t" : -8.913866,
        "log_score" : -0.36732568302452784,
        "name" : "H__sapiens__1_-721077",
        "isInPath" : false,
        "Synonym" : "TNRC13"
      },
      "position" : {
        "x" : -376.34832988510425,
        "y" : 454.86161259295426
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "651",
        "annotations" : [ "GO:0004857", "GO:0034358", "GO:0005539", "GO:0008201", "GO:1901681", "GO:0034185", "GO:0032994" ],
        "annotation_name" : [ "enzyme inhibitor activity", "plasma lipoprotein particle", "glycosaminoglycan binding", "heparin binding", "sulfur compound binding", "apolipoprotein binding", "protein-lipid complex" ],
        "shared_name" : "H__sapiens__1_-718835",
        "score" : 0.7614045236074254,
        "RefSeq_mRNA_ID" : "NM_005577",
        "node_type" : "query",
        "Entrez_Gene_ID" : "4018",
        "Ensembl_Gene_ID" : "ENSG00000198670",
        "Gene_title" : "",
        "SUID" : 651,
        "ID" : "",
        "RefSeq_Protein_ID" : "NP_005568",
        "Uniprot_ID" : "P08519",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000480589",
        "isExcludedFromPaths" : false,
        "gene_name" : "LPA",
        "log_score" : -0.2725904938806173,
        "name" : "H__sapiens__1_-718835",
        "isInPath" : false,
        "Synonym" : "Lp(a)"
      },
      "position" : {
        "x" : 627.1618555389027,
        "y" : 598.8978274045392
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "650",
        "B" : 15.24248,
        "annotations" : [ "GO:0043235" ],
        "annotation_name" : [ "receptor complex" ],
        "shared_name" : "H__sapiens__1_-713649",
        "score" : 0.7551602353807436,
        "RefSeq_mRNA_ID" : "NM_001307945",
        "node_type" : "query",
        "Entrez_Gene_ID" : "1138",
        "Ensembl_Gene_ID" : "ENSG00000169684",
        "Gene_title" : "cholinergic receptor nicotinic alpha 5 subunit",
        "SUID" : 650,
        "ID" : "ILMN_2110751",
        "__mclCluster" : 1,
        "RefSeq_Protein_ID" : "NP_001294874",
        "Uniprot_ID" : "P30532",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000453519",
        "logFC" : 0.8091495,
        "isExcludedFromPaths" : false,
        "P_Value" : 2.37E-11,
        "adj_P_Val" : 1.2E-10,
        "gene_name" : "CHRNA5",
        "t" : 11.137898,
        "log_score" : -0.28082531995882365,
        "name" : "H__sapiens__1_-713649",
        "isInPath" : false,
        "Synonym" : ""
      },
      "position" : {
        "x" : -1624.1199951171875,
        "y" : -388.2788107087086
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "649",
        "B" : 13.30158,
        "annotations" : [ "GO:0008080", "GO:0031365", "GO:0000975", "GO:0001819", "GO:0006474", "GO:0007219", "GO:0032479", "GO:0032606", "GO:0001085", "GO:0001067", "GO:0032481", "GO:0051091", "GO:0003713", "GO:0044212", "GO:0008013" ],
        "annotation_name" : [ "N-acetyltransferase activity", "N-terminal protein amino acid modification", "regulatory region DNA binding", "positive regulation of cytokine production", "N-terminal protein amino acid acetylation", "Notch signaling pathway", "regulation of type I interferon production", "type I interferon production", "RNA polymerase II transcription factor binding", "regulatory region nucleic acid binding", "positive regulation of type I interferon production", "positive regulation of sequence-specific DNA binding transcription factor activity", "transcription coactivator activity", "transcription regulatory region DNA binding", "beta-catenin binding" ],
        "shared_name" : "H__sapiens__1_-703273",
        "score" : 0.014249368162820741,
        "RefSeq_mRNA_ID" : "NM_001429",
        "node_type" : "result",
        "Entrez_Gene_ID" : "2033",
        "Ensembl_Gene_ID" : "ENSG00000100393",
        "Gene_title" : "E1A binding protein p300",
        "SUID" : 649,
        "ID" : "ILMN_1744665",
        "__mclCluster" : 1,
        "RefSeq_Protein_ID" : "NP_001420",
        "Uniprot_ID" : "Q09472",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000489397",
        "logFC" : -0.6658088,
        "isExcludedFromPaths" : false,
        "P_Value" : 1.57E-10,
        "adj_P_Val" : 6.95E-10,
        "gene_name" : "EP300",
        "t" : -10.187349,
        "log_score" : -4.251042712701676,
        "name" : "H__sapiens__1_-703273",
        "isInPath" : false,
        "Synonym" : "p300"
      },
      "position" : {
        "x" : -1508.1987597427024,
        "y" : -288.1248225295097
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "648",
        "B" : 0.6099,
        "annotations" : [ "GO:0019901", "GO:0006367", "GO:0051153", "GO:0014902", "GO:0010830", "GO:0003713", "GO:0007219", "GO:0006352" ],
        "annotation_name" : [ "protein kinase binding", "transcription initiation from RNA polymerase II promoter", "regulation of striated muscle cell differentiation", "myotube differentiation", "regulation of myotube differentiation", "transcription coactivator activity", "Notch signaling pathway", "DNA-templated transcription, initiation" ],
        "shared_name" : "H__sapiens__1_-711569",
        "score" : 0.018696011484338193,
        "RefSeq_mRNA_ID" : "NM_014757",
        "node_type" : "result",
        "Entrez_Gene_ID" : "9794",
        "Ensembl_Gene_ID" : "ENSG00000161021",
        "Gene_title" : "mastermind like transcriptional coactivator 1",
        "SUID" : 648,
        "ID" : "ILMN_1803060",
        "__mclCluster" : 1,
        "RefSeq_Protein_ID" : "NP_055572",
        "Uniprot_ID" : "Q92585",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000292599",
        "logFC" : -0.2450987,
        "isExcludedFromPaths" : false,
        "P_Value" : 4.06E-5,
        "adj_P_Val" : 8.33E-5,
        "gene_name" : "MAML1",
        "t" : -4.935551,
        "log_score" : -3.979445067478643,
        "name" : "H__sapiens__1_-711569",
        "isInPath" : false,
        "Synonym" : "Mam-1"
      },
      "position" : {
        "x" : -1530.4577929458274,
        "y" : -212.63239851827922
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "647",
        "annotations" : [ "" ],
        "annotation_name" : [ "" ],
        "shared_name" : "H__sapiens__1_-704799",
        "score" : 0.01951327485298998,
        "RefSeq_mRNA_ID" : "NM_003006",
        "node_type" : "result",
        "Entrez_Gene_ID" : "6404",
        "Ensembl_Gene_ID" : "ENSG00000110876",
        "Gene_title" : "",
        "SUID" : 647,
        "ID" : "",
        "__mclCluster" : 2,
        "RefSeq_Protein_ID" : "NP_002997",
        "Uniprot_ID" : "Q14242",
        "selected" : true,
        "Ensembl_Protein_ID" : "ENSP00000447752",
        "isExcludedFromPaths" : false,
        "gene_name" : "SELPLG",
        "log_score" : -3.9366602833337727,
        "name" : "H__sapiens__1_-704799",
        "isInPath" : false,
        "Synonym" : "PSGL-1"
      },
      "position" : {
        "x" : -747.675243398085,
        "y" : -296.75512452967064
      },
      "selected" : true
    }, {
      "data" : {
        "id" : "646",
        "B" : 13.85183,
        "annotations" : [ "GO:0070661", "GO:0050798", "GO:0032609", "GO:0032943", "GO:0042098", "GO:0042110", "GO:0046651" ],
        "annotation_name" : [ "leukocyte proliferation", "activated T cell proliferation", "interferon-gamma production", "mononuclear cell proliferation", "T cell proliferation", "T cell activation", "lymphocyte proliferation" ],
        "shared_name" : "H__sapiens__1_-701435",
        "score" : 0.7797898553091105,
        "RefSeq_mRNA_ID" : "NM_194441",
        "node_type" : "query",
        "Entrez_Gene_ID" : "11119",
        "Ensembl_Gene_ID" : "ENSG00000026950",
        "Gene_title" : "butyrophilin subfamily 3 member A1",
        "SUID" : 646,
        "ID" : "ILMN_1802708",
        "__mclCluster" : 2,
        "RefSeq_Protein_ID" : "NP_919423",
        "Uniprot_ID" : "O00481",
        "selected" : true,
        "Ensembl_Protein_ID" : "ENSP00000427013",
        "logFC" : -0.6821798,
        "isExcludedFromPaths" : false,
        "P_Value" : 9.17E-11,
        "adj_P_Val" : 4.25E-10,
        "gene_name" : "BTN3A1",
        "t" : -10.45141,
        "log_score" : -0.248730811867954,
        "name" : "H__sapiens__1_-701435",
        "isInPath" : false,
        "Synonym" : "CD277"
      },
      "position" : {
        "x" : -948.1990367891322,
        "y" : -254.00650815505372
      },
      "selected" : true
    }, {
      "data" : {
        "id" : "645",
        "B" : 43.04144,
        "annotations" : [ "GO:0048534", "GO:0006367", "GO:0007050", "GO:0045596", "GO:0045165", "GO:0002520", "GO:0007219", "GO:0007220", "GO:0043235", "GO:0030097", "GO:0045786", "GO:0006352" ],
        "annotation_name" : [ "hematopoietic or lymphoid organ development", "transcription initiation from RNA polymerase II promoter", "cell cycle arrest", "negative regulation of cell differentiation", "cell fate commitment", "immune system development", "Notch signaling pathway", "Notch receptor processing", "receptor complex", "hemopoiesis", "negative regulation of cell cycle", "DNA-templated transcription, initiation" ],
        "shared_name" : "H__sapiens__1_-707850",
        "score" : 0.02140308448326983,
        "RefSeq_mRNA_ID" : "NM_024408",
        "node_type" : "result",
        "Entrez_Gene_ID" : "4853",
        "Ensembl_Gene_ID" : "ENSG00000134250",
        "Gene_title" : "notch 2",
        "SUID" : 645,
        "ID" : "ILMN_2405297",
        "__mclCluster" : 1,
        "RefSeq_Protein_ID" : "NP_077719",
        "Uniprot_ID" : "Q04721",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000477065",
        "logFC" : -1.5464992,
        "isExcludedFromPaths" : false,
        "P_Value" : 3.77E-23,
        "adj_P_Val" : 4.34E-21,
        "gene_name" : "NOTCH2",
        "t" : -34.611466,
        "log_score" : -3.844220232608509,
        "name" : "H__sapiens__1_-707850",
        "isInPath" : false,
        "Synonym" : ""
      },
      "position" : {
        "x" : -1572.5956442048025,
        "y" : -175.86351451311276
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "644",
        "B" : -7.7518,
        "annotations" : [ "GO:0006790" ],
        "annotation_name" : [ "sulfur compound metabolic process" ],
        "shared_name" : "H__sapiens__1_-705635",
        "score" : 0.7268741844246734,
        "RefSeq_mRNA_ID" : "NM_001291940",
        "node_type" : "query",
        "Entrez_Gene_ID" : "4548",
        "Ensembl_Gene_ID" : "ENSG00000116984",
        "Gene_title" : "5-methyltetrahydrofolate-homocysteine methyltransferase",
        "SUID" : 644,
        "ID" : "ILMN_1670801",
        "__mclCluster" : 1,
        "RefSeq_Protein_ID" : "NP_001278869",
        "Uniprot_ID" : "Q99707",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000441845",
        "logFC" : -0.1050471,
        "isExcludedFromPaths" : false,
        "P_Value" : 0.313,
        "adj_P_Val" : 0.353,
        "gene_name" : "MTR",
        "t" : -1.029445,
        "log_score" : -0.31900187773952166,
        "name" : "H__sapiens__1_-705635",
        "isInPath" : false,
        "Synonym" : "cblG"
      },
      "position" : {
        "x" : -1490.3872668227805,
        "y" : -324.2479914748222
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "643",
        "B" : 7.44094,
        "annotations" : [ "GO:0006367", "GO:0003713", "GO:0007219", "GO:0006352" ],
        "annotation_name" : [ "transcription initiation from RNA polymerase II promoter", "transcription coactivator activity", "Notch signaling pathway", "DNA-templated transcription, initiation" ],
        "shared_name" : "H__sapiens__1_-716738",
        "score" : 0.02584044269267033,
        "RefSeq_mRNA_ID" : "NM_032427",
        "node_type" : "result",
        "Entrez_Gene_ID" : "84441",
        "Ensembl_Gene_ID" : "ENSG00000184384",
        "Gene_title" : "mastermind like transcriptional coactivator 2",
        "SUID" : 643,
        "ID" : "ILMN_1765729",
        "__mclCluster" : 1,
        "RefSeq_Protein_ID" : "NP_115803",
        "Uniprot_ID" : "Q8IZL2",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000483371",
        "logFC" : -0.2611291,
        "isExcludedFromPaths" : false,
        "P_Value" : 4.79E-8,
        "adj_P_Val" : 1.46E-7,
        "gene_name" : "MAML2",
        "t" : -7.601405,
        "log_score" : -3.655814468145203,
        "name" : "H__sapiens__1_-716738",
        "isInPath" : false,
        "Synonym" : "MAM3"
      },
      "position" : {
        "x" : -1533.5577392578125,
        "y" : -257.86525401506583
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "642",
        "B" : 31.24256,
        "annotations" : [ "GO:0000976", "GO:0097191", "GO:0006611", "GO:0030178", "GO:0000975", "GO:0001568", "GO:2001233", "GO:0000978", "GO:0090090", "GO:0001067", "GO:0048660", "GO:0007050", "GO:0060828", "GO:0060070", "GO:0048659", "GO:0030111", "GO:0033002", "GO:0016055", "GO:0045786", "GO:0097285", "GO:0001159", "GO:0019901", "GO:2001236", "GO:0006790", "GO:0000987", "GO:2001237", "GO:0001085", "GO:0043565", "GO:0045165", "GO:0044212", "GO:0008013", "GO:0046825" ],
        "annotation_name" : [ "transcription regulatory region sequence-specific DNA binding", "extrinsic apoptotic signaling pathway", "protein export from nucleus", "negative regulation of Wnt signaling pathway", "regulatory region DNA binding", "blood vessel development", "regulation of apoptotic signaling pathway", "RNA polymerase II core promoter proximal region sequence-specific DNA binding", "negative regulation of canonical Wnt signaling pathway", "regulatory region nucleic acid binding", "regulation of smooth muscle cell proliferation", "cell cycle arrest", "regulation of canonical Wnt signaling pathway", "canonical Wnt signaling pathway", "smooth muscle cell proliferation", "regulation of Wnt signaling pathway", "muscle cell proliferation", "Wnt signaling pathway", "negative regulation of cell cycle", "cell-type specific apoptotic process", "core promoter proximal region DNA binding", "protein kinase binding", "regulation of extrinsic apoptotic signaling pathway", "sulfur compound metabolic process", "core promoter proximal region sequence-specific DNA binding", "negative regulation of extrinsic apoptotic signaling pathway", "RNA polymerase II transcription factor binding", "sequence-specific DNA binding", "cell fate commitment", "transcription regulatory region DNA binding", "beta-catenin binding", "regulation of protein export from nucleus" ],
        "shared_name" : "H__sapiens__1_-710126",
        "score" : 0.6562719463847569,
        "RefSeq_mRNA_ID" : "NM_030756",
        "node_type" : "query",
        "Entrez_Gene_ID" : "6934",
        "Ensembl_Gene_ID" : "ENSG00000148737",
        "Gene_title" : "transcription factor 7 like 2",
        "SUID" : 642,
        "ID" : "ILMN_1672486",
        "__mclCluster" : 8,
        "RefSeq_Protein_ID" : "NP_110383",
        "Uniprot_ID" : "Q9NQB0",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000490478",
        "logFC" : -1.0666529,
        "isExcludedFromPaths" : false,
        "P_Value" : 4.09E-18,
        "adj_P_Val" : 9.61E-17,
        "gene_name" : "TCF7L2",
        "t" : -21.747181,
        "log_score" : -0.42118002352537265,
        "name" : "H__sapiens__1_-710126",
        "isInPath" : false,
        "Synonym" : "TCF-4"
      },
      "position" : {
        "x" : 1594.3460693359375,
        "y" : 436.87278838514624
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "641",
        "B" : 15.3516,
        "annotations" : [ "" ],
        "annotation_name" : [ "" ],
        "shared_name" : "H__sapiens__1_-704846",
        "score" : 0.6685551467264992,
        "RefSeq_mRNA_ID" : "NM_005475",
        "node_type" : "query",
        "Entrez_Gene_ID" : "10019",
        "Ensembl_Gene_ID" : "ENSG00000111252",
        "Gene_title" : "SH2B adaptor protein 3",
        "SUID" : 641,
        "ID" : "ILMN_1752046",
        "__mclCluster" : 2,
        "RefSeq_Protein_ID" : "NP_005466",
        "Uniprot_ID" : "Q9UQQ2",
        "selected" : true,
        "Ensembl_Protein_ID" : "ENSP00000473529",
        "logFC" : -0.9274106,
        "isExcludedFromPaths" : false,
        "P_Value" : 2.13E-11,
        "adj_P_Val" : 1.09E-10,
        "gene_name" : "SH2B3",
        "t" : -11.192971,
        "log_score" : -0.4026363926091618,
        "name" : "H__sapiens__1_-704846",
        "isInPath" : false,
        "Synonym" : "LNK"
      },
      "position" : {
        "x" : -765.1438991548548,
        "y" : -151.52628354567872
      },
      "selected" : true
    }, {
      "data" : {
        "id" : "640",
        "annotations" : [ "GO:0043122", "GO:0007249" ],
        "annotation_name" : [ "regulation of I-kappaB kinase/NF-kappaB signaling", "I-kappaB kinase/NF-kappaB signaling" ],
        "shared_name" : "H__sapiens__1_-711862",
        "score" : 0.6569485538065203,
        "RefSeq_mRNA_ID" : "NM_002908",
        "node_type" : "query",
        "Entrez_Gene_ID" : "5966",
        "Ensembl_Gene_ID" : "ENSG00000162924",
        "Gene_title" : "",
        "SUID" : 640,
        "ID" : "",
        "__mclCluster" : 1,
        "RefSeq_Protein_ID" : "NP_002899",
        "Uniprot_ID" : "Q04864",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000377989",
        "isExcludedFromPaths" : false,
        "gene_name" : "REL",
        "log_score" : -0.420149568271993,
        "name" : "H__sapiens__1_-711862",
        "isInPath" : false,
        "Synonym" : "I-Rel"
      },
      "position" : {
        "x" : -1493.5332323989524,
        "y" : -234.3603495680839
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "639",
        "B" : 8.02821,
        "annotations" : [ "GO:0015850" ],
        "annotation_name" : [ "organic hydroxy compound transport" ],
        "shared_name" : "H__sapiens__1_-718375",
        "score" : 0.739172471442113,
        "RefSeq_mRNA_ID" : "NM_003059",
        "node_type" : "query",
        "Entrez_Gene_ID" : "6583",
        "Ensembl_Gene_ID" : "ENSG00000197208",
        "Gene_title" : "solute carrier family 22 member 4",
        "SUID" : 639,
        "ID" : "ILMN_1685057",
        "__mclCluster" : 6,
        "RefSeq_Protein_ID" : "NP_003050",
        "Uniprot_ID" : "Q9H015",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000200652",
        "logFC" : 0.3002903,
        "isExcludedFromPaths" : false,
        "P_Value" : 2.7E-8,
        "adj_P_Val" : 8.51E-8,
        "gene_name" : "SLC22A4",
        "t" : 7.844358,
        "log_score" : -0.302224000362584,
        "name" : "H__sapiens__1_-718375",
        "isInPath" : false,
        "Synonym" : "OCTN1"
      },
      "position" : {
        "x" : 1621.5051322820364,
        "y" : -245.83418164934355
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "638",
        "annotations" : [ "GO:0006367", "GO:0003713", "GO:0007219", "GO:0006352" ],
        "annotation_name" : [ "transcription initiation from RNA polymerase II promoter", "transcription coactivator activity", "Notch signaling pathway", "DNA-templated transcription, initiation" ],
        "shared_name" : "H__sapiens__1_-718245",
        "score" : 0.6903476274567055,
        "RefSeq_mRNA_ID" : "NM_018717",
        "node_type" : "query",
        "Entrez_Gene_ID" : "55534",
        "Ensembl_Gene_ID" : "ENSG00000196782",
        "Gene_title" : "",
        "SUID" : 638,
        "ID" : "",
        "RefSeq_Protein_ID" : "NP_061187",
        "Uniprot_ID" : "Q96JK9",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000422783",
        "isExcludedFromPaths" : false,
        "gene_name" : "MAML3",
        "log_score" : -0.3705600003511575,
        "name" : "H__sapiens__1_-718245",
        "isInPath" : false,
        "Synonym" : "TNRC3"
      },
      "position" : {
        "x" : 654.5301986296136,
        "y" : 452.4901232605526
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "637",
        "B" : 3.59703,
        "annotations" : [ "GO:0097191", "GO:0097194", "GO:0010952", "GO:0043280", "GO:2001239", "GO:0002520", "GO:0045649", "GO:2001233", "GO:0002020", "GO:0048534", "GO:0071260", "GO:2001056", "GO:0002521", "GO:0030097", "GO:0002763", "GO:0045639", "GO:2001236", "GO:1902105", "GO:1902107", "GO:0097192", "GO:0038034", "GO:0010950", "GO:0042110", "GO:0030101", "GO:0045862", "GO:0043122", "GO:0007249", "GO:0010954" ],
        "annotation_name" : [ "extrinsic apoptotic signaling pathway", "execution phase of apoptosis", "positive regulation of peptidase activity", "positive regulation of cysteine-type endopeptidase activity involved in apoptotic process", "regulation of extrinsic apoptotic signaling pathway in absence of ligand", "immune system development", "regulation of macrophage differentiation", "regulation of apoptotic signaling pathway", "protease binding", "hematopoietic or lymphoid organ development", "cellular response to mechanical stimulus", "positive regulation of cysteine-type endopeptidase activity", "leukocyte differentiation", "hemopoiesis", "positive regulation of myeloid leukocyte differentiation", "positive regulation of myeloid cell differentiation", "regulation of extrinsic apoptotic signaling pathway", "regulation of leukocyte differentiation", "positive regulation of leukocyte differentiation", "extrinsic apoptotic signaling pathway in absence of ligand", "signal transduction in absence of ligand", "positive regulation of endopeptidase activity", "T cell activation", "natural killer cell activation", "positive regulation of proteolysis", "regulation of I-kappaB kinase/NF-kappaB signaling", "I-kappaB kinase/NF-kappaB signaling", "positive regulation of protein processing" ],
        "shared_name" : "H__sapiens__1_-701844",
        "score" : 0.5807457775108202,
        "RefSeq_mRNA_ID" : "NM_033358",
        "node_type" : "query",
        "Entrez_Gene_ID" : "841",
        "Ensembl_Gene_ID" : "ENSG00000064012",
        "Gene_title" : "caspase 8",
        "SUID" : 637,
        "ID" : "ILMN_1673757",
        "__mclCluster" : 9,
        "RefSeq_Protein_ID" : "NP_203522",
        "Uniprot_ID" : "Q14790",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000412523",
        "logFC" : -0.1654386,
        "isExcludedFromPaths" : false,
        "P_Value" : 2.1E-6,
        "adj_P_Val" : 5.1E-6,
        "gene_name" : "CASP8",
        "t" : -6.074959,
        "log_score" : -0.5434421781099744,
        "name" : "H__sapiens__1_-701844",
        "isInPath" : false,
        "Synonym" : "MCH5"
      },
      "position" : {
        "x" : 2099.3701171875,
        "y" : 441.621657415635
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "636",
        "B" : 12.26017,
        "annotations" : [ "GO:0050870", "GO:0097191", "GO:0071887", "GO:0032944", "GO:0010952", "GO:0042129", "GO:0051251", "GO:0032609", "GO:0046651", "GO:0001906", "GO:0071260", "GO:0042102", "GO:0050671", "GO:0001910", "GO:0002696", "GO:0002521", "GO:0010942", "GO:0030097", "GO:0033077", "GO:0032649", "GO:0002705", "GO:1902105", "GO:1902107", "GO:0050863", "GO:0097192", "GO:0042104", "GO:0010950", "GO:0002456", "GO:0045862", "GO:0001912", "GO:0070665", "GO:0002821", "GO:0050670", "GO:0001913", "GO:0001914", "GO:0043280", "GO:2001239", "GO:0001819", "GO:0002520", "GO:0045649", "GO:0002709", "GO:2001233", "GO:0002020", "GO:0002711", "GO:0070661", "GO:0048534", "GO:0050798", "GO:0001909", "GO:0002460", "GO:2001056", "GO:0002824", "GO:0050867", "GO:0002706", "GO:0032946", "GO:0046006", "GO:0002763", "GO:0097285", "GO:0045639", "GO:2001236", "GO:0030217", "GO:0030098", "GO:0050865", "GO:0002708", "GO:0038034", "GO:0042110", "GO:0032729", "GO:0031343", "GO:0001916", "GO:0002694", "GO:0051249", "GO:0043122", "GO:0007249", "GO:0010954", "GO:0032943", "GO:0042098", "GO:0070663", "GO:0031341" ],
        "annotation_name" : [ "positive regulation of T cell activation", "extrinsic apoptotic signaling pathway", "leukocyte apoptotic process", "regulation of mononuclear cell proliferation", "positive regulation of peptidase activity", "regulation of T cell proliferation", "positive regulation of lymphocyte activation", "interferon-gamma production", "lymphocyte proliferation", "cell killing", "cellular response to mechanical stimulus", "positive regulation of T cell proliferation", "positive regulation of lymphocyte proliferation", "regulation of leukocyte mediated cytotoxicity", "positive regulation of leukocyte activation", "leukocyte differentiation", "positive regulation of cell death", "hemopoiesis", "T cell differentiation in thymus", "regulation of interferon-gamma production", "positive regulation of leukocyte mediated immunity", "regulation of leukocyte differentiation", "positive regulation of leukocyte differentiation", "regulation of T cell activation", "extrinsic apoptotic signaling pathway in absence of ligand", "positive regulation of activated T cell proliferation", "positive regulation of endopeptidase activity", "T cell mediated immunity", "positive regulation of proteolysis", "positive regulation of leukocyte mediated cytotoxicity", "positive regulation of leukocyte proliferation", "positive regulation of adaptive immune response", "regulation of lymphocyte proliferation", "T cell mediated cytotoxicity", "regulation of T cell mediated cytotoxicity", "positive regulation of cysteine-type endopeptidase activity involved in apoptotic process", "regulation of extrinsic apoptotic signaling pathway in absence of ligand", "positive regulation of cytokine production", "immune system development", "regulation of macrophage differentiation", "regulation of T cell mediated immunity", "regulation of apoptotic signaling pathway", "protease binding", "positive regulation of T cell mediated immunity", "leukocyte proliferation", "hematopoietic or lymphoid organ development", "activated T cell proliferation", "leukocyte mediated cytotoxicity", "adaptive immune response based on somatic recombination of immune receptors built from immunoglobulin superfamily domains", "positive regulation of cysteine-type endopeptidase activity", "positive regulation of adaptive immune response based on somatic recombination of immune receptors built from immunoglobulin superfamily domains", "positive regulation of cell activation", "regulation of lymphocyte mediated immunity", "positive regulation of mononuclear cell proliferation", "regulation of activated T cell proliferation", "positive regulation of myeloid leukocyte differentiation", "cell-type specific apoptotic process", "positive regulation of myeloid cell differentiation", "regulation of extrinsic apoptotic signaling pathway", "T cell differentiation", "lymphocyte differentiation", "regulation of cell activation", "positive regulation of lymphocyte mediated immunity", "signal transduction in absence of ligand", "T cell activation", "positive regulation of interferon-gamma production", "positive regulation of cell killing", "positive regulation of T cell mediated cytotoxicity", "regulation of leukocyte activation", "regulation of lymphocyte activation", "regulation of I-kappaB kinase/NF-kappaB signaling", "I-kappaB kinase/NF-kappaB signaling", "positive regulation of protein processing", "mononuclear cell proliferation", "T cell proliferation", "regulation of leukocyte proliferation", "regulation of cell killing" ],
        "shared_name" : "H__sapiens__1_-713265",
        "score" : 0.019244810879498087,
        "RefSeq_mRNA_ID" : "NM_003824",
        "node_type" : "result",
        "Entrez_Gene_ID" : "8772",
        "Ensembl_Gene_ID" : "ENSG00000168040",
        "Gene_title" : "Fas associated via death domain",
        "SUID" : 636,
        "ID" : "ILMN_1758658",
        "__mclCluster" : 2,
        "RefSeq_Protein_ID" : "NP_003815",
        "Uniprot_ID" : "Q13158",
        "selected" : true,
        "Ensembl_Protein_ID" : "ENSP00000301838",
        "logFC" : -0.5487496,
        "isExcludedFromPaths" : false,
        "P_Value" : 4.32E-10,
        "adj_P_Val" : 1.78E-9,
        "gene_name" : "FADD",
        "t" : -9.698668,
        "log_score" : -3.9505138192888367,
        "name" : "H__sapiens__1_-713265",
        "isInPath" : false,
        "Synonym" : "MORT1"
      },
      "position" : {
        "x" : -897.4564945222864,
        "y" : -256.04096250075685
      },
      "selected" : true
    }, {
      "data" : {
        "id" : "635",
        "B" : 2.79213,
        "annotations" : [ "GO:0097191", "GO:2001236", "GO:0051153", "GO:0097192", "GO:2001239", "GO:0038034", "GO:2001233", "GO:0002020", "GO:2001237", "GO:0051091", "GO:0043122", "GO:0007249", "GO:0014902", "GO:0010830" ],
        "annotation_name" : [ "extrinsic apoptotic signaling pathway", "regulation of extrinsic apoptotic signaling pathway", "regulation of striated muscle cell differentiation", "extrinsic apoptotic signaling pathway in absence of ligand", "regulation of extrinsic apoptotic signaling pathway in absence of ligand", "signal transduction in absence of ligand", "regulation of apoptotic signaling pathway", "protease binding", "negative regulation of extrinsic apoptotic signaling pathway", "positive regulation of sequence-specific DNA binding transcription factor activity", "regulation of I-kappaB kinase/NF-kappaB signaling", "I-kappaB kinase/NF-kappaB signaling", "myotube differentiation", "regulation of myotube differentiation" ],
        "shared_name" : "H__sapiens__1_-701014",
        "score" : 0.037619695278954646,
        "RefSeq_mRNA_ID" : "NM_003879",
        "node_type" : "result",
        "Entrez_Gene_ID" : "8837",
        "Ensembl_Gene_ID" : "ENSG00000003402",
        "Gene_title" : "CASP8 and FADD like apoptosis regulator",
        "SUID" : 635,
        "ID" : "ILMN_1789830",
        "__mclCluster" : 2,
        "RefSeq_Protein_ID" : "NP_003870",
        "Uniprot_ID" : "O15519",
        "selected" : true,
        "Ensembl_Protein_ID" : "ENSP00000471805",
        "logFC" : -0.2714946,
        "isExcludedFromPaths" : false,
        "P_Value" : 4.65E-6,
        "adj_P_Val" : 1.08E-5,
        "gene_name" : "CFLAR",
        "t" : -5.765772,
        "log_score" : -3.2802275551166815,
        "name" : "H__sapiens__1_-701014",
        "isInPath" : false,
        "Synonym" : "MRIT"
      },
      "position" : {
        "x" : -813.2487728120814,
        "y" : -316.1767657234131
      },
      "selected" : true
    }, {
      "data" : {
        "id" : "634",
        "B" : -5.84387,
        "annotations" : [ "GO:0097191", "GO:2001236", "GO:0051091", "GO:2001233" ],
        "annotation_name" : [ "extrinsic apoptotic signaling pathway", "regulation of extrinsic apoptotic signaling pathway", "positive regulation of sequence-specific DNA binding transcription factor activity", "regulation of apoptotic signaling pathway" ],
        "shared_name" : "H__sapiens__1_-701737",
        "score" : 0.020575045353363353,
        "RefSeq_mRNA_ID" : "NM_005658",
        "node_type" : "result",
        "Entrez_Gene_ID" : "7185",
        "Ensembl_Gene_ID" : "ENSG00000056558",
        "Gene_title" : "TNF receptor associated factor 1",
        "SUID" : 634,
        "ID" : "ILMN_1698218",
        "RefSeq_Protein_ID" : "NP_005649",
        "Uniprot_ID" : "Q13077",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000443183",
        "logFC" : -0.0839758,
        "isExcludedFromPaths" : false,
        "P_Value" : 0.0315,
        "adj_P_Val" : 0.0419,
        "gene_name" : "TRAF1",
        "t" : -2.274393,
        "log_score" : -3.8836763281267377,
        "name" : "H__sapiens__1_-701737",
        "isInPath" : false,
        "Synonym" : "EBI6"
      },
      "position" : {
        "x" : 982.1575425944843,
        "y" : 422.1309789601756
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "633",
        "annotations" : [ "" ],
        "annotation_name" : [ "" ],
        "shared_name" : "H__sapiens__1_-706527",
        "score" : 0.7391994101476305,
        "RefSeq_mRNA_ID" : "NM_001732",
        "node_type" : "query",
        "Entrez_Gene_ID" : "696",
        "Ensembl_Gene_ID" : "ENSG00000124557",
        "Gene_title" : "",
        "SUID" : 633,
        "ID" : "",
        "RefSeq_Protein_ID" : "NP_001723",
        "Uniprot_ID" : "Q13410",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000484707",
        "isExcludedFromPaths" : false,
        "gene_name" : "BTN1A1",
        "log_score" : -0.30218755661543767,
        "name" : "H__sapiens__1_-706527",
        "isInPath" : false,
        "Synonym" : "BTN1"
      },
      "position" : {
        "x" : 1022.0410261352458,
        "y" : 635.4887711770245
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "632",
        "annotations" : [ "GO:0050870", "GO:0050865", "GO:0034341", "GO:0002694", "GO:0051249", "GO:0050863", "GO:0051251", "GO:0002696", "GO:0042110", "GO:0050867" ],
        "annotation_name" : [ "positive regulation of T cell activation", "regulation of cell activation", "response to interferon-gamma", "regulation of leukocyte activation", "regulation of lymphocyte activation", "regulation of T cell activation", "positive regulation of lymphocyte activation", "positive regulation of leukocyte activation", "T cell activation", "positive regulation of cell activation" ],
        "shared_name" : "H__sapiens__1_-721468",
        "score" : 0.8414639365773404,
        "RefSeq_mRNA_ID" : "NM_020056",
        "node_type" : "query",
        "Entrez_Gene_ID" : "3118",
        "Ensembl_Gene_ID" : "ENSG00000257473",
        "Gene_title" : "",
        "SUID" : 632,
        "ID" : "",
        "__mclCluster" : 13,
        "RefSeq_Protein_ID" : "NP_064440",
        "Uniprot_ID" : "P01906",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000448003",
        "isExcludedFromPaths" : false,
        "gene_name" : "HLA-DQA2",
        "log_score" : -0.17261212238686852,
        "name" : "H__sapiens__1_-721468",
        "isInPath" : false,
        "Synonym" : "HLA-DXA"
      },
      "position" : {
        "x" : 23.65167011489575,
        "y" : 454.86161259295426
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "631",
        "B" : 42.29593,
        "annotations" : [ "GO:0097194", "GO:0010952", "GO:0043280", "GO:2001056", "GO:0010950", "GO:2001233" ],
        "annotation_name" : [ "execution phase of apoptosis", "positive regulation of peptidase activity", "positive regulation of cysteine-type endopeptidase activity involved in apoptotic process", "positive regulation of cysteine-type endopeptidase activity", "positive regulation of endopeptidase activity", "regulation of apoptotic signaling pathway" ],
        "shared_name" : "H__sapiens__1_-717093",
        "score" : 0.024854555380215915,
        "RefSeq_mRNA_ID" : "NM_005745",
        "node_type" : "result",
        "Entrez_Gene_ID" : "10134",
        "Ensembl_Gene_ID" : "ENSG00000185825",
        "Gene_title" : "B-cell receptor-associated protein 31",
        "SUID" : 631,
        "ID" : "ILMN_1812403",
        "__mclCluster" : 13,
        "RefSeq_Protein_ID" : "NP_005736",
        "Uniprot_ID" : "P51572",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000409888",
        "logFC" : -1.6344436,
        "isExcludedFromPaths" : false,
        "P_Value" : 7.9E-23,
        "adj_P_Val" : 8.16E-21,
        "gene_name" : "BCAP31",
        "t" : -33.613872,
        "log_score" : -3.694714228140427,
        "name" : "H__sapiens__1_-717093",
        "isInPath" : false,
        "Synonym" : "DXS1357E"
      },
      "position" : {
        "x" : 223.65167011489575,
        "y" : 454.86161259295426
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "630",
        "B" : -6.12389,
        "annotations" : [ "GO:0001067", "GO:0045596", "GO:0051153", "GO:0014902", "GO:0000975", "GO:0010830", "GO:0044212" ],
        "annotation_name" : [ "regulatory region nucleic acid binding", "negative regulation of cell differentiation", "regulation of striated muscle cell differentiation", "myotube differentiation", "regulatory region DNA binding", "regulation of myotube differentiation", "transcription regulatory region DNA binding" ],
        "shared_name" : "H__sapiens__1_-709018",
        "score" : 0.6733466291848449,
        "RefSeq_mRNA_ID" : "NM_006885",
        "node_type" : "query",
        "Entrez_Gene_ID" : "463",
        "Ensembl_Gene_ID" : "ENSG00000140836",
        "Gene_title" : "zinc finger homeobox 3",
        "SUID" : 630,
        "ID" : "ILMN_1808587",
        "__mclCluster" : 6,
        "RefSeq_Protein_ID" : "NP_008816",
        "Uniprot_ID" : "Q15911",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000438926",
        "logFC" : -0.1714381,
        "isExcludedFromPaths" : false,
        "P_Value" : 0.043,
        "adj_P_Val" : 0.0559,
        "gene_name" : "ZFHX3",
        "t" : -2.128819,
        "log_score" : -0.3954950311358261,
        "name" : "H__sapiens__1_-709018",
        "isInPath" : false,
        "Synonym" : "ZNF927"
      },
      "position" : {
        "x" : 1761.1812817423606,
        "y" : -308.34776490209674
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "629",
        "B" : 4.66105,
        "annotations" : [ "GO:0002237", "GO:0009617", "GO:0032481", "GO:0034341", "GO:0001819", "GO:0010942", "GO:0032479", "GO:0032606" ],
        "annotation_name" : [ "response to molecule of bacterial origin", "response to bacterium", "positive regulation of type I interferon production", "response to interferon-gamma", "positive regulation of cytokine production", "positive regulation of cell death", "regulation of type I interferon production", "type I interferon production" ],
        "shared_name" : "H__sapiens__1_-707031",
        "score" : 0.7449372045600712,
        "RefSeq_mRNA_ID" : "NM_032643",
        "node_type" : "query",
        "Entrez_Gene_ID" : "3663",
        "Ensembl_Gene_ID" : "ENSG00000128604",
        "Gene_title" : "interferon regulatory factor 5",
        "SUID" : 629,
        "ID" : "ILMN_2312606",
        "__mclCluster" : 2,
        "RefSeq_Protein_ID" : "NP_116032",
        "Uniprot_ID" : "Q13568",
        "selected" : true,
        "Ensembl_Protein_ID" : "ENSP00000483292",
        "logFC" : -0.4366195,
        "isExcludedFromPaths" : false,
        "P_Value" : 7.34E-7,
        "adj_P_Val" : 1.9E-6,
        "gene_name" : "IRF5",
        "t" : -6.488144,
        "log_score" : -0.2944553533362227,
        "name" : "H__sapiens__1_-707031",
        "isInPath" : false,
        "Synonym" : ""
      },
      "position" : {
        "x" : -836.9723289530544,
        "y" : -261.0334777832031
      },
      "selected" : true
    }, {
      "data" : {
        "id" : "628",
        "B" : 1.88366,
        "annotations" : [ "GO:0001159", "GO:0008589", "GO:0000976", "GO:0006367", "GO:0008080", "GO:0031365", "GO:0002039", "GO:0000975", "GO:0001819", "GO:0000987", "GO:0006474", "GO:0007219", "GO:0032479", "GO:0007224", "GO:0032606", "GO:0001085", "GO:0001067", "GO:0032481", "GO:0043565", "GO:0003713", "GO:0044212", "GO:0006352" ],
        "annotation_name" : [ "core promoter proximal region DNA binding", "regulation of smoothened signaling pathway", "transcription regulatory region sequence-specific DNA binding", "transcription initiation from RNA polymerase II promoter", "N-acetyltransferase activity", "N-terminal protein amino acid modification", "p53 binding", "regulatory region DNA binding", "positive regulation of cytokine production", "core promoter proximal region sequence-specific DNA binding", "N-terminal protein amino acid acetylation", "Notch signaling pathway", "regulation of type I interferon production", "smoothened signaling pathway", "type I interferon production", "RNA polymerase II transcription factor binding", "regulatory region nucleic acid binding", "positive regulation of type I interferon production", "sequence-specific DNA binding", "transcription coactivator activity", "transcription regulatory region DNA binding", "DNA-templated transcription, initiation" ],
        "shared_name" : "H__sapiens__1_-701067",
        "score" : 0.4985513297275635,
        "RefSeq_mRNA_ID" : "NM_004380",
        "node_type" : "query",
        "Entrez_Gene_ID" : "1387",
        "Ensembl_Gene_ID" : "ENSG00000005339",
        "Gene_title" : "CREB binding protein",
        "SUID" : 628,
        "ID" : "ILMN_2293692",
        "__mclCluster" : 1,
        "RefSeq_Protein_ID" : "NP_004371",
        "Uniprot_ID" : "Q92793",
        "selected" : false,
        "Ensembl_Protein_ID" : "ENSP00000490244",
        "logFC" : 0.1708933,
        "isExcludedFromPaths" : false,
        "P_Value" : 1.14E-5,
        "adj_P_Val" : 2.53E-5,
        "gene_name" : "CREBBP",
        "t" : 5.419275,
        "log_score" : -0.6960487265209141,
        "name" : "H__sapiens__1_-701067",
        "isInPath" : false,
        "Synonym" : "RSTS"
      },
      "position" : {
        "x" : -1578.8143603286399,
        "y" : -253.9088572585136
      },
      "selected" : false
    } ],
    "edges" : [ {
      "data" : {
        "id" : "1183",
        "source" : "694",
        "target" : "637",
        "shared_name" : "H__sapiens__1_-713636|H__sapiens__1_-701844|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.03503337792045997,
        "shared_interaction" : "",
        "raw_weights" : [ 0.22131742537021637 ],
        "name" : "H__sapiens__1_-713636|H__sapiens__1_-701844|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1183,
        "networks" : [ "IMID" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1182",
        "source" : "694",
        "target" : "659",
        "shared_name" : "H__sapiens__1_-713636|H__sapiens__1_-713803|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 5.981041447811013E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 5.747989634983242E-4 ],
        "name" : "H__sapiens__1_-713636|H__sapiens__1_-713803|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1182,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1181",
        "source" : "694",
        "target" : "641",
        "shared_name" : "H__sapiens__1_-713636|H__sapiens__1_-704846|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 9.303761091450955E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 8.941239211708307E-4 ],
        "name" : "H__sapiens__1_-713636|H__sapiens__1_-704846|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1181,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1180",
        "source" : "694",
        "target" : "638",
        "shared_name" : "H__sapiens__1_-713636|H__sapiens__1_-718245|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 4.075926064356105E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 3.9171072421595454E-4 ],
        "name" : "H__sapiens__1_-713636|H__sapiens__1_-718245|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1180,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1179",
        "source" : "694",
        "target" : "679",
        "shared_name" : "H__sapiens__1_-713636|H__sapiens__1_-701028|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 1.783356042747807E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0017138674156740308 ],
        "name" : "H__sapiens__1_-713636|H__sapiens__1_-701028|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1179,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1178",
        "source" : "694",
        "target" : "651",
        "shared_name" : "H__sapiens__1_-713636|H__sapiens__1_-718835|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 1.3565663264674377E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.00130370759870857 ],
        "name" : "H__sapiens__1_-713636|H__sapiens__1_-718835|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1178,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1177",
        "source" : "694",
        "target" : "691",
        "shared_name" : "H__sapiens__1_-713636|H__sapiens__1_-711762|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 3.0095877343603033E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.002892318880185485 ],
        "name" : "H__sapiens__1_-713636|H__sapiens__1_-711762|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1177,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1176",
        "source" : "693",
        "target" : "675",
        "shared_name" : "H__sapiens__1_-710026|H__sapiens__1_-702552|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 2.961891650559267E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.002846481278538704 ],
        "name" : "H__sapiens__1_-710026|H__sapiens__1_-702552|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1176,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1175",
        "source" : "692",
        "target" : "695",
        "shared_name" : "H__sapiens__1_-711599|H__sapiens__1_-712410|Shared protein domains",
        "highlight" : 1,
        "normalized_max_weight" : 1.1361166116283351E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0030731328297406435 ],
        "name" : "H__sapiens__1_-711599|H__sapiens__1_-712410|Shared protein domains",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1175,
        "networks" : [ "INTERPRO" ],
        "selected" : false,
        "data_type" : "Shared protein domains"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1174",
        "source" : "692",
        "target" : "689",
        "shared_name" : "H__sapiens__1_-711599|H__sapiens__1_-717747|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 5.150059455566096E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 4.949386930093169E-4 ],
        "name" : "H__sapiens__1_-711599|H__sapiens__1_-717747|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1174,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1173",
        "source" : "691",
        "target" : "690",
        "shared_name" : "H__sapiens__1_-711762|H__sapiens__1_-713226|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 4.8889422272490635E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.004698444157838821 ],
        "name" : "H__sapiens__1_-711762|H__sapiens__1_-713226|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1173,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1172",
        "source" : "689",
        "target" : "690",
        "shared_name" : "H__sapiens__1_-717747|H__sapiens__1_-713226|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 6.8941762382664965E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 6.625544046983123E-4 ],
        "name" : "H__sapiens__1_-717747|H__sapiens__1_-713226|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1172,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1171",
        "source" : "688",
        "target" : "684",
        "shared_name" : "H__sapiens__1_-707211|H__sapiens__1_-713444|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 5.626394023701128E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 5.407161079347134E-4 ],
        "name" : "H__sapiens__1_-707211|H__sapiens__1_-713444|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1171,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1170",
        "source" : "688",
        "target" : "689",
        "shared_name" : "H__sapiens__1_-707211|H__sapiens__1_-717747|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 4.344752105502174E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 4.1754584526643157E-4 ],
        "name" : "H__sapiens__1_-707211|H__sapiens__1_-717747|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1170,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1169",
        "source" : "688",
        "target" : "650",
        "shared_name" : "H__sapiens__1_-707211|H__sapiens__1_-713649|Co-localization",
        "highlight" : 1,
        "normalized_max_weight" : 3.1404947517302636E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.01446897815912962 ],
        "name" : "H__sapiens__1_-707211|H__sapiens__1_-713649|Co-localization",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1169,
        "networks" : [ "Johnson-Shoemaker-2003" ],
        "selected" : false,
        "data_type" : "Co-localization"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1168",
        "source" : "688",
        "target" : "683",
        "shared_name" : "H__sapiens__1_-707211|H__sapiens__1_-701859|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 1.5402597694865722E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.014841008000075817 ],
        "name" : "H__sapiens__1_-707211|H__sapiens__1_-701859|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1168,
        "networks" : [ "Bild-Nevins-2006 B" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1167",
        "source" : "687",
        "target" : "666",
        "shared_name" : "H__sapiens__1_-711077|H__sapiens__1_-706362|Predicted",
        "highlight" : 1,
        "normalized_max_weight" : 3.4724661205545753E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.06850723922252655 ],
        "name" : "H__sapiens__1_-711077|H__sapiens__1_-706362|Predicted",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1167,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Predicted"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1166",
        "source" : "687",
        "target" : "660",
        "shared_name" : "H__sapiens__1_-711077|H__sapiens__1_-709312|Predicted",
        "highlight" : 1,
        "normalized_max_weight" : 8.983652761928578E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.17723578214645386 ],
        "name" : "H__sapiens__1_-711077|H__sapiens__1_-709312|Predicted",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1166,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Predicted"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1165",
        "source" : "687",
        "target" : "628",
        "shared_name" : "H__sapiens__1_-711077|H__sapiens__1_-701067|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 5.556739905326098E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0060790046118199825 ],
        "name" : "H__sapiens__1_-711077|H__sapiens__1_-701067|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1165,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1164",
        "source" : "687",
        "target" : "682",
        "shared_name" : "H__sapiens__1_-711077|H__sapiens__1_-702553|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 6.988712679146376E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0076455650851130486 ],
        "name" : "H__sapiens__1_-711077|H__sapiens__1_-702553|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1164,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1163",
        "source" : "687",
        "target" : "666",
        "shared_name" : "H__sapiens__1_-711077|H__sapiens__1_-706362|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.06304590790752071,
        "shared_interaction" : "",
        "raw_weights" : [ 0.6731135249137878 ],
        "name" : "H__sapiens__1_-711077|H__sapiens__1_-706362|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1163,
        "networks" : [ "CELL_MAP" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1162",
        "source" : "687",
        "target" : "686",
        "shared_name" : "H__sapiens__1_-711077|H__sapiens__1_-712776|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 5.73449159597315E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 5.511046620085835E-4 ],
        "name" : "H__sapiens__1_-711077|H__sapiens__1_-712776|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1162,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1161",
        "source" : "687",
        "target" : "640",
        "shared_name" : "H__sapiens__1_-711077|H__sapiens__1_-711862|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 8.781748797089878E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 8.439567172899842E-4 ],
        "name" : "H__sapiens__1_-711077|H__sapiens__1_-711862|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1161,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1160",
        "source" : "687",
        "target" : "630",
        "shared_name" : "H__sapiens__1_-711077|H__sapiens__1_-709018|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 4.679604192730684E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 4.497262998484075E-4 ],
        "name" : "H__sapiens__1_-711077|H__sapiens__1_-709018|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1160,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1159",
        "source" : "687",
        "target" : "684",
        "shared_name" : "H__sapiens__1_-711077|H__sapiens__1_-713444|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 4.1217152351247605E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 3.961112233810127E-4 ],
        "name" : "H__sapiens__1_-711077|H__sapiens__1_-713444|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1159,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1158",
        "source" : "687",
        "target" : "692",
        "shared_name" : "H__sapiens__1_-711077|H__sapiens__1_-711599|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 9.06009668418375E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 8.707069209776819E-4 ],
        "name" : "H__sapiens__1_-711077|H__sapiens__1_-711599|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1158,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1157",
        "source" : "687",
        "target" : "666",
        "shared_name" : "H__sapiens__1_-711077|H__sapiens__1_-706362|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 1.3099690095561434E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.00438153138384223 ],
        "name" : "H__sapiens__1_-711077|H__sapiens__1_-706362|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1157,
        "networks" : [ "Perou-Botstein-2000" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1156",
        "source" : "686",
        "target" : "640",
        "shared_name" : "H__sapiens__1_-712776|H__sapiens__1_-711862|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 0.008769614005081397,
        "shared_interaction" : "",
        "raw_weights" : [ 0.1280638426542282 ],
        "name" : "H__sapiens__1_-712776|H__sapiens__1_-711862|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1156,
        "networks" : [ "Coyaud-Raught-2015" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1155",
        "source" : "686",
        "target" : "682",
        "shared_name" : "H__sapiens__1_-712776|H__sapiens__1_-702553|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 2.8131519384832715E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0024654928129166365, 0.0029486215207725763 ],
        "name" : "H__sapiens__1_-712776|H__sapiens__1_-702553|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1155,
        "networks" : [ "BIOGRID-SMALL-SCALE-STUDIES", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1154",
        "source" : "686",
        "target" : "695",
        "shared_name" : "H__sapiens__1_-712776|H__sapiens__1_-712410|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 9.343446263150586E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.08188750594854355 ],
        "name" : "H__sapiens__1_-712776|H__sapiens__1_-712410|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1154,
        "networks" : [ "BIOGRID-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1153",
        "source" : "686",
        "target" : "682",
        "shared_name" : "H__sapiens__1_-712776|H__sapiens__1_-702553|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.01670356156325574,
        "shared_interaction" : "",
        "raw_weights" : [ 0.17833660542964935, 0.008546222932636738 ],
        "name" : "H__sapiens__1_-712776|H__sapiens__1_-702553|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1153,
        "networks" : [ "CELL_MAP", "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1152",
        "source" : "686",
        "target" : "678",
        "shared_name" : "H__sapiens__1_-712776|H__sapiens__1_-705245|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 1.0011192877643152E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 9.621105855330825E-4 ],
        "name" : "H__sapiens__1_-712776|H__sapiens__1_-705245|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1152,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1151",
        "source" : "686",
        "target" : "638",
        "shared_name" : "H__sapiens__1_-712776|H__sapiens__1_-718245|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 3.130590463531369E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 3.008606727235019E-4 ],
        "name" : "H__sapiens__1_-712776|H__sapiens__1_-718245|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1151,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1150",
        "source" : "686",
        "target" : "671",
        "shared_name" : "H__sapiens__1_-712776|H__sapiens__1_-716648|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 8.231293917288378E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 7.910560816526413E-4 ],
        "name" : "H__sapiens__1_-712776|H__sapiens__1_-716648|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1150,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1149",
        "source" : "686",
        "target" : "685",
        "shared_name" : "H__sapiens__1_-712776|H__sapiens__1_-746649|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 1.2809538692837005E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.001231041387654841 ],
        "name" : "H__sapiens__1_-712776|H__sapiens__1_-746649|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1149,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1148",
        "source" : "686",
        "target" : "660",
        "shared_name" : "H__sapiens__1_-712776|H__sapiens__1_-709312|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 3.055316470565945E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.016339696943759918 ],
        "name" : "H__sapiens__1_-712776|H__sapiens__1_-709312|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1148,
        "networks" : [ "Roth-Zlotnik-2006" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1147",
        "source" : "685",
        "target" : "655",
        "shared_name" : "H__sapiens__1_-746649|H__sapiens__1_-707212|Co-localization",
        "highlight" : 1,
        "normalized_max_weight" : 1.0686414806771426E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.004923475906252861 ],
        "name" : "H__sapiens__1_-746649|H__sapiens__1_-707212|Co-localization",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1147,
        "networks" : [ "Johnson-Shoemaker-2003" ],
        "selected" : false,
        "data_type" : "Co-localization"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1146",
        "source" : "685",
        "target" : "655",
        "shared_name" : "H__sapiens__1_-746649|H__sapiens__1_-707212|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 1.3521533242073934E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.007231255993247032 ],
        "name" : "H__sapiens__1_-746649|H__sapiens__1_-707212|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1146,
        "networks" : [ "Roth-Zlotnik-2006" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1145",
        "source" : "684",
        "target" : "683",
        "shared_name" : "H__sapiens__1_-713444|H__sapiens__1_-701859|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 6.83920197788532E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 6.572711863555014E-4 ],
        "name" : "H__sapiens__1_-713444|H__sapiens__1_-701859|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1145,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1144",
        "source" : "684",
        "target" : "690",
        "shared_name" : "H__sapiens__1_-713444|H__sapiens__1_-713226|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 8.927862888960262E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 8.5799879161641E-4 ],
        "name" : "H__sapiens__1_-713444|H__sapiens__1_-713226|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1144,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1143",
        "source" : "683",
        "target" : "689",
        "shared_name" : "H__sapiens__1_-701859|H__sapiens__1_-717747|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 5.281293278290391E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 5.075507215224206E-4 ],
        "name" : "H__sapiens__1_-701859|H__sapiens__1_-717747|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1143,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1142",
        "source" : "682",
        "target" : "642",
        "shared_name" : "H__sapiens__1_-702553|H__sapiens__1_-710126|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 7.066252546363198E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.00773039273917675 ],
        "name" : "H__sapiens__1_-702553|H__sapiens__1_-710126|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1142,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1141",
        "source" : "682",
        "target" : "674",
        "shared_name" : "H__sapiens__1_-702553|H__sapiens__1_-714657|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 3.326564359224667E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.003196944482624531 ],
        "name" : "H__sapiens__1_-702553|H__sapiens__1_-714657|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1141,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1140",
        "source" : "682",
        "target" : "693",
        "shared_name" : "H__sapiens__1_-702553|H__sapiens__1_-710026|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 3.183244012448033E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0030592086259275675 ],
        "name" : "H__sapiens__1_-702553|H__sapiens__1_-710026|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1140,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1139",
        "source" : "682",
        "target" : "638",
        "shared_name" : "H__sapiens__1_-702553|H__sapiens__1_-718245|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 4.1420604689728046E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0100331399589777 ],
        "name" : "H__sapiens__1_-702553|H__sapiens__1_-718245|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1139,
        "networks" : [ "Boldrick-Relman-2002" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1138",
        "source" : "680",
        "target" : "693",
        "shared_name" : "H__sapiens__1_-710028|H__sapiens__1_-710026|Shared protein domains",
        "highlight" : 1,
        "normalized_max_weight" : 9.049306289940909E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.024477874860167503 ],
        "name" : "H__sapiens__1_-710028|H__sapiens__1_-710026|Shared protein domains",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1138,
        "networks" : [ "INTERPRO" ],
        "selected" : false,
        "data_type" : "Shared protein domains"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1137",
        "source" : "680",
        "target" : "644",
        "shared_name" : "H__sapiens__1_-710028|H__sapiens__1_-705635|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 0.0012953462536352473,
        "shared_interaction" : "",
        "raw_weights" : [ 0.11352628469467163, 0.03204864636063576 ],
        "name" : "H__sapiens__1_-710028|H__sapiens__1_-705635|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1137,
        "networks" : [ "BIOGRID-SMALL-SCALE-STUDIES", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1136",
        "source" : "680",
        "target" : "650",
        "shared_name" : "H__sapiens__1_-710028|H__sapiens__1_-713649|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 2.535964903114842E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.009530281648039818 ],
        "name" : "H__sapiens__1_-710028|H__sapiens__1_-713649|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1136,
        "networks" : [ "Dobbin-Giordano-2005" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1135",
        "source" : "679",
        "target" : "689",
        "shared_name" : "H__sapiens__1_-701028|H__sapiens__1_-717747|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 7.6024917070003805E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 7.306259940378368E-4 ],
        "name" : "H__sapiens__1_-701028|H__sapiens__1_-717747|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1135,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1134",
        "source" : "679",
        "target" : "676",
        "shared_name" : "H__sapiens__1_-701028|H__sapiens__1_-718153|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 0.001067057415209697,
        "shared_interaction" : "",
        "raw_weights" : [ 0.016616949811577797 ],
        "name" : "H__sapiens__1_-701028|H__sapiens__1_-718153|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1134,
        "networks" : [ "Wang-Maris-2006" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1133",
        "source" : "678",
        "target" : "629",
        "shared_name" : "H__sapiens__1_-705245|H__sapiens__1_-707031|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 1.3901048398882264E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.012183108367025852, 0.012077364139258862 ],
        "name" : "H__sapiens__1_-705245|H__sapiens__1_-707031|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1133,
        "networks" : [ "BIOGRID-SMALL-SCALE-STUDIES", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1132",
        "source" : "678",
        "target" : "667",
        "shared_name" : "H__sapiens__1_-705245|H__sapiens__1_-720857|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.0010640810373816268,
        "shared_interaction" : "",
        "raw_weights" : [ 0.011640914715826511 ],
        "name" : "H__sapiens__1_-705245|H__sapiens__1_-720857|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1132,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1131",
        "source" : "678",
        "target" : "638",
        "shared_name" : "H__sapiens__1_-705245|H__sapiens__1_-718245|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.0041913494474024775,
        "shared_interaction" : "",
        "raw_weights" : [ 0.045852843672037125 ],
        "name" : "H__sapiens__1_-705245|H__sapiens__1_-718245|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1131,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1130",
        "source" : "678",
        "target" : "629",
        "shared_name" : "H__sapiens__1_-705245|H__sapiens__1_-707031|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.002866967868343575,
        "shared_interaction" : "",
        "raw_weights" : [ 0.03136427327990532 ],
        "name" : "H__sapiens__1_-705245|H__sapiens__1_-707031|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1130,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1129",
        "source" : "678",
        "target" : "641",
        "shared_name" : "H__sapiens__1_-705245|H__sapiens__1_-704846|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 1.2181139257662248E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.001170650008134544 ],
        "name" : "H__sapiens__1_-705245|H__sapiens__1_-704846|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1129,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1128",
        "source" : "678",
        "target" : "638",
        "shared_name" : "H__sapiens__1_-705245|H__sapiens__1_-718245|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 5.336489216403066E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 5.128552438691258E-4 ],
        "name" : "H__sapiens__1_-705245|H__sapiens__1_-718245|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1128,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1127",
        "source" : "678",
        "target" : "684",
        "shared_name" : "H__sapiens__1_-705245|H__sapiens__1_-713444|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 7.195630697646403E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 6.915252306498587E-4 ],
        "name" : "H__sapiens__1_-705245|H__sapiens__1_-713444|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1127,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1126",
        "source" : "678",
        "target" : "684",
        "shared_name" : "H__sapiens__1_-705245|H__sapiens__1_-713444|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 6.476169187809742E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.010085134766995907 ],
        "name" : "H__sapiens__1_-705245|H__sapiens__1_-713444|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1126,
        "networks" : [ "Wang-Maris-2006" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1125",
        "source" : "677",
        "target" : "667",
        "shared_name" : "H__sapiens__1_-707068|H__sapiens__1_-720857|Shared protein domains",
        "highlight" : 1,
        "normalized_max_weight" : 2.3406373280797117E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.013198883272707462 ],
        "name" : "H__sapiens__1_-707068|H__sapiens__1_-720857|Shared protein domains",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1125,
        "networks" : [ "PFAM" ],
        "selected" : false,
        "data_type" : "Shared protein domains"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1124",
        "source" : "677",
        "target" : "666",
        "shared_name" : "H__sapiens__1_-707068|H__sapiens__1_-706362|Shared protein domains",
        "highlight" : 1,
        "normalized_max_weight" : 4.712780005854438E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0127478102222085 ],
        "name" : "H__sapiens__1_-707068|H__sapiens__1_-706362|Shared protein domains",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1124,
        "networks" : [ "INTERPRO" ],
        "selected" : false,
        "data_type" : "Shared protein domains"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1123",
        "source" : "677",
        "target" : "667",
        "shared_name" : "H__sapiens__1_-707068|H__sapiens__1_-720857|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 0.0012742326785359643,
        "shared_interaction" : "",
        "raw_weights" : [ 0.39352262020111084, 0.20789335668087006 ],
        "name" : "H__sapiens__1_-707068|H__sapiens__1_-720857|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1123,
        "networks" : [ "IREF-HPRD", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1122",
        "source" : "677",
        "target" : "667",
        "shared_name" : "H__sapiens__1_-707068|H__sapiens__1_-720857|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.04155012067623457,
        "shared_interaction" : "",
        "raw_weights" : [ 0.4436124265193939, 0.0678103119134903 ],
        "name" : "H__sapiens__1_-707068|H__sapiens__1_-720857|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1122,
        "networks" : [ "CELL_MAP", "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1121",
        "source" : "677",
        "target" : "678",
        "shared_name" : "H__sapiens__1_-707068|H__sapiens__1_-705245|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 2.4393904538351696E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0023443393874913454 ],
        "name" : "H__sapiens__1_-707068|H__sapiens__1_-705245|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1121,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1120",
        "source" : "677",
        "target" : "642",
        "shared_name" : "H__sapiens__1_-707068|H__sapiens__1_-710126|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 7.943382392543221E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 7.6338677899912E-4 ],
        "name" : "H__sapiens__1_-707068|H__sapiens__1_-710126|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1120,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1119",
        "source" : "677",
        "target" : "679",
        "shared_name" : "H__sapiens__1_-707068|H__sapiens__1_-701028|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 3.337593977152782E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.003207544330507517 ],
        "name" : "H__sapiens__1_-707068|H__sapiens__1_-701028|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1119,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1118",
        "source" : "677",
        "target" : "684",
        "shared_name" : "H__sapiens__1_-707068|H__sapiens__1_-713444|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 1.0285728028890614E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 9.884943719953299E-4 ],
        "name" : "H__sapiens__1_-707068|H__sapiens__1_-713444|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1118,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1117",
        "source" : "677",
        "target" : "667",
        "shared_name" : "H__sapiens__1_-707068|H__sapiens__1_-720857|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 3.9213568247525496E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.01634969562292099, 0.02097124233841896 ],
        "name" : "H__sapiens__1_-707068|H__sapiens__1_-720857|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1117,
        "networks" : [ "Mallon-McKay-2013", "Roth-Zlotnik-2006" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1116",
        "source" : "676",
        "target" : "655",
        "shared_name" : "H__sapiens__1_-718153|H__sapiens__1_-707212|Co-localization",
        "highlight" : 1,
        "normalized_max_weight" : 1.710916894243345E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.007882585749030113 ],
        "name" : "H__sapiens__1_-718153|H__sapiens__1_-707212|Co-localization",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1116,
        "networks" : [ "Johnson-Shoemaker-2003" ],
        "selected" : false,
        "data_type" : "Co-localization"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1115",
        "source" : "676",
        "target" : "655",
        "shared_name" : "H__sapiens__1_-718153|H__sapiens__1_-707212|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 3.341499225615724E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.009854142554104328, 0.017870189622044563 ],
        "name" : "H__sapiens__1_-718153|H__sapiens__1_-707212|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1115,
        "networks" : [ "Dobbin-Giordano-2005", "Roth-Zlotnik-2006" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1114",
        "source" : "676",
        "target" : "672",
        "shared_name" : "H__sapiens__1_-718153|H__sapiens__1_-702208|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 3.999991198913131E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.018663005903363228 ],
        "name" : "H__sapiens__1_-718153|H__sapiens__1_-702208|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1114,
        "networks" : [ "Mallon-McKay-2013" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1113",
        "source" : "674",
        "target" : "675",
        "shared_name" : "H__sapiens__1_-714657|H__sapiens__1_-702552|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 3.095245947977492E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0029746394138783216 ],
        "name" : "H__sapiens__1_-714657|H__sapiens__1_-702552|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1113,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1112",
        "source" : "674",
        "target" : "675",
        "shared_name" : "H__sapiens__1_-714657|H__sapiens__1_-702552|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 1.6067264739926313E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.010580492205917835 ],
        "name" : "H__sapiens__1_-714657|H__sapiens__1_-702552|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1112,
        "networks" : [ "Noble-Diehl-2008" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1111",
        "source" : "672",
        "target" : "690",
        "shared_name" : "H__sapiens__1_-702208|H__sapiens__1_-713226|Shared protein domains",
        "highlight" : 1,
        "normalized_max_weight" : 0.007393866074143461,
        "shared_interaction" : "",
        "raw_weights" : [ 0.20000000298023224, 0.20000000298023224 ],
        "name" : "H__sapiens__1_-702208|H__sapiens__1_-713226|Shared protein domains",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1111,
        "networks" : [ "INTERPRO", "PFAM" ],
        "selected" : false,
        "data_type" : "Shared protein domains"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1110",
        "source" : "671",
        "target" : "685",
        "shared_name" : "H__sapiens__1_-716648|H__sapiens__1_-746649|Shared protein domains",
        "highlight" : 1,
        "normalized_max_weight" : 6.46108445319868E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.03643413633108139 ],
        "name" : "H__sapiens__1_-716648|H__sapiens__1_-746649|Shared protein domains",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1110,
        "networks" : [ "PFAM" ],
        "selected" : false,
        "data_type" : "Shared protein domains"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1109",
        "source" : "671",
        "target" : "693",
        "shared_name" : "H__sapiens__1_-716648|H__sapiens__1_-710026|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 1.9075242814705303E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0018331974279135466 ],
        "name" : "H__sapiens__1_-716648|H__sapiens__1_-710026|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1109,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1108",
        "source" : "671",
        "target" : "689",
        "shared_name" : "H__sapiens__1_-716648|H__sapiens__1_-717747|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 4.568630866717794E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 4.390613758005202E-4 ],
        "name" : "H__sapiens__1_-716648|H__sapiens__1_-717747|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1108,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1107",
        "source" : "671",
        "target" : "673",
        "shared_name" : "H__sapiens__1_-716648|H__sapiens__1_-720891|Co-localization",
        "highlight" : 1,
        "normalized_max_weight" : 2.7696064332965114E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.012760210782289505 ],
        "name" : "H__sapiens__1_-716648|H__sapiens__1_-720891|Co-localization",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1107,
        "networks" : [ "Johnson-Shoemaker-2003" ],
        "selected" : false,
        "data_type" : "Co-localization"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1106",
        "source" : "671",
        "target" : "673",
        "shared_name" : "H__sapiens__1_-716648|H__sapiens__1_-720891|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 3.9491922039319716E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.007310227025300264, 0.018425989896059036, 0.01160210371017456 ],
        "name" : "H__sapiens__1_-716648|H__sapiens__1_-720891|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1106,
        "networks" : [ "Noble-Diehl-2008", "Mallon-McKay-2013", "Roth-Zlotnik-2006" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1105",
        "source" : "671",
        "target" : "672",
        "shared_name" : "H__sapiens__1_-716648|H__sapiens__1_-702208|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 1.0522995380325647E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.006929522380232811 ],
        "name" : "H__sapiens__1_-716648|H__sapiens__1_-702208|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1105,
        "networks" : [ "Noble-Diehl-2008" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1104",
        "source" : "670",
        "target" : "657",
        "shared_name" : "H__sapiens__1_-709197|H__sapiens__1_-704636|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 5.920518707842013E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.006476974114775658 ],
        "name" : "H__sapiens__1_-709197|H__sapiens__1_-704636|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1104,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1103",
        "source" : "670",
        "target" : "688",
        "shared_name" : "H__sapiens__1_-709197|H__sapiens__1_-707211|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.03712193709541126,
        "shared_interaction" : "",
        "raw_weights" : [ 0.40610939264297485 ],
        "name" : "H__sapiens__1_-709197|H__sapiens__1_-707211|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1103,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1102",
        "source" : "670",
        "target" : "643",
        "shared_name" : "H__sapiens__1_-709197|H__sapiens__1_-716738|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 4.146095848690052E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 3.9845428545959294E-4 ],
        "name" : "H__sapiens__1_-709197|H__sapiens__1_-716738|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1102,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1101",
        "source" : "670",
        "target" : "682",
        "shared_name" : "H__sapiens__1_-709197|H__sapiens__1_-702553|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 1.4632469782884521E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0014062314294278622 ],
        "name" : "H__sapiens__1_-709197|H__sapiens__1_-702553|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1101,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1100",
        "source" : "670",
        "target" : "642",
        "shared_name" : "H__sapiens__1_-709197|H__sapiens__1_-710126|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 3.4726395060728228E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 3.3373277983628213E-4 ],
        "name" : "H__sapiens__1_-709197|H__sapiens__1_-710126|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1100,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1099",
        "source" : "670",
        "target" : "641",
        "shared_name" : "H__sapiens__1_-709197|H__sapiens__1_-704846|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 7.61216497216369E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 7.315556285902858E-4 ],
        "name" : "H__sapiens__1_-709197|H__sapiens__1_-704846|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1099,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1098",
        "source" : "670",
        "target" : "630",
        "shared_name" : "H__sapiens__1_-709197|H__sapiens__1_-709018|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 5.105289033279357E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 4.906360991299152E-4 ],
        "name" : "H__sapiens__1_-709197|H__sapiens__1_-709018|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1098,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1097",
        "source" : "670",
        "target" : "638",
        "shared_name" : "H__sapiens__1_-709197|H__sapiens__1_-718245|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 3.334847111448152E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 3.2049044966697693E-4 ],
        "name" : "H__sapiens__1_-709197|H__sapiens__1_-718245|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1097,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1096",
        "source" : "670",
        "target" : "684",
        "shared_name" : "H__sapiens__1_-709197|H__sapiens__1_-713444|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 4.496651319920134E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 4.3214389006607234E-4 ],
        "name" : "H__sapiens__1_-709197|H__sapiens__1_-713444|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1096,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1095",
        "source" : "670",
        "target" : "689",
        "shared_name" : "H__sapiens__1_-709197|H__sapiens__1_-717747|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 3.4723545351092426E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 3.3370539313182235E-4 ],
        "name" : "H__sapiens__1_-709197|H__sapiens__1_-717747|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1095,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1094",
        "source" : "670",
        "target" : "654",
        "shared_name" : "H__sapiens__1_-709197|H__sapiens__1_-707210|Co-localization",
        "highlight" : 1,
        "normalized_max_weight" : 1.6698633157505982E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.007693442516028881, 0.009972215630114079 ],
        "name" : "H__sapiens__1_-709197|H__sapiens__1_-707210|Co-localization",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1094,
        "networks" : [ "Johnson-Shoemaker-2003", "Schadt-Shoemaker-2004" ],
        "selected" : false,
        "data_type" : "Co-localization"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1093",
        "source" : "670",
        "target" : "655",
        "shared_name" : "H__sapiens__1_-709197|H__sapiens__1_-707212|Co-localization",
        "highlight" : 1,
        "normalized_max_weight" : 1.2055104942115068E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.005554062779992819, 0.011148324236273766 ],
        "name" : "H__sapiens__1_-709197|H__sapiens__1_-707212|Co-localization",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1093,
        "networks" : [ "Johnson-Shoemaker-2003", "Schadt-Shoemaker-2004" ],
        "selected" : false,
        "data_type" : "Co-localization"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1092",
        "source" : "670",
        "target" : "654",
        "shared_name" : "H__sapiens__1_-709197|H__sapiens__1_-707210|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 3.656445694806483E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.013741103932261467, 0.01600373350083828 ],
        "name" : "H__sapiens__1_-709197|H__sapiens__1_-707210|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1092,
        "networks" : [ "Dobbin-Giordano-2005", "Roth-Zlotnik-2006" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1091",
        "source" : "670",
        "target" : "676",
        "shared_name" : "H__sapiens__1_-709197|H__sapiens__1_-718153|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 3.642502080542239E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.01204306073486805, 0.0121832937002182 ],
        "name" : "H__sapiens__1_-709197|H__sapiens__1_-718153|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1091,
        "networks" : [ "Dobbin-Giordano-2005", "Perou-Botstein-2000" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1090",
        "source" : "670",
        "target" : "655",
        "shared_name" : "H__sapiens__1_-709197|H__sapiens__1_-707212|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 3.2765724485030694E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.012313521467149258 ],
        "name" : "H__sapiens__1_-709197|H__sapiens__1_-707212|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1090,
        "networks" : [ "Dobbin-Giordano-2005" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1089",
        "source" : "670",
        "target" : "631",
        "shared_name" : "H__sapiens__1_-709197|H__sapiens__1_-717093|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 9.895426662130432E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.013530929572880268, 0.01612485572695732 ],
        "name" : "H__sapiens__1_-709197|H__sapiens__1_-717093|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1089,
        "networks" : [ "Burington-Shaughnessy-2008", "Bild-Nevins-2006 B" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1088",
        "source" : "669",
        "target" : "677",
        "shared_name" : "H__sapiens__1_-702228|H__sapiens__1_-707068|Shared protein domains",
        "highlight" : 1,
        "normalized_max_weight" : 2.9550044747035077E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.016663307324051857 ],
        "name" : "H__sapiens__1_-702228|H__sapiens__1_-707068|Shared protein domains",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1088,
        "networks" : [ "PFAM" ],
        "selected" : false,
        "data_type" : "Shared protein domains"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1087",
        "source" : "669",
        "target" : "645",
        "shared_name" : "H__sapiens__1_-702228|H__sapiens__1_-707850|Shared protein domains",
        "highlight" : 1,
        "normalized_max_weight" : 0.0023163233452642056,
        "shared_interaction" : "",
        "raw_weights" : [ 0.06265527009963989, 0.09564002603292465 ],
        "name" : "H__sapiens__1_-702228|H__sapiens__1_-707850|Shared protein domains",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1087,
        "networks" : [ "INTERPRO", "PFAM" ],
        "selected" : false,
        "data_type" : "Shared protein domains"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1086",
        "source" : "669",
        "target" : "667",
        "shared_name" : "H__sapiens__1_-702228|H__sapiens__1_-720857|Shared protein domains",
        "highlight" : 1,
        "normalized_max_weight" : 0.0019715059298263346,
        "shared_interaction" : "",
        "raw_weights" : [ 0.053328149020671844, 0.057045914232730865 ],
        "name" : "H__sapiens__1_-702228|H__sapiens__1_-720857|Shared protein domains",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1086,
        "networks" : [ "INTERPRO", "PFAM" ],
        "selected" : false,
        "data_type" : "Shared protein domains"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1085",
        "source" : "669",
        "target" : "666",
        "shared_name" : "H__sapiens__1_-702228|H__sapiens__1_-706362|Shared protein domains",
        "highlight" : 1,
        "normalized_max_weight" : 5.341614932430054E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.014448774047195911 ],
        "name" : "H__sapiens__1_-702228|H__sapiens__1_-706362|Shared protein domains",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1085,
        "networks" : [ "INTERPRO" ],
        "selected" : false,
        "data_type" : "Shared protein domains"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1084",
        "source" : "669",
        "target" : "666",
        "shared_name" : "H__sapiens__1_-702228|H__sapiens__1_-706362|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 4.1015722090986405E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.12224671989679337, 0.006649596616625786 ],
        "name" : "H__sapiens__1_-702228|H__sapiens__1_-706362|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1084,
        "networks" : [ "IREF-INTACT", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1083",
        "source" : "669",
        "target" : "643",
        "shared_name" : "H__sapiens__1_-702228|H__sapiens__1_-716738|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 2.3389778936652313E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.18639342486858368, 0.03816084563732147 ],
        "name" : "H__sapiens__1_-702228|H__sapiens__1_-716738|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1083,
        "networks" : [ "IREF-HPRD", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1082",
        "source" : "669",
        "target" : "678",
        "shared_name" : "H__sapiens__1_-702228|H__sapiens__1_-705245|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 2.677680676417756E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.029952209442853928, 0.004368684254586697 ],
        "name" : "H__sapiens__1_-702228|H__sapiens__1_-705245|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1082,
        "networks" : [ "IREF-HPRD", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1081",
        "source" : "669",
        "target" : "638",
        "shared_name" : "H__sapiens__1_-702228|H__sapiens__1_-718245|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 2.586476689911752E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.2041558176279068, 0.04219883307814598 ],
        "name" : "H__sapiens__1_-702228|H__sapiens__1_-718245|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1081,
        "networks" : [ "IREF-HPRD", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1080",
        "source" : "669",
        "target" : "682",
        "shared_name" : "H__sapiens__1_-702228|H__sapiens__1_-702553|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 5.813739108796137E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.005095256958156824, 0.004599783569574356 ],
        "name" : "H__sapiens__1_-702228|H__sapiens__1_-702553|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1080,
        "networks" : [ "BIOGRID-SMALL-SCALE-STUDIES", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1079",
        "source" : "669",
        "target" : "645",
        "shared_name" : "H__sapiens__1_-702228|H__sapiens__1_-707850|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.0010158696504024643,
        "shared_interaction" : "",
        "raw_weights" : [ 0.011113488115370274 ],
        "name" : "H__sapiens__1_-702228|H__sapiens__1_-707850|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1079,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1078",
        "source" : "669",
        "target" : "643",
        "shared_name" : "H__sapiens__1_-702228|H__sapiens__1_-716738|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.004104340459240445,
        "shared_interaction" : "",
        "raw_weights" : [ 0.04490097612142563 ],
        "name" : "H__sapiens__1_-702228|H__sapiens__1_-716738|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1078,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1077",
        "source" : "669",
        "target" : "677",
        "shared_name" : "H__sapiens__1_-702228|H__sapiens__1_-707068|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.006204673380908779,
        "shared_interaction" : "",
        "raw_weights" : [ 0.067878358066082 ],
        "name" : "H__sapiens__1_-702228|H__sapiens__1_-707068|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1077,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1076",
        "source" : "669",
        "target" : "628",
        "shared_name" : "H__sapiens__1_-702228|H__sapiens__1_-701067|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 5.700241823985682E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.006235993932932615 ],
        "name" : "H__sapiens__1_-702228|H__sapiens__1_-701067|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1076,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1075",
        "source" : "669",
        "target" : "667",
        "shared_name" : "H__sapiens__1_-702228|H__sapiens__1_-720857|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.0010419915864566989,
        "shared_interaction" : "",
        "raw_weights" : [ 0.011399258859455585 ],
        "name" : "H__sapiens__1_-702228|H__sapiens__1_-720857|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1075,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1074",
        "source" : "669",
        "target" : "678",
        "shared_name" : "H__sapiens__1_-702228|H__sapiens__1_-705245|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.014307191224594309,
        "shared_interaction" : "",
        "raw_weights" : [ 0.1527516096830368, 0.011652595363557339 ],
        "name" : "H__sapiens__1_-702228|H__sapiens__1_-705245|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1074,
        "networks" : [ "CELL_MAP", "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1073",
        "source" : "669",
        "target" : "638",
        "shared_name" : "H__sapiens__1_-702228|H__sapiens__1_-718245|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.021477121284735074,
        "shared_interaction" : "",
        "raw_weights" : [ 0.2293018102645874, 0.04490097612142563 ],
        "name" : "H__sapiens__1_-702228|H__sapiens__1_-718245|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1073,
        "networks" : [ "CELL_MAP", "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1072",
        "source" : "669",
        "target" : "640",
        "shared_name" : "H__sapiens__1_-702228|H__sapiens__1_-711862|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 2.101675453215267E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0020197834819555283 ],
        "name" : "H__sapiens__1_-702228|H__sapiens__1_-711862|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1072,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1071",
        "source" : "669",
        "target" : "651",
        "shared_name" : "H__sapiens__1_-702228|H__sapiens__1_-718835|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 2.4348045109067047E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0023399321362376213 ],
        "name" : "H__sapiens__1_-702228|H__sapiens__1_-718835|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1071,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1070",
        "source" : "669",
        "target" : "667",
        "shared_name" : "H__sapiens__1_-702228|H__sapiens__1_-720857|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 7.00676045921305E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.010911407880485058 ],
        "name" : "H__sapiens__1_-702228|H__sapiens__1_-720857|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1070,
        "networks" : [ "Wang-Maris-2006" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1069",
        "source" : "669",
        "target" : "654",
        "shared_name" : "H__sapiens__1_-702228|H__sapiens__1_-707210|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 0.0010055356375220524,
        "shared_interaction" : "",
        "raw_weights" : [ 0.013749616220593452 ],
        "name" : "H__sapiens__1_-702228|H__sapiens__1_-707210|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1069,
        "networks" : [ "Burington-Shaughnessy-2008" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1068",
        "source" : "669",
        "target" : "655",
        "shared_name" : "H__sapiens__1_-702228|H__sapiens__1_-707212|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 9.972869768334926E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.013636824674904346 ],
        "name" : "H__sapiens__1_-702228|H__sapiens__1_-707212|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1068,
        "networks" : [ "Burington-Shaughnessy-2008" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1067",
        "source" : "668",
        "target" : "692",
        "shared_name" : "H__sapiens__1_-714838|H__sapiens__1_-711599|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 3.1034826687594635E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.002982555190101266 ],
        "name" : "H__sapiens__1_-714838|H__sapiens__1_-711599|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1067,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1066",
        "source" : "668",
        "target" : "672",
        "shared_name" : "H__sapiens__1_-714838|H__sapiens__1_-702208|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 3.6625781346745296E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0035198654513806105 ],
        "name" : "H__sapiens__1_-714838|H__sapiens__1_-702208|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1066,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1065",
        "source" : "667",
        "target" : "666",
        "shared_name" : "H__sapiens__1_-720857|H__sapiens__1_-706362|Shared protein domains",
        "highlight" : 1,
        "normalized_max_weight" : 5.327991867366498E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0144119244068861 ],
        "name" : "H__sapiens__1_-720857|H__sapiens__1_-706362|Shared protein domains",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1065,
        "networks" : [ "INTERPRO" ],
        "selected" : false,
        "data_type" : "Shared protein domains"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1064",
        "source" : "667",
        "target" : "638",
        "shared_name" : "H__sapiens__1_-720857|H__sapiens__1_-718245|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 8.397083813014762E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.2235555797815323, 0.1369999349117279 ],
        "name" : "H__sapiens__1_-720857|H__sapiens__1_-718245|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1064,
        "networks" : [ "IREF-HPRD", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1063",
        "source" : "667",
        "target" : "638",
        "shared_name" : "H__sapiens__1_-720857|H__sapiens__1_-718245|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.017512194541184033,
        "shared_interaction" : "",
        "raw_weights" : [ 0.18697002530097961, 0.04485596343874931 ],
        "name" : "H__sapiens__1_-720857|H__sapiens__1_-718245|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1063,
        "networks" : [ "CELL_MAP", "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1062",
        "source" : "667",
        "target" : "674",
        "shared_name" : "H__sapiens__1_-720857|H__sapiens__1_-714657|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 2.8940393422960284E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0108759431168437 ],
        "name" : "H__sapiens__1_-720857|H__sapiens__1_-714657|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1062,
        "networks" : [ "Dobbin-Giordano-2005" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1061",
        "source" : "667",
        "target" : "679",
        "shared_name" : "H__sapiens__1_-720857|H__sapiens__1_-701028|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 3.297917248046479E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.012393736280500889 ],
        "name" : "H__sapiens__1_-720857|H__sapiens__1_-701028|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1061,
        "networks" : [ "Dobbin-Giordano-2005" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1060",
        "source" : "667",
        "target" : "668",
        "shared_name" : "H__sapiens__1_-720857|H__sapiens__1_-714838|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 5.324615737534911E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.007280838210135698 ],
        "name" : "H__sapiens__1_-720857|H__sapiens__1_-714838|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1060,
        "networks" : [ "Burington-Shaughnessy-2008" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1059",
        "source" : "666",
        "target" : "688",
        "shared_name" : "H__sapiens__1_-706362|H__sapiens__1_-707211|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 2.457994908870442E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0023622189182788134 ],
        "name" : "H__sapiens__1_-706362|H__sapiens__1_-707211|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1059,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1058",
        "source" : "666",
        "target" : "665",
        "shared_name" : "H__sapiens__1_-706362|H__sapiens__1_-710332|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 1.4945365816639096E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.004998865537345409 ],
        "name" : "H__sapiens__1_-706362|H__sapiens__1_-710332|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1058,
        "networks" : [ "Perou-Botstein-2000" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1057",
        "source" : "666",
        "target" : "652",
        "shared_name" : "H__sapiens__1_-706362|H__sapiens__1_-721077|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 1.9613447369836514E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.00915116723626852 ],
        "name" : "H__sapiens__1_-706362|H__sapiens__1_-721077|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1057,
        "networks" : [ "Mallon-McKay-2013" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1056",
        "source" : "666",
        "target" : "655",
        "shared_name" : "H__sapiens__1_-706362|H__sapiens__1_-707212|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 6.174109822984864E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.008442429825663567 ],
        "name" : "H__sapiens__1_-706362|H__sapiens__1_-707212|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1056,
        "networks" : [ "Burington-Shaughnessy-2008" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1055",
        "source" : "665",
        "target" : "684",
        "shared_name" : "H__sapiens__1_-710332|H__sapiens__1_-713444|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.0024336047755365874,
        "shared_interaction" : "",
        "raw_weights" : [ 0.026623334735631943 ],
        "name" : "H__sapiens__1_-710332|H__sapiens__1_-713444|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1055,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1054",
        "source" : "665",
        "target" : "691",
        "shared_name" : "H__sapiens__1_-710332|H__sapiens__1_-711762|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.0036544381160762175,
        "shared_interaction" : "",
        "raw_weights" : [ 0.03997910022735596 ],
        "name" : "H__sapiens__1_-710332|H__sapiens__1_-711762|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1054,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1053",
        "source" : "665",
        "target" : "688",
        "shared_name" : "H__sapiens__1_-710332|H__sapiens__1_-707211|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 3.378718709271654E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0032470666337758303 ],
        "name" : "H__sapiens__1_-710332|H__sapiens__1_-707211|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1053,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1052",
        "source" : "665",
        "target" : "629",
        "shared_name" : "H__sapiens__1_-710332|H__sapiens__1_-707031|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 4.721459327166693E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.022029204294085503 ],
        "name" : "H__sapiens__1_-710332|H__sapiens__1_-707031|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1052,
        "networks" : [ "Mallon-McKay-2013" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1051",
        "source" : "665",
        "target" : "655",
        "shared_name" : "H__sapiens__1_-710332|H__sapiens__1_-707212|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 8.793380557125339E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.01202400028705597 ],
        "name" : "H__sapiens__1_-710332|H__sapiens__1_-707212|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1051,
        "networks" : [ "Burington-Shaughnessy-2008" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1050",
        "source" : "664",
        "target" : "683",
        "shared_name" : "H__sapiens__1_-716588|H__sapiens__1_-701859|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 4.203655216919129E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.014060216024518013 ],
        "name" : "H__sapiens__1_-716588|H__sapiens__1_-701859|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1050,
        "networks" : [ "Perou-Botstein-2000" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1049",
        "source" : "663",
        "target" : "680",
        "shared_name" : "H__sapiens__1_-712333|H__sapiens__1_-710028|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 3.605354034946026E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.008733098395168781 ],
        "name" : "H__sapiens__1_-712333|H__sapiens__1_-710028|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1049,
        "networks" : [ "Boldrick-Relman-2002" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1048",
        "source" : "663",
        "target" : "671",
        "shared_name" : "H__sapiens__1_-712333|H__sapiens__1_-716648|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 8.916985680948096E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.01388614159077406 ],
        "name" : "H__sapiens__1_-712333|H__sapiens__1_-716648|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1048,
        "networks" : [ "Wang-Maris-2006" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1047",
        "source" : "663",
        "target" : "664",
        "shared_name" : "H__sapiens__1_-712333|H__sapiens__1_-716588|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 9.186830822143057E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.012562001124024391 ],
        "name" : "H__sapiens__1_-712333|H__sapiens__1_-716588|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1047,
        "networks" : [ "Burington-Shaughnessy-2008" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1046",
        "source" : "662",
        "target" : "644",
        "shared_name" : "H__sapiens__1_-709795|H__sapiens__1_-705635|Shared protein domains",
        "highlight" : 1,
        "normalized_max_weight" : 0.025587671469510905,
        "shared_interaction" : "",
        "raw_weights" : [ 0.6921324133872986, 0.6520615816116333 ],
        "name" : "H__sapiens__1_-709795|H__sapiens__1_-705635|Shared protein domains",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1046,
        "networks" : [ "INTERPRO", "PFAM" ],
        "selected" : false,
        "data_type" : "Shared protein domains"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1045",
        "source" : "662",
        "target" : "694",
        "shared_name" : "H__sapiens__1_-709795|H__sapiens__1_-713636|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 1.1981613551714698E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.001151474891230464 ],
        "name" : "H__sapiens__1_-709795|H__sapiens__1_-713636|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1045,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1044",
        "source" : "662",
        "target" : "645",
        "shared_name" : "H__sapiens__1_-709795|H__sapiens__1_-707850|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 1.2929736477458447E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0012425928143784404 ],
        "name" : "H__sapiens__1_-709795|H__sapiens__1_-707850|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1044,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1043",
        "source" : "662",
        "target" : "686",
        "shared_name" : "H__sapiens__1_-709795|H__sapiens__1_-712776|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 9.202699667285009E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 8.844115654937923E-4 ],
        "name" : "H__sapiens__1_-709795|H__sapiens__1_-712776|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1043,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1042",
        "source" : "662",
        "target" : "672",
        "shared_name" : "H__sapiens__1_-709795|H__sapiens__1_-702208|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 1.7158950270438693E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0016490350244566798 ],
        "name" : "H__sapiens__1_-709795|H__sapiens__1_-702208|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1042,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1041",
        "source" : "662",
        "target" : "655",
        "shared_name" : "H__sapiens__1_-709795|H__sapiens__1_-707212|Co-localization",
        "highlight" : 1,
        "normalized_max_weight" : 2.1247160880426313E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.009789053350687027 ],
        "name" : "H__sapiens__1_-709795|H__sapiens__1_-707212|Co-localization",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1041,
        "networks" : [ "Johnson-Shoemaker-2003" ],
        "selected" : false,
        "data_type" : "Co-localization"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1040",
        "source" : "662",
        "target" : "682",
        "shared_name" : "H__sapiens__1_-709795|H__sapiens__1_-702553|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 8.413272449402231E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.008106518536806107 ],
        "name" : "H__sapiens__1_-709795|H__sapiens__1_-702553|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1040,
        "networks" : [ "Bild-Nevins-2006 B" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1039",
        "source" : "662",
        "target" : "670",
        "shared_name" : "H__sapiens__1_-709795|H__sapiens__1_-709197|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 4.4902347228235553E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.015018755570054054 ],
        "name" : "H__sapiens__1_-709795|H__sapiens__1_-709197|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1039,
        "networks" : [ "Perou-Botstein-2000" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1038",
        "source" : "662",
        "target" : "651",
        "shared_name" : "H__sapiens__1_-709795|H__sapiens__1_-718835|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 2.139920115023373E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.008041925728321075 ],
        "name" : "H__sapiens__1_-709795|H__sapiens__1_-718835|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1038,
        "networks" : [ "Dobbin-Giordano-2005" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1037",
        "source" : "662",
        "target" : "652",
        "shared_name" : "H__sapiens__1_-709795|H__sapiens__1_-721077|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 0.0010463285335373814,
        "shared_interaction" : "",
        "raw_weights" : [ 0.01430741511285305 ],
        "name" : "H__sapiens__1_-709795|H__sapiens__1_-721077|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1037,
        "networks" : [ "Burington-Shaughnessy-2008" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1036",
        "source" : "662",
        "target" : "648",
        "shared_name" : "H__sapiens__1_-709795|H__sapiens__1_-711569|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 2.255319564395874E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.008524279110133648 ],
        "name" : "H__sapiens__1_-709795|H__sapiens__1_-711569|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1036,
        "networks" : [ "Ramaswamy-Golub-2001" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1035",
        "source" : "661",
        "target" : "641",
        "shared_name" : "H__sapiens__1_-705407|H__sapiens__1_-704846|Shared protein domains",
        "highlight" : 1,
        "normalized_max_weight" : 2.279654085079178E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.012854997999966145 ],
        "name" : "H__sapiens__1_-705407|H__sapiens__1_-704846|Shared protein domains",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1035,
        "networks" : [ "PFAM" ],
        "selected" : false,
        "data_type" : "Shared protein domains"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1034",
        "source" : "661",
        "target" : "658",
        "shared_name" : "H__sapiens__1_-705407|H__sapiens__1_-706817|Shared protein domains",
        "highlight" : 1,
        "normalized_max_weight" : 0.0024250610688702416,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0655965656042099, 0.07721563428640366 ],
        "name" : "H__sapiens__1_-705407|H__sapiens__1_-706817|Shared protein domains",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1034,
        "networks" : [ "INTERPRO", "PFAM" ],
        "selected" : false,
        "data_type" : "Shared protein domains"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1033",
        "source" : "661",
        "target" : "657",
        "shared_name" : "H__sapiens__1_-705407|H__sapiens__1_-704636|Shared protein domains",
        "highlight" : 1,
        "normalized_max_weight" : 2.0092008513534189E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.005434777587652206 ],
        "name" : "H__sapiens__1_-705407|H__sapiens__1_-704636|Shared protein domains",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1033,
        "networks" : [ "INTERPRO" ],
        "selected" : false,
        "data_type" : "Shared protein domains"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1032",
        "source" : "661",
        "target" : "640",
        "shared_name" : "H__sapiens__1_-705407|H__sapiens__1_-711862|Shared protein domains",
        "highlight" : 1,
        "normalized_max_weight" : 2.685102784280745E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.007263055071234703 ],
        "name" : "H__sapiens__1_-705407|H__sapiens__1_-711862|Shared protein domains",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1032,
        "networks" : [ "INTERPRO" ],
        "selected" : false,
        "data_type" : "Shared protein domains"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1031",
        "source" : "661",
        "target" : "658",
        "shared_name" : "H__sapiens__1_-705407|H__sapiens__1_-706817|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 8.1698931066465E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.010556085966527462, 0.0243502389639616, 0.008118407800793648 ],
        "name" : "H__sapiens__1_-705407|H__sapiens__1_-706817|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1031,
        "networks" : [ "IREF-HPRD", "IREF-INTACT", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1030",
        "source" : "661",
        "target" : "636",
        "shared_name" : "H__sapiens__1_-705407|H__sapiens__1_-713265|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 4.567950366652213E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.011072712019085884, 0.007452693302184343 ],
        "name" : "H__sapiens__1_-705407|H__sapiens__1_-713265|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1030,
        "networks" : [ "IREF-HPRD", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1029",
        "source" : "661",
        "target" : "628",
        "shared_name" : "H__sapiens__1_-705407|H__sapiens__1_-701067|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 3.1655186977205685E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.002774312859401107, 0.0030416008085012436, 0.0020500109530985355 ],
        "name" : "H__sapiens__1_-705407|H__sapiens__1_-701067|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1029,
        "networks" : [ "BIOGRID-SMALL-SCALE-STUDIES", "IREF-HPRD", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1028",
        "source" : "661",
        "target" : "684",
        "shared_name" : "H__sapiens__1_-705407|H__sapiens__1_-713444|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 8.000043400110418E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.008751948364078999 ],
        "name" : "H__sapiens__1_-705407|H__sapiens__1_-713444|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1028,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1027",
        "source" : "661",
        "target" : "691",
        "shared_name" : "H__sapiens__1_-705407|H__sapiens__1_-711762|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.001201331531440205,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0131424181163311 ],
        "name" : "H__sapiens__1_-705407|H__sapiens__1_-711762|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1027,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1026",
        "source" : "661",
        "target" : "628",
        "shared_name" : "H__sapiens__1_-705407|H__sapiens__1_-701067|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.05512392088967157,
        "shared_interaction" : "",
        "raw_weights" : [ 0.3482360243797302, 0.0034145975951105356 ],
        "name" : "H__sapiens__1_-705407|H__sapiens__1_-701067|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1026,
        "networks" : [ "IMID", "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1025",
        "source" : "661",
        "target" : "658",
        "shared_name" : "H__sapiens__1_-705407|H__sapiens__1_-706817|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.018538836954073476,
        "shared_interaction" : "",
        "raw_weights" : [ 0.19793103635311127, 0.003678862703964114 ],
        "name" : "H__sapiens__1_-705407|H__sapiens__1_-706817|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1025,
        "networks" : [ "CELL_MAP", "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1024",
        "source" : "661",
        "target" : "636",
        "shared_name" : "H__sapiens__1_-705407|H__sapiens__1_-713265|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.016302176719329018,
        "shared_interaction" : "",
        "raw_weights" : [ 0.1740511953830719, 0.006626510992646217 ],
        "name" : "H__sapiens__1_-705407|H__sapiens__1_-713265|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1024,
        "networks" : [ "CELL_MAP", "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1023",
        "source" : "661",
        "target" : "670",
        "shared_name" : "H__sapiens__1_-705407|H__sapiens__1_-709197|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 1.1270495616140251E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0010831339750438929 ],
        "name" : "H__sapiens__1_-705407|H__sapiens__1_-709197|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1023,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1022",
        "source" : "661",
        "target" : "657",
        "shared_name" : "H__sapiens__1_-705407|H__sapiens__1_-704636|Co-localization",
        "highlight" : 1,
        "normalized_max_weight" : 3.0473252774435825E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.014039725065231323 ],
        "name" : "H__sapiens__1_-705407|H__sapiens__1_-704636|Co-localization",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1022,
        "networks" : [ "Johnson-Shoemaker-2003" ],
        "selected" : false,
        "data_type" : "Co-localization"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1021",
        "source" : "661",
        "target" : "629",
        "shared_name" : "H__sapiens__1_-705407|H__sapiens__1_-707031|Co-localization",
        "highlight" : 1,
        "normalized_max_weight" : 2.960145120640993E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.013638066127896309 ],
        "name" : "H__sapiens__1_-705407|H__sapiens__1_-707031|Co-localization",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1021,
        "networks" : [ "Johnson-Shoemaker-2003" ],
        "selected" : false,
        "data_type" : "Co-localization"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1020",
        "source" : "661",
        "target" : "646",
        "shared_name" : "H__sapiens__1_-705407|H__sapiens__1_-701435|Co-localization",
        "highlight" : 1,
        "normalized_max_weight" : 2.2014605732020265E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.010142632760107517 ],
        "name" : "H__sapiens__1_-705407|H__sapiens__1_-701435|Co-localization",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1020,
        "networks" : [ "Johnson-Shoemaker-2003" ],
        "selected" : false,
        "data_type" : "Co-localization"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1019",
        "source" : "661",
        "target" : "658",
        "shared_name" : "H__sapiens__1_-705407|H__sapiens__1_-706817|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 5.531501764997798E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.013398725539445877 ],
        "name" : "H__sapiens__1_-705407|H__sapiens__1_-706817|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1019,
        "networks" : [ "Boldrick-Relman-2002" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1018",
        "source" : "661",
        "target" : "646",
        "shared_name" : "H__sapiens__1_-705407|H__sapiens__1_-701435|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 2.5114928905705487E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.013431352563202381 ],
        "name" : "H__sapiens__1_-705407|H__sapiens__1_-701435|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1018,
        "networks" : [ "Roth-Zlotnik-2006" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1017",
        "source" : "661",
        "target" : "655",
        "shared_name" : "H__sapiens__1_-705407|H__sapiens__1_-707212|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 5.109159785288746E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.01931074634194374 ],
        "name" : "H__sapiens__1_-705407|H__sapiens__1_-707212|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1017,
        "networks" : [ "Ramaswamy-Golub-2001" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1016",
        "source" : "660",
        "target" : "663",
        "shared_name" : "H__sapiens__1_-709312|H__sapiens__1_-712333|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 6.724241940231707E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.016287850216031075 ],
        "name" : "H__sapiens__1_-709312|H__sapiens__1_-712333|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1016,
        "networks" : [ "Boldrick-Relman-2002" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1015",
        "source" : "659",
        "target" : "629",
        "shared_name" : "H__sapiens__1_-713803|H__sapiens__1_-707031|Shared protein domains",
        "highlight" : 1,
        "normalized_max_weight" : 4.0322564841701144E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.01090703159570694 ],
        "name" : "H__sapiens__1_-713803|H__sapiens__1_-707031|Shared protein domains",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1015,
        "networks" : [ "INTERPRO" ],
        "selected" : false,
        "data_type" : "Shared protein domains"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1014",
        "source" : "659",
        "target" : "645",
        "shared_name" : "H__sapiens__1_-713803|H__sapiens__1_-707850|Predicted",
        "highlight" : 1,
        "normalized_max_weight" : 1.0965644869845451E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.02163379080593586 ],
        "name" : "H__sapiens__1_-713803|H__sapiens__1_-707850|Predicted",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1014,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Predicted"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1013",
        "source" : "659",
        "target" : "656",
        "shared_name" : "H__sapiens__1_-713803|H__sapiens__1_-704308|Predicted",
        "highlight" : 1,
        "normalized_max_weight" : 3.115249064176189E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.006145981140434742 ],
        "name" : "H__sapiens__1_-713803|H__sapiens__1_-704308|Predicted",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1013,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Predicted"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1012",
        "source" : "659",
        "target" : "645",
        "shared_name" : "H__sapiens__1_-713803|H__sapiens__1_-707850|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 5.98897201303679E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.010823644697666168, 0.017850037664175034 ],
        "name" : "H__sapiens__1_-713803|H__sapiens__1_-707850|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1012,
        "networks" : [ "IREF-HPRD", "IREF-INTACT" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1011",
        "source" : "659",
        "target" : "656",
        "shared_name" : "H__sapiens__1_-713803|H__sapiens__1_-704308|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 1.1747802163180929E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.01377082895487547, 0.023168712854385376, 0.019166750833392143 ],
        "name" : "H__sapiens__1_-713803|H__sapiens__1_-704308|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1011,
        "networks" : [ "IREF-HPRD", "IREF-INTACT", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1010",
        "source" : "659",
        "target" : "678",
        "shared_name" : "H__sapiens__1_-713803|H__sapiens__1_-705245|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 2.4483489488204468E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.005183208268135786, 0.003994525410234928 ],
        "name" : "H__sapiens__1_-713803|H__sapiens__1_-705245|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1010,
        "networks" : [ "IREF-HPRD", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1009",
        "source" : "659",
        "target" : "628",
        "shared_name" : "H__sapiens__1_-713803|H__sapiens__1_-701067|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 3.0898076583477246E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0027079584542661905, 0.002219141460955143 ],
        "name" : "H__sapiens__1_-713803|H__sapiens__1_-701067|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1009,
        "networks" : [ "BIOGRID-SMALL-SCALE-STUDIES", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1008",
        "source" : "659",
        "target" : "682",
        "shared_name" : "H__sapiens__1_-713803|H__sapiens__1_-702553|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 5.748712467407423E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.005038266535848379, 0.0022061022464185953, 0.004205832723528147 ],
        "name" : "H__sapiens__1_-713803|H__sapiens__1_-702553|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1008,
        "networks" : [ "BIOGRID-SMALL-SCALE-STUDIES", "IREF-INTACT", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1007",
        "source" : "659",
        "target" : "628",
        "shared_name" : "H__sapiens__1_-713803|H__sapiens__1_-701067|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 3.3509042225476335E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0036658477038145065 ],
        "name" : "H__sapiens__1_-713803|H__sapiens__1_-701067|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1007,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1006",
        "source" : "659",
        "target" : "642",
        "shared_name" : "H__sapiens__1_-713803|H__sapiens__1_-710126|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 6.043481496187413E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.00661149388179183 ],
        "name" : "H__sapiens__1_-713803|H__sapiens__1_-710126|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1006,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1005",
        "source" : "659",
        "target" : "693",
        "shared_name" : "H__sapiens__1_-713803|H__sapiens__1_-710026|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.0019437026112309507,
        "shared_interaction" : "",
        "raw_weights" : [ 0.021263865754008293 ],
        "name" : "H__sapiens__1_-713803|H__sapiens__1_-710026|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1005,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1004",
        "source" : "659",
        "target" : "678",
        "shared_name" : "H__sapiens__1_-713803|H__sapiens__1_-705245|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.039652859939355356,
        "shared_interaction" : "",
        "raw_weights" : [ 0.42335620522499084, 0.006850013043731451 ],
        "name" : "H__sapiens__1_-713803|H__sapiens__1_-705245|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1004,
        "networks" : [ "CELL_MAP", "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1003",
        "source" : "659",
        "target" : "642",
        "shared_name" : "H__sapiens__1_-713803|H__sapiens__1_-710126|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 2.549941986481349E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 2.4505832698196173E-4 ],
        "name" : "H__sapiens__1_-713803|H__sapiens__1_-710126|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1003,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1002",
        "source" : "659",
        "target" : "640",
        "shared_name" : "H__sapiens__1_-713803|H__sapiens__1_-711862|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 7.034979755410696E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 6.760861142538488E-4 ],
        "name" : "H__sapiens__1_-713803|H__sapiens__1_-711862|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1002,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1001",
        "source" : "659",
        "target" : "688",
        "shared_name" : "H__sapiens__1_-713803|H__sapiens__1_-707211|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 6.12304420514201E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 5.884459242224693E-4 ],
        "name" : "H__sapiens__1_-713803|H__sapiens__1_-707211|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1001,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1000",
        "source" : "659",
        "target" : "684",
        "shared_name" : "H__sapiens__1_-713803|H__sapiens__1_-713444|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 3.3018689148264977E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 3.1732112984173E-4 ],
        "name" : "H__sapiens__1_-713803|H__sapiens__1_-713444|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 1000,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "999",
        "source" : "659",
        "target" : "675",
        "shared_name" : "H__sapiens__1_-713803|H__sapiens__1_-702552|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 9.99740948929418E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 9.607859537936747E-4 ],
        "name" : "H__sapiens__1_-713803|H__sapiens__1_-702552|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 999,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "998",
        "source" : "659",
        "target" : "647",
        "shared_name" : "H__sapiens__1_-713803|H__sapiens__1_-704799|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 7.853992664381239E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.019024398177862167 ],
        "name" : "H__sapiens__1_-713803|H__sapiens__1_-704799|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 998,
        "networks" : [ "Boldrick-Relman-2002" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "997",
        "source" : "659",
        "target" : "642",
        "shared_name" : "H__sapiens__1_-713803|H__sapiens__1_-710126|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 6.75955161281921E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.010526437312364578, 0.02055448107421398 ],
        "name" : "H__sapiens__1_-713803|H__sapiens__1_-710126|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 997,
        "networks" : [ "Wang-Maris-2006", "Bild-Nevins-2006 B" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "996",
        "source" : "659",
        "target" : "640",
        "shared_name" : "H__sapiens__1_-713803|H__sapiens__1_-711862|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 8.486204564884836E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.01321529969573021 ],
        "name" : "H__sapiens__1_-713803|H__sapiens__1_-711862|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 996,
        "networks" : [ "Wang-Maris-2006" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "995",
        "source" : "659",
        "target" : "683",
        "shared_name" : "H__sapiens__1_-713803|H__sapiens__1_-701859|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 6.941357246387209E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.010809557512402534 ],
        "name" : "H__sapiens__1_-713803|H__sapiens__1_-701859|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 995,
        "networks" : [ "Wang-Maris-2006" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "994",
        "source" : "659",
        "target" : "660",
        "shared_name" : "H__sapiens__1_-713803|H__sapiens__1_-709312|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 5.233364958219209E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.019780196249485016 ],
        "name" : "H__sapiens__1_-713803|H__sapiens__1_-709312|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 994,
        "networks" : [ "Ramaswamy-Golub-2001" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "993",
        "source" : "658",
        "target" : "641",
        "shared_name" : "H__sapiens__1_-706817|H__sapiens__1_-704846|Shared protein domains",
        "highlight" : 1,
        "normalized_max_weight" : 2.2903394147139142E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.012915252707898617 ],
        "name" : "H__sapiens__1_-706817|H__sapiens__1_-704846|Shared protein domains",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 993,
        "networks" : [ "PFAM" ],
        "selected" : false,
        "data_type" : "Shared protein domains"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "992",
        "source" : "658",
        "target" : "657",
        "shared_name" : "H__sapiens__1_-706817|H__sapiens__1_-704636|Shared protein domains",
        "highlight" : 1,
        "normalized_max_weight" : 2.0089350488859822E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.005434058606624603 ],
        "name" : "H__sapiens__1_-706817|H__sapiens__1_-704636|Shared protein domains",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 992,
        "networks" : [ "INTERPRO" ],
        "selected" : false,
        "data_type" : "Shared protein domains"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "991",
        "source" : "658",
        "target" : "640",
        "shared_name" : "H__sapiens__1_-706817|H__sapiens__1_-711862|Shared protein domains",
        "highlight" : 1,
        "normalized_max_weight" : 2.684747290695734E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.007262093480676413 ],
        "name" : "H__sapiens__1_-706817|H__sapiens__1_-711862|Shared protein domains",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 991,
        "networks" : [ "INTERPRO" ],
        "selected" : false,
        "data_type" : "Shared protein domains"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "990",
        "source" : "658",
        "target" : "628",
        "shared_name" : "H__sapiens__1_-706817|H__sapiens__1_-701067|Predicted",
        "highlight" : 1,
        "normalized_max_weight" : 3.428393058679875E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.006763773504644632 ],
        "name" : "H__sapiens__1_-706817|H__sapiens__1_-701067|Predicted",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 990,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Predicted"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "989",
        "source" : "658",
        "target" : "657",
        "shared_name" : "H__sapiens__1_-706817|H__sapiens__1_-704636|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 1.2160294358308808E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.010657482780516148, 0.0065562305971980095 ],
        "name" : "H__sapiens__1_-706817|H__sapiens__1_-704636|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 989,
        "networks" : [ "BIOGRID-SMALL-SCALE-STUDIES", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "988",
        "source" : "658",
        "target" : "684",
        "shared_name" : "H__sapiens__1_-706817|H__sapiens__1_-713444|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 8.602529096313004E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.009411060251295567 ],
        "name" : "H__sapiens__1_-706817|H__sapiens__1_-713444|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 988,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "987",
        "source" : "658",
        "target" : "691",
        "shared_name" : "H__sapiens__1_-706817|H__sapiens__1_-711762|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.001291804159400646,
        "shared_interaction" : "",
        "raw_weights" : [ 0.014132177457213402 ],
        "name" : "H__sapiens__1_-706817|H__sapiens__1_-711762|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 987,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "986",
        "source" : "658",
        "target" : "628",
        "shared_name" : "H__sapiens__1_-706817|H__sapiens__1_-701067|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.05512392088967157,
        "shared_interaction" : "",
        "raw_weights" : [ 0.3482360243797302 ],
        "name" : "H__sapiens__1_-706817|H__sapiens__1_-701067|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 986,
        "networks" : [ "IMID" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "985",
        "source" : "658",
        "target" : "674",
        "shared_name" : "H__sapiens__1_-706817|H__sapiens__1_-714657|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 1.924402827093748E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.018542377278208733 ],
        "name" : "H__sapiens__1_-706817|H__sapiens__1_-714657|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 985,
        "networks" : [ "Bild-Nevins-2006 B" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "984",
        "source" : "658",
        "target" : "635",
        "shared_name" : "H__sapiens__1_-706817|H__sapiens__1_-701014|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 9.176351279249806E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.022227492183446884 ],
        "name" : "H__sapiens__1_-706817|H__sapiens__1_-701014|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 984,
        "networks" : [ "Boldrick-Relman-2002" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "983",
        "source" : "658",
        "target" : "657",
        "shared_name" : "H__sapiens__1_-706817|H__sapiens__1_-704636|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 0.0011171492635142757,
        "shared_interaction" : "",
        "raw_weights" : [ 0.017397014424204826 ],
        "name" : "H__sapiens__1_-706817|H__sapiens__1_-704636|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 983,
        "networks" : [ "Wang-Maris-2006" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "982",
        "source" : "658",
        "target" : "637",
        "shared_name" : "H__sapiens__1_-706817|H__sapiens__1_-701844|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 2.368504188937662E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.01105087623000145 ],
        "name" : "H__sapiens__1_-706817|H__sapiens__1_-701844|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 982,
        "networks" : [ "Mallon-McKay-2013" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "981",
        "source" : "658",
        "target" : "647",
        "shared_name" : "H__sapiens__1_-706817|H__sapiens__1_-704799|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 4.2501258331621456E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.016063913702964783 ],
        "name" : "H__sapiens__1_-706817|H__sapiens__1_-704799|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 981,
        "networks" : [ "Ramaswamy-Golub-2001" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "980",
        "source" : "657",
        "target" : "640",
        "shared_name" : "H__sapiens__1_-704636|H__sapiens__1_-711862|Shared protein domains",
        "highlight" : 1,
        "normalized_max_weight" : 0.0018335182284759841,
        "shared_interaction" : "",
        "raw_weights" : [ 0.049595657736063004, 0.04912766069173813 ],
        "name" : "H__sapiens__1_-704636|H__sapiens__1_-711862|Shared protein domains",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 980,
        "networks" : [ "INTERPRO", "PFAM" ],
        "selected" : false,
        "data_type" : "Shared protein domains"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "979",
        "source" : "657",
        "target" : "628",
        "shared_name" : "H__sapiens__1_-704636|H__sapiens__1_-701067|Predicted",
        "highlight" : 1,
        "normalized_max_weight" : 2.3845292327266553E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.004704365972429514 ],
        "name" : "H__sapiens__1_-704636|H__sapiens__1_-701067|Predicted",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 979,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Predicted"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "978",
        "source" : "657",
        "target" : "682",
        "shared_name" : "H__sapiens__1_-704636|H__sapiens__1_-702553|Predicted",
        "highlight" : 1,
        "normalized_max_weight" : 1.0567588443148247E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.020848477259278297 ],
        "name" : "H__sapiens__1_-704636|H__sapiens__1_-702553|Predicted",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 978,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Predicted"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "977",
        "source" : "657",
        "target" : "640",
        "shared_name" : "H__sapiens__1_-704636|H__sapiens__1_-711862|Predicted",
        "highlight" : 1,
        "normalized_max_weight" : 1.1492874279478959E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.018106019124388695 ],
        "name" : "H__sapiens__1_-704636|H__sapiens__1_-711862|Predicted",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 977,
        "networks" : [ "I2D-INNATEDB-Mouse2Human" ],
        "selected" : false,
        "data_type" : "Predicted"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "976",
        "source" : "657",
        "target" : "684",
        "shared_name" : "H__sapiens__1_-704636|H__sapiens__1_-713444|Predicted",
        "highlight" : 1,
        "normalized_max_weight" : 3.087642673062103E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.04864311218261719 ],
        "name" : "H__sapiens__1_-704636|H__sapiens__1_-713444|Predicted",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 976,
        "networks" : [ "I2D-INNATEDB-Mouse2Human" ],
        "selected" : false,
        "data_type" : "Predicted"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "975",
        "source" : "657",
        "target" : "635",
        "shared_name" : "H__sapiens__1_-704636|H__sapiens__1_-701014|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 5.7603289317683537E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.017822200432419777, 0.00939808040857315 ],
        "name" : "H__sapiens__1_-704636|H__sapiens__1_-701014|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 975,
        "networks" : [ "IREF-HPRD", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "974",
        "source" : "657",
        "target" : "640",
        "shared_name" : "H__sapiens__1_-704636|H__sapiens__1_-711862|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 7.005866505716762E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.01888929307460785, 0.0027059889398515224, 0.011430197395384312 ],
        "name" : "H__sapiens__1_-704636|H__sapiens__1_-711862|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 974,
        "networks" : [ "IREF-HPRD", "IREF-INTACT", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "973",
        "source" : "657",
        "target" : "686",
        "shared_name" : "H__sapiens__1_-704636|H__sapiens__1_-712776|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 0.008769614005081397,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0028707815799862146, 0.1280638426542282, 0.012699360027909279, 0.0033547196071594954 ],
        "name" : "H__sapiens__1_-704636|H__sapiens__1_-712776|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 973,
        "networks" : [ "BIOGRID-SMALL-SCALE-STUDIES", "Coyaud-Raught-2015", "IREF-HPRD", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "972",
        "source" : "657",
        "target" : "682",
        "shared_name" : "H__sapiens__1_-704636|H__sapiens__1_-702553|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 4.2673880853804295E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0037400093860924244, 0.005866675637662411, 0.0031376644037663937 ],
        "name" : "H__sapiens__1_-704636|H__sapiens__1_-702553|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 972,
        "networks" : [ "BIOGRID-SMALL-SCALE-STUDIES", "IREF-HPRD", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "971",
        "source" : "657",
        "target" : "634",
        "shared_name" : "H__sapiens__1_-704636|H__sapiens__1_-701737|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 6.180086101361597E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.006760937627404928 ],
        "name" : "H__sapiens__1_-704636|H__sapiens__1_-701737|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 971,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "970",
        "source" : "657",
        "target" : "637",
        "shared_name" : "H__sapiens__1_-704636|H__sapiens__1_-701844|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 3.3472299686593363E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.003661828115582466 ],
        "name" : "H__sapiens__1_-704636|H__sapiens__1_-701844|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 970,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "969",
        "source" : "657",
        "target" : "640",
        "shared_name" : "H__sapiens__1_-704636|H__sapiens__1_-711862|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 5.440444151115592E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.005951778497546911 ],
        "name" : "H__sapiens__1_-704636|H__sapiens__1_-711862|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 969,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "968",
        "source" : "657",
        "target" : "665",
        "shared_name" : "H__sapiens__1_-704636|H__sapiens__1_-710332|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 5.485571242024944E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.006001146975904703 ],
        "name" : "H__sapiens__1_-704636|H__sapiens__1_-710332|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 968,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "967",
        "source" : "657",
        "target" : "663",
        "shared_name" : "H__sapiens__1_-704636|H__sapiens__1_-712333|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 4.470853158637007E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.004891057964414358 ],
        "name" : "H__sapiens__1_-704636|H__sapiens__1_-712333|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 967,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "966",
        "source" : "657",
        "target" : "691",
        "shared_name" : "H__sapiens__1_-704636|H__sapiens__1_-711762|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 6.927230220836197E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.007578304037451744 ],
        "name" : "H__sapiens__1_-704636|H__sapiens__1_-711762|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 966,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "965",
        "source" : "657",
        "target" : "686",
        "shared_name" : "H__sapiens__1_-704636|H__sapiens__1_-712776|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 6.909914568365033E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.16715164482593536, 0.003925853408873081 ],
        "name" : "H__sapiens__1_-704636|H__sapiens__1_-712776|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 965,
        "networks" : [ "REACTOME", "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "964",
        "source" : "657",
        "target" : "628",
        "shared_name" : "H__sapiens__1_-704636|H__sapiens__1_-701067|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.05512392088967157,
        "shared_interaction" : "",
        "raw_weights" : [ 0.3482360243797302 ],
        "name" : "H__sapiens__1_-704636|H__sapiens__1_-701067|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 964,
        "networks" : [ "IMID" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "963",
        "source" : "657",
        "target" : "635",
        "shared_name" : "H__sapiens__1_-704636|H__sapiens__1_-701014|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.04709535273585115,
        "shared_interaction" : "",
        "raw_weights" : [ 0.5028164386749268, 0.008204847574234009 ],
        "name" : "H__sapiens__1_-704636|H__sapiens__1_-701014|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 963,
        "networks" : [ "CELL_MAP", "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "962",
        "source" : "657",
        "target" : "642",
        "shared_name" : "H__sapiens__1_-704636|H__sapiens__1_-710126|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 5.201051390579495E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 4.998391959816217E-4 ],
        "name" : "H__sapiens__1_-704636|H__sapiens__1_-710126|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 962,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "961",
        "source" : "657",
        "target" : "683",
        "shared_name" : "H__sapiens__1_-704636|H__sapiens__1_-701859|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 1.5181108843453687E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0014589575584977865 ],
        "name" : "H__sapiens__1_-704636|H__sapiens__1_-701859|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 961,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "960",
        "source" : "657",
        "target" : "692",
        "shared_name" : "H__sapiens__1_-704636|H__sapiens__1_-711599|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 1.480387754618969E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0014227043138816953 ],
        "name" : "H__sapiens__1_-704636|H__sapiens__1_-711599|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 960,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "959",
        "source" : "657",
        "target" : "646",
        "shared_name" : "H__sapiens__1_-704636|H__sapiens__1_-701435|Co-localization",
        "highlight" : 1,
        "normalized_max_weight" : 3.6194527631914526E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.016675647348165512 ],
        "name" : "H__sapiens__1_-704636|H__sapiens__1_-701435|Co-localization",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 959,
        "networks" : [ "Johnson-Shoemaker-2003" ],
        "selected" : false,
        "data_type" : "Co-localization"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "958",
        "source" : "657",
        "target" : "645",
        "shared_name" : "H__sapiens__1_-704636|H__sapiens__1_-707850|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 2.676075045646975E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.014311530627310276 ],
        "name" : "H__sapiens__1_-704636|H__sapiens__1_-707850|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 958,
        "networks" : [ "Roth-Zlotnik-2006" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "957",
        "source" : "657",
        "target" : "674",
        "shared_name" : "H__sapiens__1_-704636|H__sapiens__1_-714657|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 1.9080225505862185E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.010204019956290722 ],
        "name" : "H__sapiens__1_-704636|H__sapiens__1_-714657|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 957,
        "networks" : [ "Roth-Zlotnik-2006" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "956",
        "source" : "657",
        "target" : "678",
        "shared_name" : "H__sapiens__1_-704636|H__sapiens__1_-705245|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 1.7946640153632503E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.008373474702239037 ],
        "name" : "H__sapiens__1_-704636|H__sapiens__1_-705245|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 956,
        "networks" : [ "Mallon-McKay-2013" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "955",
        "source" : "657",
        "target" : "641",
        "shared_name" : "H__sapiens__1_-704636|H__sapiens__1_-704846|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 0.0014652344124046596,
        "shared_interaction" : "",
        "raw_weights" : [ 0.020035501569509506, 0.017122115939855576 ],
        "name" : "H__sapiens__1_-704636|H__sapiens__1_-704846|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 955,
        "networks" : [ "Burington-Shaughnessy-2008", "Bild-Nevins-2006 B" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "954",
        "source" : "657",
        "target" : "640",
        "shared_name" : "H__sapiens__1_-704636|H__sapiens__1_-711862|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 3.474964265208424E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.008031283505260944, 0.007199086248874664, 0.007773605640977621 ],
        "name" : "H__sapiens__1_-704636|H__sapiens__1_-711862|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 954,
        "networks" : [ "Ramaswamy-Golub-2001", "Mallon-McKay-2013", "Perou-Botstein-1999" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "953",
        "source" : "656",
        "target" : "692",
        "shared_name" : "H__sapiens__1_-704308|H__sapiens__1_-711599|Shared protein domains",
        "highlight" : 1,
        "normalized_max_weight" : 1.3408062026885341E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0036268068943172693 ],
        "name" : "H__sapiens__1_-704308|H__sapiens__1_-711599|Shared protein domains",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 953,
        "networks" : [ "INTERPRO" ],
        "selected" : false,
        "data_type" : "Shared protein domains"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "952",
        "source" : "656",
        "target" : "695",
        "shared_name" : "H__sapiens__1_-704308|H__sapiens__1_-712410|Shared protein domains",
        "highlight" : 1,
        "normalized_max_weight" : 1.1457832827109137E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0030992806423455477 ],
        "name" : "H__sapiens__1_-704308|H__sapiens__1_-712410|Shared protein domains",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 952,
        "networks" : [ "INTERPRO" ],
        "selected" : false,
        "data_type" : "Shared protein domains"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "951",
        "source" : "656",
        "target" : "630",
        "shared_name" : "H__sapiens__1_-704308|H__sapiens__1_-709018|Predicted",
        "highlight" : 1,
        "normalized_max_weight" : 2.0684135898756036E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.04080710932612419 ],
        "name" : "H__sapiens__1_-704308|H__sapiens__1_-709018|Predicted",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 951,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Predicted"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "950",
        "source" : "656",
        "target" : "628",
        "shared_name" : "H__sapiens__1_-704308|H__sapiens__1_-701067|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 9.249264801224542E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.00810620840638876, 0.009702018462121487, 0.006631302647292614 ],
        "name" : "H__sapiens__1_-704308|H__sapiens__1_-701067|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 950,
        "networks" : [ "BIOGRID-SMALL-SCALE-STUDIES", "IREF-HPRD", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "949",
        "source" : "656",
        "target" : "686",
        "shared_name" : "H__sapiens__1_-704308|H__sapiens__1_-712776|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 1.320911766790708E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.011576688848435879, 0.01343740988522768 ],
        "name" : "H__sapiens__1_-704308|H__sapiens__1_-712776|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 949,
        "networks" : [ "BIOGRID-SMALL-SCALE-STUDIES", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "948",
        "source" : "656",
        "target" : "682",
        "shared_name" : "H__sapiens__1_-704308|H__sapiens__1_-702553|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 6.654996186073801E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.00728048337623477 ],
        "name" : "H__sapiens__1_-704308|H__sapiens__1_-702553|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 948,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "947",
        "source" : "656",
        "target" : "628",
        "shared_name" : "H__sapiens__1_-704308|H__sapiens__1_-701067|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.08164000996444919,
        "shared_interaction" : "",
        "raw_weights" : [ 0.8716346025466919, 0.005788726732134819 ],
        "name" : "H__sapiens__1_-704308|H__sapiens__1_-701067|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 947,
        "networks" : [ "CELL_MAP", "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "946",
        "source" : "656",
        "target" : "642",
        "shared_name" : "H__sapiens__1_-704308|H__sapiens__1_-710126|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 2.6984236692053657E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 2.5932793505489826E-4 ],
        "name" : "H__sapiens__1_-704308|H__sapiens__1_-710126|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 946,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "945",
        "source" : "656",
        "target" : "684",
        "shared_name" : "H__sapiens__1_-704308|H__sapiens__1_-713444|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 3.4941349779058855E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 3.357985697221011E-4 ],
        "name" : "H__sapiens__1_-704308|H__sapiens__1_-713444|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 945,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "944",
        "source" : "656",
        "target" : "639",
        "shared_name" : "H__sapiens__1_-704308|H__sapiens__1_-718375|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 7.5176109402032796E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 7.224686560221016E-4 ],
        "name" : "H__sapiens__1_-704308|H__sapiens__1_-718375|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 944,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "943",
        "source" : "656",
        "target" : "668",
        "shared_name" : "H__sapiens__1_-704308|H__sapiens__1_-714838|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 1.625966670414073E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0015626107342541218 ],
        "name" : "H__sapiens__1_-704308|H__sapiens__1_-714838|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 943,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "942",
        "source" : "656",
        "target" : "689",
        "shared_name" : "H__sapiens__1_-704308|H__sapiens__1_-717747|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 2.698202597150729E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 2.5930668925866485E-4 ],
        "name" : "H__sapiens__1_-704308|H__sapiens__1_-717747|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 942,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "941",
        "source" : "656",
        "target" : "642",
        "shared_name" : "H__sapiens__1_-704308|H__sapiens__1_-710126|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 5.575090579439415E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.021071793511509895 ],
        "name" : "H__sapiens__1_-704308|H__sapiens__1_-710126|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 941,
        "networks" : [ "Ramaswamy-Golub-2001" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "940",
        "source" : "654",
        "target" : "655",
        "shared_name" : "H__sapiens__1_-707210|H__sapiens__1_-707212|Predicted",
        "highlight" : 1,
        "normalized_max_weight" : 9.970913366582376E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.1967131495475769 ],
        "name" : "H__sapiens__1_-707210|H__sapiens__1_-707212|Predicted",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 940,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Predicted"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "939",
        "source" : "654",
        "target" : "685",
        "shared_name" : "H__sapiens__1_-707210|H__sapiens__1_-746649|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 5.124021447954289E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.15272065997123718, 0.05278971418738365 ],
        "name" : "H__sapiens__1_-707210|H__sapiens__1_-746649|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 939,
        "networks" : [ "IREF-INTACT", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "938",
        "source" : "654",
        "target" : "666",
        "shared_name" : "H__sapiens__1_-707210|H__sapiens__1_-706362|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 2.815884910680346E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.024678880348801613, 0.02069883421063423, 0.013335066847503185 ],
        "name" : "H__sapiens__1_-707210|H__sapiens__1_-706362|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 938,
        "networks" : [ "BIOGRID-SMALL-SCALE-STUDIES", "IREF-HPRD", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "937",
        "source" : "654",
        "target" : "666",
        "shared_name" : "H__sapiens__1_-707210|H__sapiens__1_-706362|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.005897218353596716,
        "shared_interaction" : "",
        "raw_weights" : [ 0.06451483815908432 ],
        "name" : "H__sapiens__1_-707210|H__sapiens__1_-706362|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 937,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "936",
        "source" : "654",
        "target" : "655",
        "shared_name" : "H__sapiens__1_-707210|H__sapiens__1_-707212|Co-localization",
        "highlight" : 1,
        "normalized_max_weight" : 1.5164465107543577E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.006986616179347038, 0.013451211154460907 ],
        "name" : "H__sapiens__1_-707210|H__sapiens__1_-707212|Co-localization",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 936,
        "networks" : [ "Johnson-Shoemaker-2003", "Schadt-Shoemaker-2004" ],
        "selected" : false,
        "data_type" : "Co-localization"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "935",
        "source" : "654",
        "target" : "676",
        "shared_name" : "H__sapiens__1_-707210|H__sapiens__1_-718153|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 2.960329405217957E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.011125064454972744 ],
        "name" : "H__sapiens__1_-707210|H__sapiens__1_-718153|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 935,
        "networks" : [ "Dobbin-Giordano-2005" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "934",
        "source" : "654",
        "target" : "630",
        "shared_name" : "H__sapiens__1_-707210|H__sapiens__1_-709018|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 0.001014719985075663,
        "shared_interaction" : "",
        "raw_weights" : [ 0.013875202275812626 ],
        "name" : "H__sapiens__1_-707210|H__sapiens__1_-709018|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 934,
        "networks" : [ "Burington-Shaughnessy-2008" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "933",
        "source" : "654",
        "target" : "666",
        "shared_name" : "H__sapiens__1_-707210|H__sapiens__1_-706362|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 6.875849090202938E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.00940198265016079 ],
        "name" : "H__sapiens__1_-707210|H__sapiens__1_-706362|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 933,
        "networks" : [ "Burington-Shaughnessy-2008" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "932",
        "source" : "654",
        "target" : "665",
        "shared_name" : "H__sapiens__1_-707210|H__sapiens__1_-710332|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 9.062025805018559E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.012391343712806702, 0.012462126091122627 ],
        "name" : "H__sapiens__1_-707210|H__sapiens__1_-710332|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 932,
        "networks" : [ "Burington-Shaughnessy-2008", "Wang-Maris-2006" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "931",
        "source" : "654",
        "target" : "655",
        "shared_name" : "H__sapiens__1_-707210|H__sapiens__1_-707212|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 0.0010046242322440048,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0345146618783474, 0.012951928190886974, 0.018567988649010658, 0.015010948292911053, 0.011411738581955433, 0.015644697472453117, 0.014536840841174126, 0.023804061114788055 ],
        "name" : "H__sapiens__1_-707210|H__sapiens__1_-707212|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 931,
        "networks" : [ "Ramaswamy-Golub-2001", "Burington-Shaughnessy-2008", "Noble-Diehl-2008", "Mallon-McKay-2013", "Dobbin-Giordano-2005", "Wang-Maris-2006", "Roth-Zlotnik-2006", "Bild-Nevins-2006 B" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "930",
        "source" : "652",
        "target" : "683",
        "shared_name" : "H__sapiens__1_-721077|H__sapiens__1_-701859|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 8.156299925139566E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0127015495672822 ],
        "name" : "H__sapiens__1_-721077|H__sapiens__1_-701859|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 930,
        "networks" : [ "Wang-Maris-2006" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "929",
        "source" : "652",
        "target" : "633",
        "shared_name" : "H__sapiens__1_-721077|H__sapiens__1_-706527|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 1.8421789337014628E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.012130975723266602 ],
        "name" : "H__sapiens__1_-721077|H__sapiens__1_-706527|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 929,
        "networks" : [ "Noble-Diehl-2008" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "928",
        "source" : "652",
        "target" : "653",
        "shared_name" : "H__sapiens__1_-721077|H__sapiens__1_-709006|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 2.3524085413657143E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.008891239762306213 ],
        "name" : "H__sapiens__1_-721077|H__sapiens__1_-709006|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 928,
        "networks" : [ "Ramaswamy-Golub-2001" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "927",
        "source" : "651",
        "target" : "685",
        "shared_name" : "H__sapiens__1_-718835|H__sapiens__1_-746649|Shared protein domains",
        "highlight" : 1,
        "normalized_max_weight" : 1.5410774548072564E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.008690155111253262 ],
        "name" : "H__sapiens__1_-718835|H__sapiens__1_-746649|Shared protein domains",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 927,
        "networks" : [ "PFAM" ],
        "selected" : false,
        "data_type" : "Shared protein domains"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "926",
        "source" : "651",
        "target" : "675",
        "shared_name" : "H__sapiens__1_-718835|H__sapiens__1_-702552|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 2.2675231029271E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.002179168863222003 ],
        "name" : "H__sapiens__1_-718835|H__sapiens__1_-702552|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 926,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "925",
        "source" : "651",
        "target" : "689",
        "shared_name" : "H__sapiens__1_-718835|H__sapiens__1_-717747|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 5.783076571136886E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 5.557738477364182E-4 ],
        "name" : "H__sapiens__1_-718835|H__sapiens__1_-717747|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 925,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "924",
        "source" : "651",
        "target" : "685",
        "shared_name" : "H__sapiens__1_-718835|H__sapiens__1_-746649|Co-localization",
        "highlight" : 1,
        "normalized_max_weight" : 2.0321114034231867E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.009362402372062206 ],
        "name" : "H__sapiens__1_-718835|H__sapiens__1_-746649|Co-localization",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 924,
        "networks" : [ "Johnson-Shoemaker-2003" ],
        "selected" : false,
        "data_type" : "Co-localization"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "923",
        "source" : "651",
        "target" : "655",
        "shared_name" : "H__sapiens__1_-718835|H__sapiens__1_-707212|Co-localization",
        "highlight" : 1,
        "normalized_max_weight" : 1.8330915724571682E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.008445472456514835 ],
        "name" : "H__sapiens__1_-718835|H__sapiens__1_-707212|Co-localization",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 923,
        "networks" : [ "Johnson-Shoemaker-2003" ],
        "selected" : false,
        "data_type" : "Co-localization"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "922",
        "source" : "651",
        "target" : "655",
        "shared_name" : "H__sapiens__1_-718835|H__sapiens__1_-707212|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 1.5822178560185782E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.008461630903184414 ],
        "name" : "H__sapiens__1_-718835|H__sapiens__1_-707212|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 922,
        "networks" : [ "Roth-Zlotnik-2006" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "921",
        "source" : "650",
        "target" : "689",
        "shared_name" : "H__sapiens__1_-713649|H__sapiens__1_-717747|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 7.003603876927512E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 6.730707827955484E-4 ],
        "name" : "H__sapiens__1_-713649|H__sapiens__1_-717747|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 921,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "920",
        "source" : "650",
        "target" : "651",
        "shared_name" : "H__sapiens__1_-713649|H__sapiens__1_-718835|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 3.430059693063065E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.01296436507254839 ],
        "name" : "H__sapiens__1_-713649|H__sapiens__1_-718835|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 920,
        "networks" : [ "Ramaswamy-Golub-2001" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "919",
        "source" : "649",
        "target" : "628",
        "shared_name" : "H__sapiens__1_-703273|H__sapiens__1_-701067|Shared protein domains",
        "highlight" : 1,
        "normalized_max_weight" : 0.005989760821429396,
        "shared_interaction" : "",
        "raw_weights" : [ 0.1620197296142578, 0.1907898187637329 ],
        "name" : "H__sapiens__1_-703273|H__sapiens__1_-701067|Shared protein domains",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 919,
        "networks" : [ "INTERPRO", "PFAM" ],
        "selected" : false,
        "data_type" : "Shared protein domains"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "918",
        "source" : "649",
        "target" : "678",
        "shared_name" : "H__sapiens__1_-703273|H__sapiens__1_-705245|Shared protein domains",
        "highlight" : 1,
        "normalized_max_weight" : 6.434364339344575E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.017404600977897644, 0.013033779338002205 ],
        "name" : "H__sapiens__1_-703273|H__sapiens__1_-705245|Shared protein domains",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 918,
        "networks" : [ "INTERPRO", "PFAM" ],
        "selected" : false,
        "data_type" : "Shared protein domains"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "917",
        "source" : "649",
        "target" : "658",
        "shared_name" : "H__sapiens__1_-703273|H__sapiens__1_-706817|Predicted",
        "highlight" : 1,
        "normalized_max_weight" : 3.4436011004842796E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.006793776992708445 ],
        "name" : "H__sapiens__1_-703273|H__sapiens__1_-706817|Predicted",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 917,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Predicted"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "916",
        "source" : "649",
        "target" : "648",
        "shared_name" : "H__sapiens__1_-703273|H__sapiens__1_-711569|Predicted",
        "highlight" : 1,
        "normalized_max_weight" : 9.759530460320682E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.019254283979535103 ],
        "name" : "H__sapiens__1_-703273|H__sapiens__1_-711569|Predicted",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 916,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Predicted"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "915",
        "source" : "649",
        "target" : "657",
        "shared_name" : "H__sapiens__1_-703273|H__sapiens__1_-704636|Predicted",
        "highlight" : 1,
        "normalized_max_weight" : 2.3951070262681312E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.00472523458302021 ],
        "name" : "H__sapiens__1_-703273|H__sapiens__1_-704636|Predicted",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 915,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Predicted"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "914",
        "source" : "649",
        "target" : "656",
        "shared_name" : "H__sapiens__1_-703273|H__sapiens__1_-704308|Predicted",
        "highlight" : 1,
        "normalized_max_weight" : 1.6276794306420867E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0032111997716128826 ],
        "name" : "H__sapiens__1_-703273|H__sapiens__1_-704308|Predicted",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 914,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Predicted"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "913",
        "source" : "649",
        "target" : "640",
        "shared_name" : "H__sapiens__1_-703273|H__sapiens__1_-711862|Predicted",
        "highlight" : 1,
        "normalized_max_weight" : 3.508876342244455E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0069225565530359745 ],
        "name" : "H__sapiens__1_-703273|H__sapiens__1_-711862|Predicted",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 913,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Predicted"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "912",
        "source" : "649",
        "target" : "661",
        "shared_name" : "H__sapiens__1_-703273|H__sapiens__1_-705407|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 2.4446044982568813E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.00214249175041914, 0.002916901372373104, 0.0017624730244278908 ],
        "name" : "H__sapiens__1_-703273|H__sapiens__1_-705407|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 912,
        "networks" : [ "BIOGRID-SMALL-SCALE-STUDIES", "IREF-HPRD", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "911",
        "source" : "649",
        "target" : "659",
        "shared_name" : "H__sapiens__1_-703273|H__sapiens__1_-713803|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 2.386135725589591E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0020912487525492907, 0.001671333098784089, 0.0019078810000792146 ],
        "name" : "H__sapiens__1_-703273|H__sapiens__1_-713803|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 911,
        "networks" : [ "BIOGRID-SMALL-SCALE-STUDIES", "IREF-HPRD", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "910",
        "source" : "649",
        "target" : "658",
        "shared_name" : "H__sapiens__1_-703273|H__sapiens__1_-706817|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 4.3348322517991194E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0037991185672581196, 0.004086633678525686, 0.0026140648405998945 ],
        "name" : "H__sapiens__1_-703273|H__sapiens__1_-706817|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 910,
        "networks" : [ "BIOGRID-SMALL-SCALE-STUDIES", "IREF-HPRD", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "909",
        "source" : "649",
        "target" : "648",
        "shared_name" : "H__sapiens__1_-703273|H__sapiens__1_-711569|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 6.918084732808131E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.006063123699277639, 0.015452654100954533, 0.008665496483445168 ],
        "name" : "H__sapiens__1_-703273|H__sapiens__1_-711569|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 909,
        "networks" : [ "BIOGRID-SMALL-SCALE-STUDIES", "IREF-HPRD", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "908",
        "source" : "649",
        "target" : "657",
        "shared_name" : "H__sapiens__1_-703273|H__sapiens__1_-704636|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 1.7712780835288625E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.001552377361804247, 0.0014233308611437678 ],
        "name" : "H__sapiens__1_-703273|H__sapiens__1_-704636|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 908,
        "networks" : [ "BIOGRID-SMALL-SCALE-STUDIES", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "907",
        "source" : "649",
        "target" : "643",
        "shared_name" : "H__sapiens__1_-703273|H__sapiens__1_-716738|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 2.21168557858639E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.01938357762992382, 0.010378850623965263 ],
        "name" : "H__sapiens__1_-703273|H__sapiens__1_-716738|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 907,
        "networks" : [ "BIOGRID-SMALL-SCALE-STUDIES", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "906",
        "source" : "649",
        "target" : "628",
        "shared_name" : "H__sapiens__1_-703273|H__sapiens__1_-701067|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 8.176193848406657E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 7.165751303546131E-4, 0.0011775110615417361, 6.600877386517823E-4 ],
        "name" : "H__sapiens__1_-703273|H__sapiens__1_-701067|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 906,
        "networks" : [ "BIOGRID-SMALL-SCALE-STUDIES", "IREF-HPRD", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "905",
        "source" : "649",
        "target" : "686",
        "shared_name" : "H__sapiens__1_-703273|H__sapiens__1_-712776|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 1.1676636596611154E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0010233597131446004, 0.001337575726211071 ],
        "name" : "H__sapiens__1_-703273|H__sapiens__1_-712776|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 905,
        "networks" : [ "BIOGRID-SMALL-SCALE-STUDIES", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "904",
        "source" : "649",
        "target" : "678",
        "shared_name" : "H__sapiens__1_-703273|H__sapiens__1_-705245|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 7.19563304042416E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0011909797322005033, 0.003502032719552517, 0.0214464720338583, 0.001188179012387991 ],
        "name" : "H__sapiens__1_-703273|H__sapiens__1_-705245|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 904,
        "networks" : [ "BIOGRID-SMALL-SCALE-STUDIES", "IREF-HPRD", "IREF-INTACT", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "903",
        "source" : "649",
        "target" : "642",
        "shared_name" : "H__sapiens__1_-703273|H__sapiens__1_-710126|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 5.1520816357622576E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.004515369422733784, 0.01810373179614544, 0.0057549807243049145 ],
        "name" : "H__sapiens__1_-703273|H__sapiens__1_-710126|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 903,
        "networks" : [ "BIOGRID-SMALL-SCALE-STUDIES", "IREF-HPRD", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "902",
        "source" : "649",
        "target" : "640",
        "shared_name" : "H__sapiens__1_-703273|H__sapiens__1_-711862|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 5.700914543957611E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.004996375646442175, 0.004557386040687561 ],
        "name" : "H__sapiens__1_-703273|H__sapiens__1_-711862|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 902,
        "networks" : [ "BIOGRID-SMALL-SCALE-STUDIES", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "901",
        "source" : "649",
        "target" : "629",
        "shared_name" : "H__sapiens__1_-703273|H__sapiens__1_-707031|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 6.45904986203392E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.005660817958414555, 0.0057684388011693954 ],
        "name" : "H__sapiens__1_-703273|H__sapiens__1_-707031|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 901,
        "networks" : [ "BIOGRID-SMALL-SCALE-STUDIES", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "900",
        "source" : "649",
        "target" : "661",
        "shared_name" : "H__sapiens__1_-703273|H__sapiens__1_-705407|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 3.006585089286967E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0032891668379306793 ],
        "name" : "H__sapiens__1_-703273|H__sapiens__1_-705407|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 900,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "899",
        "source" : "649",
        "target" : "642",
        "shared_name" : "H__sapiens__1_-703273|H__sapiens__1_-710126|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 5.412007843740522E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.00592066952958703 ],
        "name" : "H__sapiens__1_-703273|H__sapiens__1_-710126|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 899,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "898",
        "source" : "649",
        "target" : "680",
        "shared_name" : "H__sapiens__1_-703273|H__sapiens__1_-710028|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.0012032323363711111,
        "shared_interaction" : "",
        "raw_weights" : [ 0.013163212686777115 ],
        "name" : "H__sapiens__1_-703273|H__sapiens__1_-710028|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 898,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "897",
        "source" : "649",
        "target" : "629",
        "shared_name" : "H__sapiens__1_-703273|H__sapiens__1_-707031|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.0014779245287105363,
        "shared_interaction" : "",
        "raw_weights" : [ 0.01616831123828888 ],
        "name" : "H__sapiens__1_-703273|H__sapiens__1_-707031|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 897,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "896",
        "source" : "649",
        "target" : "628",
        "shared_name" : "H__sapiens__1_-703273|H__sapiens__1_-701067|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 3.0007738345128726E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.019117364659905434, 0.0032828093972057104 ],
        "name" : "H__sapiens__1_-703273|H__sapiens__1_-701067|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 896,
        "networks" : [ "REACTOME", "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "895",
        "source" : "649",
        "target" : "659",
        "shared_name" : "H__sapiens__1_-703273|H__sapiens__1_-713803|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.03880973250434543,
        "shared_interaction" : "",
        "raw_weights" : [ 0.41435450315475464, 0.0035311877727508545 ],
        "name" : "H__sapiens__1_-703273|H__sapiens__1_-713803|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 895,
        "networks" : [ "CELL_MAP", "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "894",
        "source" : "649",
        "target" : "678",
        "shared_name" : "H__sapiens__1_-703273|H__sapiens__1_-705245|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.02503332046379672,
        "shared_interaction" : "",
        "raw_weights" : [ 0.2672697901725769, 0.006134266499429941 ],
        "name" : "H__sapiens__1_-703273|H__sapiens__1_-705245|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 894,
        "networks" : [ "CELL_MAP", "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "893",
        "source" : "649",
        "target" : "659",
        "shared_name" : "H__sapiens__1_-703273|H__sapiens__1_-713803|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 1.0731382641256924E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0010313233360648155 ],
        "name" : "H__sapiens__1_-703273|H__sapiens__1_-713803|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 893,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "892",
        "source" : "649",
        "target" : "643",
        "shared_name" : "H__sapiens__1_-703273|H__sapiens__1_-716738|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 9.09219150110215E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 8.737913449294865E-4 ],
        "name" : "H__sapiens__1_-703273|H__sapiens__1_-716738|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 892,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "891",
        "source" : "649",
        "target" : "628",
        "shared_name" : "H__sapiens__1_-703273|H__sapiens__1_-701067|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 3.727071091483828E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.003581845434382558 ],
        "name" : "H__sapiens__1_-703273|H__sapiens__1_-701067|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 891,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "890",
        "source" : "649",
        "target" : "686",
        "shared_name" : "H__sapiens__1_-703273|H__sapiens__1_-712776|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 1.3719407067975617E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0013184829149395227 ],
        "name" : "H__sapiens__1_-703273|H__sapiens__1_-712776|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 890,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "889",
        "source" : "649",
        "target" : "642",
        "shared_name" : "H__sapiens__1_-703273|H__sapiens__1_-710126|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 7.615333267828913E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 7.318601128645241E-4 ],
        "name" : "H__sapiens__1_-703273|H__sapiens__1_-710126|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 889,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "888",
        "source" : "649",
        "target" : "688",
        "shared_name" : "H__sapiens__1_-703273|H__sapiens__1_-707211|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 1.8286305093191176E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0017573777586221695 ],
        "name" : "H__sapiens__1_-703273|H__sapiens__1_-707211|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 888,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "887",
        "source" : "649",
        "target" : "628",
        "shared_name" : "H__sapiens__1_-703273|H__sapiens__1_-701067|Co-localization",
        "highlight" : 1,
        "normalized_max_weight" : 8.166059716098154E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.037622906267642975 ],
        "name" : "H__sapiens__1_-703273|H__sapiens__1_-701067|Co-localization",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 887,
        "networks" : [ "Johnson-Shoemaker-2003" ],
        "selected" : false,
        "data_type" : "Co-localization"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "886",
        "source" : "649",
        "target" : "648",
        "shared_name" : "H__sapiens__1_-703273|H__sapiens__1_-711569|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 4.6834538038693465E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0025046903174370527 ],
        "name" : "H__sapiens__1_-703273|H__sapiens__1_-711569|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 886,
        "networks" : [ "Roth-Zlotnik-2006" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "885",
        "source" : "649",
        "target" : "642",
        "shared_name" : "H__sapiens__1_-703273|H__sapiens__1_-710126|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 9.242130450439064E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.014392479322850704 ],
        "name" : "H__sapiens__1_-703273|H__sapiens__1_-710126|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 885,
        "networks" : [ "Wang-Maris-2006" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "884",
        "source" : "649",
        "target" : "640",
        "shared_name" : "H__sapiens__1_-703273|H__sapiens__1_-711862|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 2.4535125013527235E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.01615668274462223 ],
        "name" : "H__sapiens__1_-703273|H__sapiens__1_-711862|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 884,
        "networks" : [ "Noble-Diehl-2008" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "883",
        "source" : "649",
        "target" : "645",
        "shared_name" : "H__sapiens__1_-703273|H__sapiens__1_-707850|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 6.070296704137078E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.009091357700526714 ],
        "name" : "H__sapiens__1_-703273|H__sapiens__1_-707850|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 883,
        "networks" : [ "Bahr-Bowler-2013" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "882",
        "source" : "649",
        "target" : "628",
        "shared_name" : "H__sapiens__1_-703273|H__sapiens__1_-701067|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 0.001082419362775391,
        "shared_interaction" : "",
        "raw_weights" : [ 0.016211170703172684 ],
        "name" : "H__sapiens__1_-703273|H__sapiens__1_-701067|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 882,
        "networks" : [ "Bahr-Bowler-2013" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "881",
        "source" : "649",
        "target" : "630",
        "shared_name" : "H__sapiens__1_-703273|H__sapiens__1_-709018|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 8.153628249482543E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.012211520224809647 ],
        "name" : "H__sapiens__1_-703273|H__sapiens__1_-709018|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 881,
        "networks" : [ "Bahr-Bowler-2013" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "880",
        "source" : "648",
        "target" : "643",
        "shared_name" : "H__sapiens__1_-711569|H__sapiens__1_-716738|Shared protein domains",
        "highlight" : 1,
        "normalized_max_weight" : 0.018484664909915682,
        "shared_interaction" : "",
        "raw_weights" : [ 0.5, 0.5 ],
        "name" : "H__sapiens__1_-711569|H__sapiens__1_-716738|Shared protein domains",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 880,
        "networks" : [ "INTERPRO", "PFAM" ],
        "selected" : false,
        "data_type" : "Shared protein domains"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "879",
        "source" : "648",
        "target" : "638",
        "shared_name" : "H__sapiens__1_-711569|H__sapiens__1_-718245|Shared protein domains",
        "highlight" : 1,
        "normalized_max_weight" : 0.018484664909915682,
        "shared_interaction" : "",
        "raw_weights" : [ 0.5, 0.5 ],
        "name" : "H__sapiens__1_-711569|H__sapiens__1_-718245|Shared protein domains",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 879,
        "networks" : [ "INTERPRO", "PFAM" ],
        "selected" : false,
        "data_type" : "Shared protein domains"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "878",
        "source" : "648",
        "target" : "643",
        "shared_name" : "H__sapiens__1_-711569|H__sapiens__1_-716738|Predicted",
        "highlight" : 1,
        "normalized_max_weight" : 0.0033522213040615896,
        "shared_interaction" : "",
        "raw_weights" : [ 0.6613496541976929 ],
        "name" : "H__sapiens__1_-711569|H__sapiens__1_-716738|Predicted",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 878,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Predicted"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "877",
        "source" : "648",
        "target" : "638",
        "shared_name" : "H__sapiens__1_-711569|H__sapiens__1_-718245|Predicted",
        "highlight" : 1,
        "normalized_max_weight" : 0.0033522213040615896,
        "shared_interaction" : "",
        "raw_weights" : [ 0.6613496541976929 ],
        "name" : "H__sapiens__1_-711569|H__sapiens__1_-718245|Predicted",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 877,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Predicted"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "876",
        "source" : "648",
        "target" : "669",
        "shared_name" : "H__sapiens__1_-711569|H__sapiens__1_-702228|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 2.643936928649379E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.02317189984023571, 0.13216356933116913, 0.031861208379268646 ],
        "name" : "H__sapiens__1_-711569|H__sapiens__1_-702228|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 876,
        "networks" : [ "BIOGRID-SMALL-SCALE-STUDIES", "IREF-HPRD", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "875",
        "source" : "648",
        "target" : "645",
        "shared_name" : "H__sapiens__1_-711569|H__sapiens__1_-707850|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 9.711495799485637E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.08511315286159515, 0.10007224231958389, 0.08029655367136002 ],
        "name" : "H__sapiens__1_-711569|H__sapiens__1_-707850|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 875,
        "networks" : [ "BIOGRID-SMALL-SCALE-STUDIES", "IREF-HPRD", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "874",
        "source" : "648",
        "target" : "643",
        "shared_name" : "H__sapiens__1_-711569|H__sapiens__1_-716738|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 0.0024232284762797417,
        "shared_interaction" : "",
        "raw_weights" : [ 0.21237574517726898, 0.15848010778427124 ],
        "name" : "H__sapiens__1_-711569|H__sapiens__1_-716738|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 874,
        "networks" : [ "BIOGRID-SMALL-SCALE-STUDIES", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "873",
        "source" : "648",
        "target" : "628",
        "shared_name" : "H__sapiens__1_-711569|H__sapiens__1_-701067|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 8.958228792363099E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.007851139642298222, 0.016113266348838806, 0.010079225525259972 ],
        "name" : "H__sapiens__1_-711569|H__sapiens__1_-701067|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 873,
        "networks" : [ "BIOGRID-SMALL-SCALE-STUDIES", "IREF-HPRD", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "872",
        "source" : "648",
        "target" : "667",
        "shared_name" : "H__sapiens__1_-711569|H__sapiens__1_-720857|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 0.0015022211070856628,
        "shared_interaction" : "",
        "raw_weights" : [ 0.13165713846683502, 0.14472231268882751, 0.10343848168849945 ],
        "name" : "H__sapiens__1_-711569|H__sapiens__1_-720857|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 872,
        "networks" : [ "BIOGRID-SMALL-SCALE-STUDIES", "IREF-HPRD", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "871",
        "source" : "648",
        "target" : "638",
        "shared_name" : "H__sapiens__1_-711569|H__sapiens__1_-718245|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 0.0030144596727887238,
        "shared_interaction" : "",
        "raw_weights" : [ 0.26419222354888916, 0.17524965107440948 ],
        "name" : "H__sapiens__1_-711569|H__sapiens__1_-718245|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 871,
        "networks" : [ "BIOGRID-SMALL-SCALE-STUDIES", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "870",
        "source" : "648",
        "target" : "645",
        "shared_name" : "H__sapiens__1_-711569|H__sapiens__1_-707850|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.003997436356766661,
        "shared_interaction" : "",
        "raw_weights" : [ 0.043731458485126495 ],
        "name" : "H__sapiens__1_-711569|H__sapiens__1_-707850|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 870,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "869",
        "source" : "648",
        "target" : "628",
        "shared_name" : "H__sapiens__1_-711569|H__sapiens__1_-701067|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.0022430393762794413,
        "shared_interaction" : "",
        "raw_weights" : [ 0.024538572877645493 ],
        "name" : "H__sapiens__1_-711569|H__sapiens__1_-701067|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 869,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "868",
        "source" : "648",
        "target" : "678",
        "shared_name" : "H__sapiens__1_-711569|H__sapiens__1_-705245|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.0041913494474024775,
        "shared_interaction" : "",
        "raw_weights" : [ 0.045852843672037125 ],
        "name" : "H__sapiens__1_-711569|H__sapiens__1_-705245|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 868,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "867",
        "source" : "648",
        "target" : "667",
        "shared_name" : "H__sapiens__1_-711569|H__sapiens__1_-720857|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.004100225907828739,
        "shared_interaction" : "",
        "raw_weights" : [ 0.04485596343874931 ],
        "name" : "H__sapiens__1_-711569|H__sapiens__1_-720857|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 867,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "866",
        "source" : "648",
        "target" : "669",
        "shared_name" : "H__sapiens__1_-711569|H__sapiens__1_-702228|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.04742126289818356,
        "shared_interaction" : "",
        "raw_weights" : [ 0.5062960386276245, 0.04490097612142563 ],
        "name" : "H__sapiens__1_-711569|H__sapiens__1_-702228|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 866,
        "networks" : [ "CELL_MAP", "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "865",
        "source" : "648",
        "target" : "686",
        "shared_name" : "H__sapiens__1_-711569|H__sapiens__1_-712776|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 1.07362946806079E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0010317954001948237 ],
        "name" : "H__sapiens__1_-711569|H__sapiens__1_-712776|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 865,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "864",
        "source" : "648",
        "target" : "684",
        "shared_name" : "H__sapiens__1_-711569|H__sapiens__1_-713444|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 7.716804129553582E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 7.416118169203401E-4 ],
        "name" : "H__sapiens__1_-711569|H__sapiens__1_-713444|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 864,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "863",
        "source" : "648",
        "target" : "630",
        "shared_name" : "H__sapiens__1_-711569|H__sapiens__1_-709018|Co-localization",
        "highlight" : 1,
        "normalized_max_weight" : 5.131276650427426E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.023640966042876244 ],
        "name" : "H__sapiens__1_-711569|H__sapiens__1_-709018|Co-localization",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 863,
        "networks" : [ "Johnson-Shoemaker-2003" ],
        "selected" : false,
        "data_type" : "Co-localization"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "862",
        "source" : "648",
        "target" : "638",
        "shared_name" : "H__sapiens__1_-711569|H__sapiens__1_-718245|Co-localization",
        "highlight" : 1,
        "normalized_max_weight" : 3.066971023109193E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.014130237512290478 ],
        "name" : "H__sapiens__1_-711569|H__sapiens__1_-718245|Co-localization",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 862,
        "networks" : [ "Johnson-Shoemaker-2003" ],
        "selected" : false,
        "data_type" : "Co-localization"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "861",
        "source" : "648",
        "target" : "679",
        "shared_name" : "H__sapiens__1_-711569|H__sapiens__1_-701028|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 1.3393514700130795E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.006249094847589731, 0.006043706089258194 ],
        "name" : "H__sapiens__1_-711569|H__sapiens__1_-701028|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 861,
        "networks" : [ "Mallon-McKay-2013", "Roth-Zlotnik-2006" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "860",
        "source" : "648",
        "target" : "637",
        "shared_name" : "H__sapiens__1_-711569|H__sapiens__1_-701844|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 0.0012563382515696977,
        "shared_interaction" : "",
        "raw_weights" : [ 0.018815917894244194 ],
        "name" : "H__sapiens__1_-711569|H__sapiens__1_-701844|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 860,
        "networks" : [ "Bahr-Bowler-2013" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "859",
        "source" : "647",
        "target" : "674",
        "shared_name" : "H__sapiens__1_-704799|H__sapiens__1_-714657|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 0.0028311354874351593,
        "shared_interaction" : "",
        "raw_weights" : [ 0.24812538921833038, 0.09971032291650772, 0.7305099964141846, 0.07414155453443527 ],
        "name" : "H__sapiens__1_-704799|H__sapiens__1_-714657|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 859,
        "networks" : [ "BIOGRID-SMALL-SCALE-STUDIES", "IREF-HPRD", "IREF-INTACT", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "858",
        "source" : "647",
        "target" : "674",
        "shared_name" : "H__sapiens__1_-704799|H__sapiens__1_-714657|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.034849481553965875,
        "shared_interaction" : "",
        "raw_weights" : [ 0.38124901056289673 ],
        "name" : "H__sapiens__1_-704799|H__sapiens__1_-714657|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 858,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "857",
        "source" : "647",
        "target" : "634",
        "shared_name" : "H__sapiens__1_-704799|H__sapiens__1_-701737|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 2.7385284414885977E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0026318214368075132 ],
        "name" : "H__sapiens__1_-704799|H__sapiens__1_-701737|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 857,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "856",
        "source" : "647",
        "target" : "645",
        "shared_name" : "H__sapiens__1_-704799|H__sapiens__1_-707850|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 1.4887686264941016E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0014307586243376136 ],
        "name" : "H__sapiens__1_-704799|H__sapiens__1_-707850|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 856,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "855",
        "source" : "647",
        "target" : "643",
        "shared_name" : "H__sapiens__1_-704799|H__sapiens__1_-716738|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 7.0224071151089E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 6.74877839628607E-4 ],
        "name" : "H__sapiens__1_-704799|H__sapiens__1_-716738|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 855,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "854",
        "source" : "647",
        "target" : "628",
        "shared_name" : "H__sapiens__1_-704799|H__sapiens__1_-701067|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 2.8786251337348203E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.002766459248960018 ],
        "name" : "H__sapiens__1_-704799|H__sapiens__1_-701067|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 854,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "853",
        "source" : "647",
        "target" : "630",
        "shared_name" : "H__sapiens__1_-704799|H__sapiens__1_-709018|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 8.647030515378387E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 8.310098201036453E-4 ],
        "name" : "H__sapiens__1_-704799|H__sapiens__1_-709018|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 853,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "852",
        "source" : "647",
        "target" : "688",
        "shared_name" : "H__sapiens__1_-704799|H__sapiens__1_-707211|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 1.4123535263328164E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0013573210453614593 ],
        "name" : "H__sapiens__1_-704799|H__sapiens__1_-707211|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 852,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "851",
        "source" : "647",
        "target" : "689",
        "shared_name" : "H__sapiens__1_-704799|H__sapiens__1_-717747|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 5.881264664269411E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 5.652100662700832E-4 ],
        "name" : "H__sapiens__1_-704799|H__sapiens__1_-717747|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 851,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "850",
        "source" : "647",
        "target" : "654",
        "shared_name" : "H__sapiens__1_-704799|H__sapiens__1_-707210|Co-localization",
        "highlight" : 1,
        "normalized_max_weight" : 2.3180407206962943E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.010679744184017181 ],
        "name" : "H__sapiens__1_-704799|H__sapiens__1_-707210|Co-localization",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 850,
        "networks" : [ "Johnson-Shoemaker-2003" ],
        "selected" : false,
        "data_type" : "Co-localization"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "849",
        "source" : "647",
        "target" : "634",
        "shared_name" : "H__sapiens__1_-704799|H__sapiens__1_-701737|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 1.6238829406686165E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.01564675010740757 ],
        "name" : "H__sapiens__1_-704799|H__sapiens__1_-701737|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 849,
        "networks" : [ "Bild-Nevins-2006 B" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "848",
        "source" : "647",
        "target" : "654",
        "shared_name" : "H__sapiens__1_-704799|H__sapiens__1_-707210|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 1.371631143668142E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.013216204941272736 ],
        "name" : "H__sapiens__1_-704799|H__sapiens__1_-707210|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 848,
        "networks" : [ "Bild-Nevins-2006 B" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "847",
        "source" : "647",
        "target" : "674",
        "shared_name" : "H__sapiens__1_-704799|H__sapiens__1_-714657|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 2.839197948154922E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0068772705271840096 ],
        "name" : "H__sapiens__1_-704799|H__sapiens__1_-714657|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 847,
        "networks" : [ "Boldrick-Relman-2002" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "846",
        "source" : "647",
        "target" : "669",
        "shared_name" : "H__sapiens__1_-704799|H__sapiens__1_-702228|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 1.343592543184605E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.00884772278368473 ],
        "name" : "H__sapiens__1_-704799|H__sapiens__1_-702228|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 846,
        "networks" : [ "Noble-Diehl-2008" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "845",
        "source" : "647",
        "target" : "635",
        "shared_name" : "H__sapiens__1_-704799|H__sapiens__1_-701014|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 8.619966321410281E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.012909945100545883 ],
        "name" : "H__sapiens__1_-704799|H__sapiens__1_-701014|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 845,
        "networks" : [ "Bahr-Bowler-2013" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "844",
        "source" : "645",
        "target" : "677",
        "shared_name" : "H__sapiens__1_-707850|H__sapiens__1_-707068|Shared protein domains",
        "highlight" : 1,
        "normalized_max_weight" : 1.62109273350887E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.009141362272202969 ],
        "name" : "H__sapiens__1_-707850|H__sapiens__1_-707068|Shared protein domains",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 844,
        "networks" : [ "PFAM" ],
        "selected" : false,
        "data_type" : "Shared protein domains"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "843",
        "source" : "645",
        "target" : "667",
        "shared_name" : "H__sapiens__1_-707850|H__sapiens__1_-720857|Shared protein domains",
        "highlight" : 1,
        "normalized_max_weight" : 0.0019539300515404308,
        "shared_interaction" : "",
        "raw_weights" : [ 0.052852731198072433, 0.06156548857688904 ],
        "name" : "H__sapiens__1_-707850|H__sapiens__1_-720857|Shared protein domains",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 843,
        "networks" : [ "INTERPRO", "PFAM" ],
        "selected" : false,
        "data_type" : "Shared protein domains"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "842",
        "source" : "645",
        "target" : "666",
        "shared_name" : "H__sapiens__1_-707850|H__sapiens__1_-706362|Shared protein domains",
        "highlight" : 1,
        "normalized_max_weight" : 5.293976037608896E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.01431991346180439 ],
        "name" : "H__sapiens__1_-707850|H__sapiens__1_-706362|Shared protein domains",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 842,
        "networks" : [ "INTERPRO" ],
        "selected" : false,
        "data_type" : "Shared protein domains"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "841",
        "source" : "645",
        "target" : "643",
        "shared_name" : "H__sapiens__1_-707850|H__sapiens__1_-716738|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 5.894687241297703E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.14113427698612213, 0.09617288410663605 ],
        "name" : "H__sapiens__1_-707850|H__sapiens__1_-716738|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 841,
        "networks" : [ "IREF-HPRD", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "840",
        "source" : "645",
        "target" : "638",
        "shared_name" : "H__sapiens__1_-707850|H__sapiens__1_-718245|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 6.518432550453365E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.15458369255065918, 0.1063494011759758 ],
        "name" : "H__sapiens__1_-707850|H__sapiens__1_-718245|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 840,
        "networks" : [ "IREF-HPRD", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "839",
        "source" : "645",
        "target" : "682",
        "shared_name" : "H__sapiens__1_-707850|H__sapiens__1_-702553|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 2.1354556480170027E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.01871548593044281, 0.016610456630587578, 0.011592364870011806 ],
        "name" : "H__sapiens__1_-707850|H__sapiens__1_-702553|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 839,
        "networks" : [ "BIOGRID-SMALL-SCALE-STUDIES", "IREF-HPRD", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "838",
        "source" : "645",
        "target" : "677",
        "shared_name" : "H__sapiens__1_-707850|H__sapiens__1_-707068|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.0060430627368065854,
        "shared_interaction" : "",
        "raw_weights" : [ 0.06611035764217377 ],
        "name" : "H__sapiens__1_-707850|H__sapiens__1_-707068|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 838,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "837",
        "source" : "645",
        "target" : "628",
        "shared_name" : "H__sapiens__1_-707850|H__sapiens__1_-701067|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 5.551770383298079E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.006073568016290665 ],
        "name" : "H__sapiens__1_-707850|H__sapiens__1_-701067|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 837,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "836",
        "source" : "645",
        "target" : "678",
        "shared_name" : "H__sapiens__1_-707850|H__sapiens__1_-705245|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.001037405324183199,
        "shared_interaction" : "",
        "raw_weights" : [ 0.011349085718393326 ],
        "name" : "H__sapiens__1_-707850|H__sapiens__1_-705245|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 836,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "835",
        "source" : "645",
        "target" : "667",
        "shared_name" : "H__sapiens__1_-707850|H__sapiens__1_-720857|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.001014851313400337,
        "shared_interaction" : "",
        "raw_weights" : [ 0.011102347634732723 ],
        "name" : "H__sapiens__1_-707850|H__sapiens__1_-720857|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 835,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "834",
        "source" : "645",
        "target" : "643",
        "shared_name" : "H__sapiens__1_-707850|H__sapiens__1_-716738|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.019312292982930947,
        "shared_interaction" : "",
        "raw_weights" : [ 0.2061888873577118, 0.043731458485126495 ],
        "name" : "H__sapiens__1_-707850|H__sapiens__1_-716738|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 834,
        "networks" : [ "CELL_MAP", "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "833",
        "source" : "645",
        "target" : "682",
        "shared_name" : "H__sapiens__1_-707850|H__sapiens__1_-702553|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.011874136592591442,
        "shared_interaction" : "",
        "raw_weights" : [ 0.12677495181560516, 0.007638728246092796 ],
        "name" : "H__sapiens__1_-707850|H__sapiens__1_-702553|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 833,
        "networks" : [ "CELL_MAP", "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "832",
        "source" : "645",
        "target" : "638",
        "shared_name" : "H__sapiens__1_-707850|H__sapiens__1_-718245|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.02095751598793996,
        "shared_interaction" : "",
        "raw_weights" : [ 0.22375421226024628, 0.043731458485126495 ],
        "name" : "H__sapiens__1_-707850|H__sapiens__1_-718245|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 832,
        "networks" : [ "CELL_MAP", "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "831",
        "source" : "645",
        "target" : "641",
        "shared_name" : "H__sapiens__1_-707850|H__sapiens__1_-704846|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 1.0039982515925513E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 9.648773702792823E-4 ],
        "name" : "H__sapiens__1_-707850|H__sapiens__1_-704846|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 831,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "830",
        "source" : "645",
        "target" : "629",
        "shared_name" : "H__sapiens__1_-707850|H__sapiens__1_-707031|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 2.4752869832810915E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0023788372054696083 ],
        "name" : "H__sapiens__1_-707850|H__sapiens__1_-707031|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 830,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "829",
        "source" : "645",
        "target" : "689",
        "shared_name" : "H__sapiens__1_-707850|H__sapiens__1_-717747|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 4.5798246836432026E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 4.4013714068569243E-4 ],
        "name" : "H__sapiens__1_-707850|H__sapiens__1_-717747|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 829,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "828",
        "source" : "645",
        "target" : "628",
        "shared_name" : "H__sapiens__1_-707850|H__sapiens__1_-701067|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 5.648488900772444E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.012635849416255951 ],
        "name" : "H__sapiens__1_-707850|H__sapiens__1_-701067|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 828,
        "networks" : [ "Perou-Botstein-1999" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "827",
        "source" : "645",
        "target" : "678",
        "shared_name" : "H__sapiens__1_-707850|H__sapiens__1_-705245|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 6.426471073447542E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.014376220293343067 ],
        "name" : "H__sapiens__1_-707850|H__sapiens__1_-705245|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 827,
        "networks" : [ "Perou-Botstein-1999" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "826",
        "source" : "645",
        "target" : "666",
        "shared_name" : "H__sapiens__1_-707850|H__sapiens__1_-706362|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 2.4270317994723334E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.011323952116072178 ],
        "name" : "H__sapiens__1_-707850|H__sapiens__1_-706362|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 826,
        "networks" : [ "Mallon-McKay-2013" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "825",
        "source" : "645",
        "target" : "641",
        "shared_name" : "H__sapiens__1_-707850|H__sapiens__1_-704846|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 0.001972399305581235,
        "shared_interaction" : "",
        "raw_weights" : [ 0.026970434933900833, 0.013022586703300476 ],
        "name" : "H__sapiens__1_-707850|H__sapiens__1_-704846|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 825,
        "networks" : [ "Burington-Shaughnessy-2008", "Perou-Botstein-1999" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "824",
        "source" : "645",
        "target" : "642",
        "shared_name" : "H__sapiens__1_-707850|H__sapiens__1_-710126|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 6.736764629467702E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.010089512914419174 ],
        "name" : "H__sapiens__1_-707850|H__sapiens__1_-710126|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 824,
        "networks" : [ "Bahr-Bowler-2013" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "823",
        "source" : "645",
        "target" : "630",
        "shared_name" : "H__sapiens__1_-707850|H__sapiens__1_-709018|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 4.70154358377003E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.007041404489427805, 0.010143949650228024 ],
        "name" : "H__sapiens__1_-707850|H__sapiens__1_-709018|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 823,
        "networks" : [ "Bahr-Bowler-2013", "Perou-Botstein-1999" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "822",
        "source" : "645",
        "target" : "638",
        "shared_name" : "H__sapiens__1_-707850|H__sapiens__1_-718245|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 2.7291741557529246E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.004087427631020546 ],
        "name" : "H__sapiens__1_-707850|H__sapiens__1_-718245|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 822,
        "networks" : [ "Bahr-Bowler-2013" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "821",
        "source" : "644",
        "target" : "683",
        "shared_name" : "H__sapiens__1_-705635|H__sapiens__1_-701859|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 1.922155859697074E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0018472588853910565 ],
        "name" : "H__sapiens__1_-705635|H__sapiens__1_-701859|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 821,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "820",
        "source" : "644",
        "target" : "689",
        "shared_name" : "H__sapiens__1_-705635|H__sapiens__1_-717747|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 6.5847704530420005E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 6.328194285742939E-4 ],
        "name" : "H__sapiens__1_-705635|H__sapiens__1_-717747|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 820,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "819",
        "source" : "643",
        "target" : "638",
        "shared_name" : "H__sapiens__1_-716738|H__sapiens__1_-718245|Shared protein domains",
        "highlight" : 1,
        "normalized_max_weight" : 0.018484664909915682,
        "shared_interaction" : "",
        "raw_weights" : [ 0.5, 0.5 ],
        "name" : "H__sapiens__1_-716738|H__sapiens__1_-718245|Shared protein domains",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 819,
        "networks" : [ "INTERPRO", "PFAM" ],
        "selected" : false,
        "data_type" : "Shared protein domains"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "818",
        "source" : "643",
        "target" : "667",
        "shared_name" : "H__sapiens__1_-716738|H__sapiens__1_-720857|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 7.593571199352047E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.20410531759262085, 0.1238904818892479 ],
        "name" : "H__sapiens__1_-716738|H__sapiens__1_-720857|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 818,
        "networks" : [ "IREF-HPRD", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "817",
        "source" : "643",
        "target" : "628",
        "shared_name" : "H__sapiens__1_-716738|H__sapiens__1_-701067|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 2.863912153647287E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.025099799036979675, 0.022724920883774757, 0.012072103098034859 ],
        "name" : "H__sapiens__1_-716738|H__sapiens__1_-701067|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 817,
        "networks" : [ "BIOGRID-SMALL-SCALE-STUDIES", "IREF-HPRD", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "816",
        "source" : "643",
        "target" : "628",
        "shared_name" : "H__sapiens__1_-716738|H__sapiens__1_-701067|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.0022430393762794413,
        "shared_interaction" : "",
        "raw_weights" : [ 0.024538572877645493 ],
        "name" : "H__sapiens__1_-716738|H__sapiens__1_-701067|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 816,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "815",
        "source" : "643",
        "target" : "678",
        "shared_name" : "H__sapiens__1_-716738|H__sapiens__1_-705245|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.0041913494474024775,
        "shared_interaction" : "",
        "raw_weights" : [ 0.045852843672037125 ],
        "name" : "H__sapiens__1_-716738|H__sapiens__1_-705245|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 815,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "814",
        "source" : "643",
        "target" : "667",
        "shared_name" : "H__sapiens__1_-716738|H__sapiens__1_-720857|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.016137439343131728,
        "shared_interaction" : "",
        "raw_weights" : [ 0.17229236662387848, 0.04485596343874931 ],
        "name" : "H__sapiens__1_-716738|H__sapiens__1_-720857|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 814,
        "networks" : [ "CELL_MAP", "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "813",
        "source" : "643",
        "target" : "656",
        "shared_name" : "H__sapiens__1_-716738|H__sapiens__1_-704308|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 3.2217354432739037E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 3.096200234722346E-4 ],
        "name" : "H__sapiens__1_-716738|H__sapiens__1_-704308|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 813,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "812",
        "source" : "643",
        "target" : "637",
        "shared_name" : "H__sapiens__1_-716738|H__sapiens__1_-701844|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 4.471158077959314E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 4.2969390051439404E-4 ],
        "name" : "H__sapiens__1_-716738|H__sapiens__1_-701844|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 812,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "811",
        "source" : "643",
        "target" : "630",
        "shared_name" : "H__sapiens__1_-716738|H__sapiens__1_-709018|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 3.1761709786210583E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 3.0524111934937537E-4 ],
        "name" : "H__sapiens__1_-716738|H__sapiens__1_-709018|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 811,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "810",
        "source" : "643",
        "target" : "638",
        "shared_name" : "H__sapiens__1_-716738|H__sapiens__1_-718245|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 2.07471989532263E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 1.993878249777481E-4 ],
        "name" : "H__sapiens__1_-716738|H__sapiens__1_-718245|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 810,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "809",
        "source" : "643",
        "target" : "688",
        "shared_name" : "H__sapiens__1_-716738|H__sapiens__1_-707211|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 5.187764657257379E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 4.98562294524163E-4 ],
        "name" : "H__sapiens__1_-716738|H__sapiens__1_-707211|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 809,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "808",
        "source" : "643",
        "target" : "679",
        "shared_name" : "H__sapiens__1_-716738|H__sapiens__1_-701028|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 9.077603773880416E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 8.723894134163857E-4 ],
        "name" : "H__sapiens__1_-716738|H__sapiens__1_-701028|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 808,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "807",
        "source" : "643",
        "target" : "689",
        "shared_name" : "H__sapiens__1_-716738|H__sapiens__1_-717747|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 2.16026796660246E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 2.0760929328389466E-4 ],
        "name" : "H__sapiens__1_-716738|H__sapiens__1_-717747|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 807,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "806",
        "source" : "643",
        "target" : "690",
        "shared_name" : "H__sapiens__1_-716738|H__sapiens__1_-713226|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 8.231853562708884E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 7.911098655313253E-4 ],
        "name" : "H__sapiens__1_-716738|H__sapiens__1_-713226|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 806,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "805",
        "source" : "643",
        "target" : "644",
        "shared_name" : "H__sapiens__1_-716738|H__sapiens__1_-705635|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 6.372490401113709E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.00954394694417715 ],
        "name" : "H__sapiens__1_-716738|H__sapiens__1_-705635|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 805,
        "networks" : [ "Bahr-Bowler-2013" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "804",
        "source" : "642",
        "target" : "640",
        "shared_name" : "H__sapiens__1_-710126|H__sapiens__1_-711862|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 4.992247320782672E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 4.797723959200084E-4 ],
        "name" : "H__sapiens__1_-710126|H__sapiens__1_-711862|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 804,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "803",
        "source" : "642",
        "target" : "630",
        "shared_name" : "H__sapiens__1_-710126|H__sapiens__1_-709018|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 2.6602605758063953E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 2.556603285484016E-4 ],
        "name" : "H__sapiens__1_-710126|H__sapiens__1_-709018|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 803,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "802",
        "source" : "642",
        "target" : "638",
        "shared_name" : "H__sapiens__1_-710126|H__sapiens__1_-718245|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 1.7377200779413998E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 1.67000966030173E-4 ],
        "name" : "H__sapiens__1_-710126|H__sapiens__1_-718245|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 802,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "801",
        "source" : "642",
        "target" : "688",
        "shared_name" : "H__sapiens__1_-710126|H__sapiens__1_-707211|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 4.345108243497041E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 4.175800713710487E-4 ],
        "name" : "H__sapiens__1_-710126|H__sapiens__1_-707211|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 801,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "800",
        "source" : "642",
        "target" : "644",
        "shared_name" : "H__sapiens__1_-710126|H__sapiens__1_-705635|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 6.585310716802922E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 6.328713498078287E-4 ],
        "name" : "H__sapiens__1_-710126|H__sapiens__1_-705635|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 800,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "799",
        "source" : "642",
        "target" : "684",
        "shared_name" : "H__sapiens__1_-710126|H__sapiens__1_-713444|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 2.343112120416277E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 2.2518125479109585E-4 ],
        "name" : "H__sapiens__1_-710126|H__sapiens__1_-713444|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 799,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "798",
        "source" : "642",
        "target" : "639",
        "shared_name" : "H__sapiens__1_-710126|H__sapiens__1_-718375|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 5.041191765164016E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 4.8447612789459527E-4 ],
        "name" : "H__sapiens__1_-710126|H__sapiens__1_-718375|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 798,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "797",
        "source" : "642",
        "target" : "683",
        "shared_name" : "H__sapiens__1_-710126|H__sapiens__1_-701859|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 5.281726337246735E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 5.075923399999738E-4 ],
        "name" : "H__sapiens__1_-710126|H__sapiens__1_-701859|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 797,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "796",
        "source" : "642",
        "target" : "693",
        "shared_name" : "H__sapiens__1_-710126|H__sapiens__1_-710026|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 7.554608105642863E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 7.260242127813399E-4 ],
        "name" : "H__sapiens__1_-710126|H__sapiens__1_-710026|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 796,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "795",
        "source" : "642",
        "target" : "690",
        "shared_name" : "H__sapiens__1_-710126|H__sapiens__1_-713226|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 6.89474133477876E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 6.626087124459445E-4 ],
        "name" : "H__sapiens__1_-710126|H__sapiens__1_-713226|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 795,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "794",
        "source" : "642",
        "target" : "666",
        "shared_name" : "H__sapiens__1_-710126|H__sapiens__1_-706362|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 1.4647232068726115E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.004899146966636181 ],
        "name" : "H__sapiens__1_-710126|H__sapiens__1_-706362|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 794,
        "networks" : [ "Perou-Botstein-2000" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "793",
        "source" : "642",
        "target" : "640",
        "shared_name" : "H__sapiens__1_-710126|H__sapiens__1_-711862|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 0.0010322533683043778,
        "shared_interaction" : "",
        "raw_weights" : [ 0.016074957326054573 ],
        "name" : "H__sapiens__1_-710126|H__sapiens__1_-711862|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 793,
        "networks" : [ "Wang-Maris-2006" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "792",
        "source" : "642",
        "target" : "655",
        "shared_name" : "H__sapiens__1_-710126|H__sapiens__1_-707212|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 5.836287225033791E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.00798049382865429 ],
        "name" : "H__sapiens__1_-710126|H__sapiens__1_-707212|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 792,
        "networks" : [ "Burington-Shaughnessy-2008" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "791",
        "source" : "642",
        "target" : "641",
        "shared_name" : "H__sapiens__1_-710126|H__sapiens__1_-704846|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 9.986391569538578E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.01495641190558672, 0.012456751428544521 ],
        "name" : "H__sapiens__1_-710126|H__sapiens__1_-704846|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 791,
        "networks" : [ "Bahr-Bowler-2013", "Burington-Shaughnessy-2008" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "790",
        "source" : "641",
        "target" : "630",
        "shared_name" : "H__sapiens__1_-704846|H__sapiens__1_-709018|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 5.831398076865643E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 5.604177131317556E-4 ],
        "name" : "H__sapiens__1_-704846|H__sapiens__1_-709018|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 790,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "789",
        "source" : "641",
        "target" : "684",
        "shared_name" : "H__sapiens__1_-704846|H__sapiens__1_-713444|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 5.136196117871337E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 4.936063778586686E-4 ],
        "name" : "H__sapiens__1_-704846|H__sapiens__1_-713444|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 789,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "788",
        "source" : "641",
        "target" : "666",
        "shared_name" : "H__sapiens__1_-704846|H__sapiens__1_-706362|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 7.054459571826602E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.01578105054795742 ],
        "name" : "H__sapiens__1_-704846|H__sapiens__1_-706362|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 788,
        "networks" : [ "Perou-Botstein-1999" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "787",
        "source" : "640",
        "target" : "684",
        "shared_name" : "H__sapiens__1_-711862|H__sapiens__1_-713444|Predicted",
        "highlight" : 1,
        "normalized_max_weight" : 3.88738709524267E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.06124238669872284 ],
        "name" : "H__sapiens__1_-711862|H__sapiens__1_-713444|Predicted",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 787,
        "networks" : [ "I2D-INNATEDB-Mouse2Human" ],
        "selected" : false,
        "data_type" : "Predicted"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "786",
        "source" : "640",
        "target" : "684",
        "shared_name" : "H__sapiens__1_-711862|H__sapiens__1_-713444|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 6.464361287195713E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 6.212476873770356E-4 ],
        "name" : "H__sapiens__1_-711862|H__sapiens__1_-713444|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 786,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "785",
        "source" : "640",
        "target" : "685",
        "shared_name" : "H__sapiens__1_-711862|H__sapiens__1_-746649|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 1.9616412668212048E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.001885205740109086 ],
        "name" : "H__sapiens__1_-711862|H__sapiens__1_-746649|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 785,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "784",
        "source" : "640",
        "target" : "689",
        "shared_name" : "H__sapiens__1_-711862|H__sapiens__1_-717747|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 4.991837580385516E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 4.7973301843740046E-4 ],
        "name" : "H__sapiens__1_-711862|H__sapiens__1_-717747|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 784,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "783",
        "source" : "640",
        "target" : "683",
        "shared_name" : "H__sapiens__1_-711862|H__sapiens__1_-701859|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 9.118626798461876E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.014200150966644287 ],
        "name" : "H__sapiens__1_-711862|H__sapiens__1_-701859|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 783,
        "networks" : [ "Wang-Maris-2006" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "782",
        "source" : "640",
        "target" : "629",
        "shared_name" : "H__sapiens__1_-711862|H__sapiens__1_-707031|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 6.352074570697762E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.014209792949259281 ],
        "name" : "H__sapiens__1_-711862|H__sapiens__1_-707031|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 782,
        "networks" : [ "Perou-Botstein-1999" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "781",
        "source" : "640",
        "target" : "641",
        "shared_name" : "H__sapiens__1_-711862|H__sapiens__1_-704846|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 9.331225655144053E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.013975183479487896 ],
        "name" : "H__sapiens__1_-711862|H__sapiens__1_-704846|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 781,
        "networks" : [ "Bahr-Bowler-2013" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "780",
        "source" : "640",
        "target" : "630",
        "shared_name" : "H__sapiens__1_-711862|H__sapiens__1_-709018|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 0.0010152000386621982,
        "shared_interaction" : "",
        "raw_weights" : [ 0.01520444080233574 ],
        "name" : "H__sapiens__1_-711862|H__sapiens__1_-709018|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 780,
        "networks" : [ "Bahr-Bowler-2013" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "779",
        "source" : "638",
        "target" : "693",
        "shared_name" : "H__sapiens__1_-718245|H__sapiens__1_-710026|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 7.2548459074154385E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 6.972160190343857E-4 ],
        "name" : "H__sapiens__1_-718245|H__sapiens__1_-710026|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 779,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "778",
        "source" : "638",
        "target" : "681",
        "shared_name" : "H__sapiens__1_-718245|H__sapiens__1_-710479|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 5.529798255038852E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 5.314329173415899E-4 ],
        "name" : "H__sapiens__1_-718245|H__sapiens__1_-710479|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 778,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "777",
        "source" : "638",
        "target" : "689",
        "shared_name" : "H__sapiens__1_-718245|H__sapiens__1_-717747|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 1.73757759245961E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 1.669872726779431E-4 ],
        "name" : "H__sapiens__1_-718245|H__sapiens__1_-717747|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 777,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "776",
        "source" : "638",
        "target" : "663",
        "shared_name" : "H__sapiens__1_-718245|H__sapiens__1_-712333|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 6.298062697768574E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.006068430840969086 ],
        "name" : "H__sapiens__1_-718245|H__sapiens__1_-712333|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 776,
        "networks" : [ "Bild-Nevins-2006 B" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "775",
        "source" : "638",
        "target" : "639",
        "shared_name" : "H__sapiens__1_-718245|H__sapiens__1_-718375|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 2.5043535281697905E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.003750718431547284 ],
        "name" : "H__sapiens__1_-718245|H__sapiens__1_-718375|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 775,
        "networks" : [ "Bahr-Bowler-2013" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "774",
        "source" : "637",
        "target" : "642",
        "shared_name" : "H__sapiens__1_-701844|H__sapiens__1_-710126|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 3.744901249218695E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 3.598980838432908E-4 ],
        "name" : "H__sapiens__1_-701844|H__sapiens__1_-710126|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 774,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "773",
        "source" : "637",
        "target" : "676",
        "shared_name" : "H__sapiens__1_-701844|H__sapiens__1_-718153|Co-localization",
        "highlight" : 1,
        "normalized_max_weight" : 5.046625724776631E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0065270233899354935 ],
        "name" : "H__sapiens__1_-701844|H__sapiens__1_-718153|Co-localization",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 773,
        "networks" : [ "Schadt-Shoemaker-2004" ],
        "selected" : false,
        "data_type" : "Co-localization"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "772",
        "source" : "637",
        "target" : "679",
        "shared_name" : "H__sapiens__1_-701844|H__sapiens__1_-701028|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 3.2494618288667055E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.010868668556213379 ],
        "name" : "H__sapiens__1_-701844|H__sapiens__1_-701028|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 772,
        "networks" : [ "Perou-Botstein-2000" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "771",
        "source" : "637",
        "target" : "646",
        "shared_name" : "H__sapiens__1_-701844|H__sapiens__1_-701435|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 4.6570380933508645E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.015576672740280628 ],
        "name" : "H__sapiens__1_-701844|H__sapiens__1_-701435|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 771,
        "networks" : [ "Perou-Botstein-2000" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "770",
        "source" : "637",
        "target" : "640",
        "shared_name" : "H__sapiens__1_-701844|H__sapiens__1_-711862|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 1.9840212988539864E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.013065025210380554 ],
        "name" : "H__sapiens__1_-701844|H__sapiens__1_-711862|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 770,
        "networks" : [ "Noble-Diehl-2008" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "769",
        "source" : "636",
        "target" : "657",
        "shared_name" : "H__sapiens__1_-713265|H__sapiens__1_-704636|Shared protein domains",
        "highlight" : 1,
        "normalized_max_weight" : 4.4364325485600687E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.012000305578112602, 0.012520037591457367 ],
        "name" : "H__sapiens__1_-713265|H__sapiens__1_-704636|Shared protein domains",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 769,
        "networks" : [ "INTERPRO", "PFAM" ],
        "selected" : false,
        "data_type" : "Shared protein domains"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "768",
        "source" : "636",
        "target" : "635",
        "shared_name" : "H__sapiens__1_-713265|H__sapiens__1_-701014|Shared protein domains",
        "highlight" : 1,
        "normalized_max_weight" : 0.0010089248803282689,
        "shared_interaction" : "",
        "raw_weights" : [ 0.027290862053632736, 0.07956524938344955 ],
        "name" : "H__sapiens__1_-713265|H__sapiens__1_-701014|Shared protein domains",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 768,
        "networks" : [ "INTERPRO", "PFAM" ],
        "selected" : false,
        "data_type" : "Shared protein domains"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "767",
        "source" : "636",
        "target" : "637",
        "shared_name" : "H__sapiens__1_-713265|H__sapiens__1_-701844|Shared protein domains",
        "highlight" : 1,
        "normalized_max_weight" : 7.681159703983476E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.020777113735675812, 0.07956524938344955 ],
        "name" : "H__sapiens__1_-713265|H__sapiens__1_-701844|Shared protein domains",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 767,
        "networks" : [ "INTERPRO", "PFAM" ],
        "selected" : false,
        "data_type" : "Shared protein domains"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "766",
        "source" : "636",
        "target" : "637",
        "shared_name" : "H__sapiens__1_-713265|H__sapiens__1_-701844|Predicted",
        "highlight" : 1,
        "normalized_max_weight" : 0.0010660898101641193,
        "shared_interaction" : "",
        "raw_weights" : [ 0.16795313358306885 ],
        "name" : "H__sapiens__1_-713265|H__sapiens__1_-701844|Predicted",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 766,
        "networks" : [ "I2D-INNATEDB-Mouse2Human" ],
        "selected" : false,
        "data_type" : "Predicted"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "765",
        "source" : "636",
        "target" : "635",
        "shared_name" : "H__sapiens__1_-713265|H__sapiens__1_-701014|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 2.645524950365773E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.023185817524790764, 0.029578253626823425, 0.01584499329328537 ],
        "name" : "H__sapiens__1_-713265|H__sapiens__1_-701014|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 765,
        "networks" : [ "BIOGRID-SMALL-SCALE-STUDIES", "IREF-HPRD", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "764",
        "source" : "636",
        "target" : "637",
        "shared_name" : "H__sapiens__1_-713265|H__sapiens__1_-701844|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 1.1003690875659833E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.009643816389143467, 0.010868505574762821, 0.01405352633446455, 0.005818481091409922 ],
        "name" : "H__sapiens__1_-713265|H__sapiens__1_-701844|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 764,
        "networks" : [ "BIOGRID-SMALL-SCALE-STUDIES", "IREF-HPRD", "IREF-INTACT", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "763",
        "source" : "636",
        "target" : "657",
        "shared_name" : "H__sapiens__1_-713265|H__sapiens__1_-704636|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 3.4927639642492634E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.003821040503680706 ],
        "name" : "H__sapiens__1_-713265|H__sapiens__1_-704636|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 763,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "762",
        "source" : "636",
        "target" : "634",
        "shared_name" : "H__sapiens__1_-713265|H__sapiens__1_-701737|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.0020758832989454653,
        "shared_interaction" : "",
        "raw_weights" : [ 0.022709906101226807 ],
        "name" : "H__sapiens__1_-713265|H__sapiens__1_-701737|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 762,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "761",
        "source" : "636",
        "target" : "635",
        "shared_name" : "H__sapiens__1_-713265|H__sapiens__1_-701014|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.00251922255292377,
        "shared_interaction" : "",
        "raw_weights" : [ 0.027559982612729073 ],
        "name" : "H__sapiens__1_-713265|H__sapiens__1_-701014|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 761,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "760",
        "source" : "636",
        "target" : "637",
        "shared_name" : "H__sapiens__1_-713265|H__sapiens__1_-701844|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.04888759810684253,
        "shared_interaction" : "",
        "raw_weights" : [ 0.3088391125202179, 0.012300035916268826 ],
        "name" : "H__sapiens__1_-713265|H__sapiens__1_-701844|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 760,
        "networks" : [ "IMID", "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "759",
        "source" : "636",
        "target" : "631",
        "shared_name" : "H__sapiens__1_-713265|H__sapiens__1_-717093|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 1.133838763670583E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.002746450249105692 ],
        "name" : "H__sapiens__1_-713265|H__sapiens__1_-717093|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 759,
        "networks" : [ "Boldrick-Relman-2002" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "758",
        "source" : "636",
        "target" : "682",
        "shared_name" : "H__sapiens__1_-713265|H__sapiens__1_-702553|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 2.130268241021084E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.008005653508007526 ],
        "name" : "H__sapiens__1_-713265|H__sapiens__1_-702553|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 758,
        "networks" : [ "Dobbin-Giordano-2005" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "757",
        "source" : "636",
        "target" : "637",
        "shared_name" : "H__sapiens__1_-713265|H__sapiens__1_-701844|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 4.809088521933256E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.014110321179032326 ],
        "name" : "H__sapiens__1_-713265|H__sapiens__1_-701844|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 757,
        "networks" : [ "Alizadeh-Staudt-2000" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "756",
        "source" : "635",
        "target" : "637",
        "shared_name" : "H__sapiens__1_-701014|H__sapiens__1_-701844|Shared protein domains",
        "highlight" : 1,
        "normalized_max_weight" : 0.0020979353554877633,
        "shared_interaction" : "",
        "raw_weights" : [ 0.05674799531698227, 0.18255168199539185 ],
        "name" : "H__sapiens__1_-701014|H__sapiens__1_-701844|Shared protein domains",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 756,
        "networks" : [ "INTERPRO", "PFAM" ],
        "selected" : false,
        "data_type" : "Shared protein domains"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "755",
        "source" : "635",
        "target" : "637",
        "shared_name" : "H__sapiens__1_-701014|H__sapiens__1_-701844|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 1.715052720956972E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.015031005255877972, 0.019755741581320763, 0.010454099625349045, 0.009085570462048054 ],
        "name" : "H__sapiens__1_-701014|H__sapiens__1_-701844|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 755,
        "networks" : [ "BIOGRID-SMALL-SCALE-STUDIES", "IREF-HPRD", "IREF-INTACT", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "754",
        "source" : "635",
        "target" : "637",
        "shared_name" : "H__sapiens__1_-701014|H__sapiens__1_-701844|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.09340322560857055,
        "shared_interaction" : "",
        "raw_weights" : [ 0.5900590419769287, 0.0264116320759058 ],
        "name" : "H__sapiens__1_-701014|H__sapiens__1_-701844|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 754,
        "networks" : [ "IMID", "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "753",
        "source" : "635",
        "target" : "692",
        "shared_name" : "H__sapiens__1_-701014|H__sapiens__1_-711599|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 2.2931866006538342E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0022038323804736137 ],
        "name" : "H__sapiens__1_-701014|H__sapiens__1_-711599|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 753,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "752",
        "source" : "635",
        "target" : "689",
        "shared_name" : "H__sapiens__1_-701014|H__sapiens__1_-717747|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 8.055999525559632E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 7.742096786387265E-4 ],
        "name" : "H__sapiens__1_-701014|H__sapiens__1_-717747|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 752,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "751",
        "source" : "635",
        "target" : "678",
        "shared_name" : "H__sapiens__1_-701014|H__sapiens__1_-705245|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 7.479490497290014E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.016731858253479004 ],
        "name" : "H__sapiens__1_-701014|H__sapiens__1_-705245|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 751,
        "networks" : [ "Perou-Botstein-1999" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "750",
        "source" : "635",
        "target" : "641",
        "shared_name" : "H__sapiens__1_-701014|H__sapiens__1_-704846|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 0.0013201337491767257,
        "shared_interaction" : "",
        "raw_weights" : [ 0.018051406368613243, 0.013125558383762836, 0.012344597838819027 ],
        "name" : "H__sapiens__1_-701014|H__sapiens__1_-704846|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 750,
        "networks" : [ "Burington-Shaughnessy-2008", "Perou-Botstein-1999", "Wang-Maris-2006" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "749",
        "source" : "634",
        "target" : "664",
        "shared_name" : "H__sapiens__1_-701737|H__sapiens__1_-716588|Predicted",
        "highlight" : 1,
        "normalized_max_weight" : 7.313252357608221E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.14428095519542694 ],
        "name" : "H__sapiens__1_-701737|H__sapiens__1_-716588|Predicted",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 749,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Predicted"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "748",
        "source" : "634",
        "target" : "693",
        "shared_name" : "H__sapiens__1_-701737|H__sapiens__1_-710026|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 5.52978961522934E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.01648145169019699 ],
        "name" : "H__sapiens__1_-701737|H__sapiens__1_-710026|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 748,
        "networks" : [ "IREF-INTACT" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "747",
        "source" : "634",
        "target" : "635",
        "shared_name" : "H__sapiens__1_-701737|H__sapiens__1_-701014|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 1.6675225848950955E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.01461444329470396, 0.029864851385354996, 0.006634654477238655, 0.015518909320235252 ],
        "name" : "H__sapiens__1_-701737|H__sapiens__1_-701014|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 747,
        "networks" : [ "BIOGRID-SMALL-SCALE-STUDIES", "IREF-HPRD", "IREF-INTACT", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "746",
        "source" : "634",
        "target" : "637",
        "shared_name" : "H__sapiens__1_-701737|H__sapiens__1_-701844|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 6.935826709907243E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.006078673060983419, 0.010973815806210041, 0.005698738154023886 ],
        "name" : "H__sapiens__1_-701737|H__sapiens__1_-701844|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 746,
        "networks" : [ "BIOGRID-SMALL-SCALE-STUDIES", "IREF-HPRD", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "745",
        "source" : "634",
        "target" : "664",
        "shared_name" : "H__sapiens__1_-701737|H__sapiens__1_-716588|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 2.4904698893904237E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.021826889365911484, 0.093518927693367, 0.031160686165094376 ],
        "name" : "H__sapiens__1_-701737|H__sapiens__1_-716588|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 745,
        "networks" : [ "BIOGRID-SMALL-SCALE-STUDIES", "IREF-HPRD", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "744",
        "source" : "634",
        "target" : "635",
        "shared_name" : "H__sapiens__1_-701737|H__sapiens__1_-701014|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.0044575047032285,
        "shared_interaction" : "",
        "raw_weights" : [ 0.04876454919576645 ],
        "name" : "H__sapiens__1_-701737|H__sapiens__1_-701014|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 744,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "743",
        "source" : "634",
        "target" : "637",
        "shared_name" : "H__sapiens__1_-701737|H__sapiens__1_-701844|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.0019893869696554277,
        "shared_interaction" : "",
        "raw_weights" : [ 0.021763646975159645 ],
        "name" : "H__sapiens__1_-701737|H__sapiens__1_-701844|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 743,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "742",
        "source" : "634",
        "target" : "664",
        "shared_name" : "H__sapiens__1_-701737|H__sapiens__1_-716588|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.03987069910024149,
        "shared_interaction" : "",
        "raw_weights" : [ 0.4256819784641266 ],
        "name" : "H__sapiens__1_-701737|H__sapiens__1_-716588|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 742,
        "networks" : [ "CELL_MAP" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "741",
        "source" : "634",
        "target" : "675",
        "shared_name" : "H__sapiens__1_-701737|H__sapiens__1_-702552|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 3.303175541520932E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0031744670122861862 ],
        "name" : "H__sapiens__1_-701737|H__sapiens__1_-702552|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 741,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "740",
        "source" : "634",
        "target" : "678",
        "shared_name" : "H__sapiens__1_-701737|H__sapiens__1_-705245|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 6.994100364551239E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.010891692712903023 ],
        "name" : "H__sapiens__1_-701737|H__sapiens__1_-705245|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 740,
        "networks" : [ "Wang-Maris-2006" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "739",
        "source" : "634",
        "target" : "629",
        "shared_name" : "H__sapiens__1_-701737|H__sapiens__1_-707031|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 3.513538135648188E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.013204050250351429 ],
        "name" : "H__sapiens__1_-701737|H__sapiens__1_-707031|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 739,
        "networks" : [ "Dobbin-Giordano-2005" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "738",
        "source" : "634",
        "target" : "646",
        "shared_name" : "H__sapiens__1_-701737|H__sapiens__1_-701435|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 7.114450162903338E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.01065516471862793 ],
        "name" : "H__sapiens__1_-701737|H__sapiens__1_-701435|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 738,
        "networks" : [ "Bahr-Bowler-2013" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "737",
        "source" : "634",
        "target" : "635",
        "shared_name" : "H__sapiens__1_-701737|H__sapiens__1_-701014|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 6.432297091743599E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.01887296885251999, 0.013258133083581924 ],
        "name" : "H__sapiens__1_-701737|H__sapiens__1_-701014|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 737,
        "networks" : [ "Alizadeh-Staudt-2000", "Boldrick-Relman-2002" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "736",
        "source" : "633",
        "target" : "646",
        "shared_name" : "H__sapiens__1_-706527|H__sapiens__1_-701435|Shared protein domains",
        "highlight" : 1,
        "normalized_max_weight" : 7.889362914448067E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.02134029194712639, 0.01925576478242874 ],
        "name" : "H__sapiens__1_-706527|H__sapiens__1_-701435|Shared protein domains",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 736,
        "networks" : [ "INTERPRO", "PFAM" ],
        "selected" : false,
        "data_type" : "Shared protein domains"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "735",
        "source" : "633",
        "target" : "668",
        "shared_name" : "H__sapiens__1_-706527|H__sapiens__1_-714838|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 3.3212950776530416E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0032001982908695936 ],
        "name" : "H__sapiens__1_-706527|H__sapiens__1_-714838|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 735,
        "networks" : [ "Bild-Nevins-2006 B" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "734",
        "source" : "631",
        "target" : "637",
        "shared_name" : "H__sapiens__1_-717093|H__sapiens__1_-701844|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 1.389423682286374E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.012177138589322567, 0.019787365570664406, 0.015490514226257801, 0.008909419178962708 ],
        "name" : "H__sapiens__1_-717093|H__sapiens__1_-701844|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 734,
        "networks" : [ "BIOGRID-SMALL-SCALE-STUDIES", "IREF-HPRD", "IREF-INTACT", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "733",
        "source" : "631",
        "target" : "637",
        "shared_name" : "H__sapiens__1_-717093|H__sapiens__1_-701844|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.04352968785413817,
        "shared_interaction" : "",
        "raw_weights" : [ 0.2749914228916168, 0.30090150237083435 ],
        "name" : "H__sapiens__1_-717093|H__sapiens__1_-701844|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 733,
        "networks" : [ "IMID", "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "732",
        "source" : "631",
        "target" : "641",
        "shared_name" : "H__sapiens__1_-717093|H__sapiens__1_-704846|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 1.010056810274302E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0024466183967888355 ],
        "name" : "H__sapiens__1_-717093|H__sapiens__1_-704846|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 732,
        "networks" : [ "Boldrick-Relman-2002" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "731",
        "source" : "631",
        "target" : "633",
        "shared_name" : "H__sapiens__1_-717093|H__sapiens__1_-706527|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 1.5374209443194846E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.004510938655585051, 0.0026257666759192944 ],
        "name" : "H__sapiens__1_-717093|H__sapiens__1_-706527|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 731,
        "networks" : [ "Alizadeh-Staudt-2000", "Boldrick-Relman-2002" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "730",
        "source" : "631",
        "target" : "632",
        "shared_name" : "H__sapiens__1_-717093|H__sapiens__1_-721468|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 1.34585258664696E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0025616753846406937, 0.0032600024715065956 ],
        "name" : "H__sapiens__1_-717093|H__sapiens__1_-721468|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 730,
        "networks" : [ "Alizadeh-Staudt-2000", "Boldrick-Relman-2002" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "729",
        "source" : "630",
        "target" : "638",
        "shared_name" : "H__sapiens__1_-709018|H__sapiens__1_-718245|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 2.554703212293682E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 2.455158974044025E-4 ],
        "name" : "H__sapiens__1_-709018|H__sapiens__1_-718245|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 729,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "728",
        "source" : "630",
        "target" : "639",
        "shared_name" : "H__sapiens__1_-709018|H__sapiens__1_-718375|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 7.4112904238444226E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 7.122508832253516E-4 ],
        "name" : "H__sapiens__1_-709018|H__sapiens__1_-718375|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 728,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "727",
        "source" : "630",
        "target" : "683",
        "shared_name" : "H__sapiens__1_-709018|H__sapiens__1_-701859|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 7.764911831350047E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 7.46235135011375E-4 ],
        "name" : "H__sapiens__1_-709018|H__sapiens__1_-701859|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 727,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "726",
        "source" : "630",
        "target" : "671",
        "shared_name" : "H__sapiens__1_-709018|H__sapiens__1_-716648|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 6.717107819008895E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 6.455375114455819E-4 ],
        "name" : "H__sapiens__1_-709018|H__sapiens__1_-716648|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 726,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "725",
        "source" : "630",
        "target" : "692",
        "shared_name" : "H__sapiens__1_-709018|H__sapiens__1_-711599|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 7.571963776124025E-6,
        "shared_interaction" : "",
        "raw_weights" : [ 7.276921533048153E-4 ],
        "name" : "H__sapiens__1_-709018|H__sapiens__1_-711599|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 725,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "724",
        "source" : "630",
        "target" : "638",
        "shared_name" : "H__sapiens__1_-709018|H__sapiens__1_-718245|Co-localization",
        "highlight" : 1,
        "normalized_max_weight" : 2.6465748676813227E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.012193376198410988 ],
        "name" : "H__sapiens__1_-709018|H__sapiens__1_-718245|Co-localization",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 724,
        "networks" : [ "Johnson-Shoemaker-2003" ],
        "selected" : false,
        "data_type" : "Co-localization"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "723",
        "source" : "630",
        "target" : "666",
        "shared_name" : "H__sapiens__1_-709018|H__sapiens__1_-706362|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 2.627672203649341E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.009874922223389149 ],
        "name" : "H__sapiens__1_-709018|H__sapiens__1_-706362|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 723,
        "networks" : [ "Dobbin-Giordano-2005" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "722",
        "source" : "630",
        "target" : "681",
        "shared_name" : "H__sapiens__1_-709018|H__sapiens__1_-710479|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 1.78820413624257E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0067201596684753895 ],
        "name" : "H__sapiens__1_-709018|H__sapiens__1_-710479|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 722,
        "networks" : [ "Dobbin-Giordano-2005" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "721",
        "source" : "630",
        "target" : "655",
        "shared_name" : "H__sapiens__1_-709018|H__sapiens__1_-707212|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 9.521767625038647E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.013019991107285023 ],
        "name" : "H__sapiens__1_-709018|H__sapiens__1_-707212|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 721,
        "networks" : [ "Burington-Shaughnessy-2008" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "720",
        "source" : "630",
        "target" : "638",
        "shared_name" : "H__sapiens__1_-709018|H__sapiens__1_-718245|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 3.473001136146194E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.004806640557944775, 0.008412505500018597 ],
        "name" : "H__sapiens__1_-709018|H__sapiens__1_-718245|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 720,
        "networks" : [ "Bahr-Bowler-2013", "Boldrick-Relman-2002" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "719",
        "source" : "630",
        "target" : "639",
        "shared_name" : "H__sapiens__1_-709018|H__sapiens__1_-718375|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 4.171908964927899E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.006248181685805321 ],
        "name" : "H__sapiens__1_-709018|H__sapiens__1_-718375|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 719,
        "networks" : [ "Bahr-Bowler-2013" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "718",
        "source" : "628",
        "target" : "678",
        "shared_name" : "H__sapiens__1_-701067|H__sapiens__1_-705245|Shared protein domains",
        "highlight" : 1,
        "normalized_max_weight" : 5.127943146071296E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.013870803639292717, 0.013033779338002205 ],
        "name" : "H__sapiens__1_-701067|H__sapiens__1_-705245|Shared protein domains",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 718,
        "networks" : [ "INTERPRO", "PFAM" ],
        "selected" : false,
        "data_type" : "Shared protein domains"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "717",
        "source" : "628",
        "target" : "640",
        "shared_name" : "H__sapiens__1_-701067|H__sapiens__1_-711862|Predicted",
        "highlight" : 1,
        "normalized_max_weight" : 3.493380104837816E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0068919844925403595 ],
        "name" : "H__sapiens__1_-701067|H__sapiens__1_-711862|Predicted",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 717,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Predicted"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "716",
        "source" : "628",
        "target" : "678",
        "shared_name" : "H__sapiens__1_-701067|H__sapiens__1_-705245|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 1.2152895473138319E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.001542199868708849, 0.003651747014373541, 0.03622151538729668, 0.0013820239109918475 ],
        "name" : "H__sapiens__1_-701067|H__sapiens__1_-705245|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 716,
        "networks" : [ "BIOGRID-SMALL-SCALE-STUDIES", "IREF-HPRD", "IREF-INTACT", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "715",
        "source" : "628",
        "target" : "640",
        "shared_name" : "H__sapiens__1_-701067|H__sapiens__1_-711862|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 7.382115644869566E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.006469808053225279, 0.0053008985705673695 ],
        "name" : "H__sapiens__1_-701067|H__sapiens__1_-711862|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 715,
        "networks" : [ "BIOGRID-SMALL-SCALE-STUDIES", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "714",
        "source" : "628",
        "target" : "629",
        "shared_name" : "H__sapiens__1_-701067|H__sapiens__1_-707031|Physical Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 8.363825271690606E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.00733019458130002, 0.006709527689963579 ],
        "name" : "H__sapiens__1_-701067|H__sapiens__1_-707031|Physical Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 714,
        "networks" : [ "BIOGRID-SMALL-SCALE-STUDIES", "IREF-SMALL-SCALE-STUDIES" ],
        "selected" : false,
        "data_type" : "Physical Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "713",
        "source" : "628",
        "target" : "667",
        "shared_name" : "H__sapiens__1_-701067|H__sapiens__1_-720857|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 5.69452783137719E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.006229742895811796 ],
        "name" : "H__sapiens__1_-701067|H__sapiens__1_-720857|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 713,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "712",
        "source" : "628",
        "target" : "642",
        "shared_name" : "H__sapiens__1_-701067|H__sapiens__1_-710126|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 5.618391774322972E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0061464509926736355 ],
        "name" : "H__sapiens__1_-701067|H__sapiens__1_-710126|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 712,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "711",
        "source" : "628",
        "target" : "638",
        "shared_name" : "H__sapiens__1_-701067|H__sapiens__1_-718245|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.0022430393762794413,
        "shared_interaction" : "",
        "raw_weights" : [ 0.024538572877645493 ],
        "name" : "H__sapiens__1_-701067|H__sapiens__1_-718245|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 711,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "710",
        "source" : "628",
        "target" : "680",
        "shared_name" : "H__sapiens__1_-701067|H__sapiens__1_-710028|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.001249117008034679,
        "shared_interaction" : "",
        "raw_weights" : [ 0.013665185309946537 ],
        "name" : "H__sapiens__1_-701067|H__sapiens__1_-710028|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 710,
        "networks" : [ "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "709",
        "source" : "628",
        "target" : "678",
        "shared_name" : "H__sapiens__1_-701067|H__sapiens__1_-705245|Pathway",
        "highlight" : 1,
        "normalized_max_weight" : 0.05512392088967157,
        "shared_interaction" : "",
        "raw_weights" : [ 0.3482360243797302, 0.07524148374795914, 0.006368192844092846 ],
        "name" : "H__sapiens__1_-701067|H__sapiens__1_-705245|Pathway",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 709,
        "networks" : [ "IMID", "REACTOME", "Wu-Stein-2010" ],
        "selected" : false,
        "data_type" : "Pathway"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "708",
        "source" : "628",
        "target" : "641",
        "shared_name" : "H__sapiens__1_-701067|H__sapiens__1_-704846|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 1.941291977881827E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.001865649363026023 ],
        "name" : "H__sapiens__1_-701067|H__sapiens__1_-704846|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 708,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "707",
        "source" : "628",
        "target" : "684",
        "shared_name" : "H__sapiens__1_-701067|H__sapiens__1_-713444|Genetic Interactions",
        "highlight" : 1,
        "normalized_max_weight" : 1.1467581655687588E-5,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0011020746314898133 ],
        "name" : "H__sapiens__1_-701067|H__sapiens__1_-713444|Genetic Interactions",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 707,
        "networks" : [ "Lin-Smith-2010" ],
        "selected" : false,
        "data_type" : "Genetic Interactions"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "706",
        "source" : "628",
        "target" : "652",
        "shared_name" : "H__sapiens__1_-701067|H__sapiens__1_-721077|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 1.2160435952727335E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.011717057786881924 ],
        "name" : "H__sapiens__1_-701067|H__sapiens__1_-721077|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 706,
        "networks" : [ "Bild-Nevins-2006 B" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "705",
        "source" : "628",
        "target" : "680",
        "shared_name" : "H__sapiens__1_-701067|H__sapiens__1_-710028|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 3.5346241266185014E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.008561772294342518 ],
        "name" : "H__sapiens__1_-701067|H__sapiens__1_-710028|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 705,
        "networks" : [ "Boldrick-Relman-2002" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "704",
        "source" : "628",
        "target" : "641",
        "shared_name" : "H__sapiens__1_-701067|H__sapiens__1_-704846|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 5.620982594467586E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.012574316933751106 ],
        "name" : "H__sapiens__1_-701067|H__sapiens__1_-704846|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 704,
        "networks" : [ "Perou-Botstein-1999" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "703",
        "source" : "628",
        "target" : "644",
        "shared_name" : "H__sapiens__1_-701067|H__sapiens__1_-705635|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 7.6903999912243E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.010515793226659298 ],
        "name" : "H__sapiens__1_-701067|H__sapiens__1_-705635|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 703,
        "networks" : [ "Burington-Shaughnessy-2008" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "702",
        "source" : "628",
        "target" : "640",
        "shared_name" : "H__sapiens__1_-701067|H__sapiens__1_-711862|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 0.0011785256940755626,
        "shared_interaction" : "",
        "raw_weights" : [ 0.017650535330176353 ],
        "name" : "H__sapiens__1_-701067|H__sapiens__1_-711862|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 702,
        "networks" : [ "Bahr-Bowler-2013" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "701",
        "source" : "628",
        "target" : "630",
        "shared_name" : "H__sapiens__1_-701067|H__sapiens__1_-709018|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 9.412633104784707E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.0276175569742918, 0.009433341212570667 ],
        "name" : "H__sapiens__1_-701067|H__sapiens__1_-709018|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 701,
        "networks" : [ "Alizadeh-Staudt-2000", "Bahr-Bowler-2013" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "700",
        "source" : "628",
        "target" : "629",
        "shared_name" : "H__sapiens__1_-701067|H__sapiens__1_-707031|Co-expression",
        "highlight" : 1,
        "normalized_max_weight" : 6.24351260893215E-4,
        "shared_interaction" : "",
        "raw_weights" : [ 0.018319057300686836 ],
        "name" : "H__sapiens__1_-701067|H__sapiens__1_-707031|Co-expression",
        "interaction" : "",
        "isInPath" : false,
        "SUID" : 700,
        "networks" : [ "Alizadeh-Staudt-2000" ],
        "selected" : false,
        "data_type" : "Co-expression"
      },
      "selected" : false
    } ]
  },
}

var cyHuman1 = writeHTMLenvironment("cyHuman1",cytoscapeInput,8,0.3,"gene_name","","");
