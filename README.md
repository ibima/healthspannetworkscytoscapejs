# Healthspan Networks #

This repository presents the source code for the presentation of healthspan-associated
molecular interaction pathways. It is the supplement to a review paper submitted in
2018 currently on http://functional.domains/healthspan. A preprint is available on
https://www.biorxiv.org/content/early/2018/06/25/355131.

### How do I get set up? ###

For inspection, only a regular web browser is required. Download this repository via
the 'Downloads' button to the left. Unzip and open the index.html file in your browser.
An initial inspection may take a minute or two e.g. for the automated download of
the CytoscapeJS Java library

You will find the web page displaying five networks. Below every network, alphabetically
ordered the genes interacting are listed as buttons and so are the most abundant
Gene Ontology terms associated with these genes. A click on these buttons will
highlight the single gene or the set of genes in the graph.

### Contribution guidelines ###

The JavaScript behind that page is rather simplistic but will with some confidence
improve over time. Also the pathways will become better, e.g. the integration of
mouse data is in preparation.

Please create issues for any change you find helpful. And pull requests are welcome.

### Who do I talk to? ###

Please contact Steffen M�ller <steffen.moeller@uni-rostock.de> about the JavaScript
implementation, address Prof. Georg Fuellen about the pathways.